# OPPTools

A set of tools assembled in Python package form for working with FVCOM.

## Installation

### Installation with pip

Clone the repository and install it with pip:
```
    git clone git@gitlab.com:mdunphy/OPPTools.git OPPTools
    pip install --user --editable OPPTools
```

### Installation with Anaconda

Clone the repository:
```
    git clone git@gitlab.com:mdunphy/OPPTools.git OPPTools
```
Create a new conda environment called `opptools` and install dependencies:
```
    conda env create -f OPPTools/env_opptools.yml
```
Activate the `opptools` environment and install the OPPTools package within the `opptools` environment using pip:
```
    conda activate opptools # for Anaconda3, OR
    source activate opptools # for Anaconda2
    pip install --editable OPPTools
```
From here on, activate the `opptools` environment to work with this package.

### GPSC and your own installation of Anaconda

The GPSC tools (ssm, jobctl, etc) are written in python 2, and if you install anaconda3 there will be lots of problems because the tools are not python 3 compatible. The suggested workaround at the moment is to install anaconda 3, but ensure to comment out the part that it adds to your `.bashrc`, as below:

```
    # added by Anaconda3 installer
    #export PATH="$HOME/anaconda3/bin:$PATH"
```
This will ensure that both submitted jobs and fresh logins to GPSC will not have anaconda 3 loaded, and thus not incur problems.

Next, add an alias that loads anaconda3 and activates the `opptools` conda environment, editing paths as appropriate:
```
    CONDA_SCRIPT=/home/mid002/WORK4/anaconda3/etc/profile.d/conda.sh
    alias cfvt='source $CONDA_SCRIPT && conda activate opptools
```
Upon login to gpsc, run `cfvt` to enable anaconda to work with OPPTools, and run `conda deactivate` to turn it off.

## Organization

1. We include Pramod's fvcomToolbox library, a collection of functions for
   working with FVCOM, as a submodule. Load it using:
    
    ```
        import OPPTools.fvcomToolbox as fvt
    ```

1. The functions for producing a FVCOM type-3 nesting file are also included
   as a submodule; load using:
    
    ```
        import OPPTools.nesting as nesting
    ```

1. The functions for producing atmospheric forcing:
    
    ```
        import OPPTools.atm as atm
    ```

1. The functions for producing river forcing:

    ```
        import OPPTools.river as riv
    ```

1. More utility functions included in submodule utils, load using:

    ```
        import OPPTools.utils as utils
    ```

## Examples

See bin/gen_nesting_file.py for a demo of generating a type-3 nesting file.


## Acknowledgements

Thanks to Maxim Krassovski, Pramod Thupaki and Marlene Jeffries for extensive contributions to this package.

## License

Apache 2.0

