from collections import OrderedDict

# Using lon,lat from Marlene's spreadsheet (sourced from ISDM)
stations = OrderedDict()
stations["07594"] = {"Name":"Sand Heads",        "Lon":-123.195,   'Lat':49.125}
stations["07610"] = {"Name":"Woodwards Landing", "Lon":-123.0754,  "Lat":49.1251}
stations["07654"] = {"Name":"New Westminster",   "Lon":-122.90535, "Lat":49.203683}
stations["07724"] = {"Name":"Calamity Point",    "Lon":-123.127678,"Lat":49.31257}
stations["07735"] = {"Name":"Vancouver Harbour", "Lon":-123.11,    "Lat":49.287}
stations["07786"] = {"Name":"Sandy Cove",        "Lon":-123.23,    "Lat":49.34}
stations["07755"] = {"Name":"Port Moody",        "Lon":-122.866,   "Lat":49.288}
stations["07774"] = {"Name":"Indian Arm Head",   "Lon":-122.88635, "Lat":49.461783}
#stations["07795"] = {"Name":"Point Atkinson",    "Lon":-123.253,   "Lat":49.337}


# Using lon,lat from the CHS Web API
stations_chs = OrderedDict()
stations_chs["07594"] = {"Name":"Sand Heads",        "Lon":-123.303368, 'Lat':49.105896}   # Seems to be the lighthouse location ...
stations_chs["07610"] = {"Name":"Woodwards Landing", "Lon":-123.076432, "Lat":49.124433}
stations_chs["07654"] = {"Name":"New Westminster",   "Lon":-122.910377, "Lat":49.200077}
stations_chs["07724"] = {"Name":"Calamity Point",    "Lon":-123.127657, "Lat":49.312617}
stations_chs["07735"] = {"Name":"Vancouver Harbour", "Lon":-123.107339, "Lat":49.289554}
stations_chs["07786"] = {"Name":"Sandy Cove",        "Lon":-123.231978, "Lat":49.340556}
stations_chs["07755"] = {"Name":"Port Moody",        "Lon":-122.865747, "Lat":49.287994}
stations_chs["07774"] = {"Name":"Indian Arm Head",   "Lon":-122.88635 , "Lat":49.461783}   # Same value as above

# Vancouver Harbour
# https://maps.google.com/maps?q=49.287,-123.11
# https://maps.google.com/maps?q=49.289554,-123.107339
# Sandy Cove
# https://maps.google.com/maps?q=49.34,-123.23
# https://maps.google.com/maps?q=49.340556,-123.231978
# Sand Heads
# https://maps.google.com/maps?q=49.125,-123.195
