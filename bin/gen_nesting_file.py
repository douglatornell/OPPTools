"""
Generation of FVCOM Type 3 FVCOM nesting file from the variables output by NEMO.

Time recorded in the NEMO files should have units: 'seconds since 1900-01-01 00:00:00'


The program needs a parameter file. For example, if the parameter file is:

  make_fvcom_boundary_param.py

then the program is run like this:

  python gen_nesting_file.py make_fvcom_boundary_param


Some of the code is from generate_type3_nesting_file.py
Pramod Thupaki, Maxim Krassovski, 2016

Michael Dunphy
Maxim Krassovski
2017
"""

import sys
import OPPTools.nesting as nesting

import time
start_time = time.time()

# import parameters
prm = __import__(sys.argv[1])

nesting.make_type3_nesting_file(prm.output_file, 
                                prm.nemo_nznodes_file,
                                prm.nznodes_file,
                                prm.nzcentr_file, 
                                prm.interp_uv,
                                prm.nemo_vertical_weight_file,
                                prm.nemo_azimuth_file,
                                prm.fvcom_grd_file,
                                prm.fvcom_dep_file,
                                prm.fvcom_sigma_file,
                                prm.input_dir,
                                prm.fvcom_time_start,
                                prm.fvcom_time_end,
                                prm.opt,
                                prm.nrampup)

print("--- gen_nesting_file run time: %s seconds ---" % (time.time() - start_time))