"""
Commandline script to call nc_surface_extract that rewrites the output netcdf file
with only the surface layer included.
"""

from OPPTools.utils import nc_surface_extract
import sys

if __name__ == "__main__":
    """
    Usage: python gen_surface_nc.py in.nc out.nc
    """
    infile = sys.argv[1]
    outfile = sys.argv[2]

    nc_surface_extract(infile, outfile)
