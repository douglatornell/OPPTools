"""
Generation of FVCOM Type 3 FVCOM nesting file from the variables output by NEMO.

Version 2 of gen_nesting_file.py. Besides standard fvcom and nemo grid definition 
files, requires only files defining fvcom indices of the nesting zone nodes and 
a reference polyline for nesting zone weights calculation.

Time recorded in the NEMO files should have units: 'seconds since 1900-01-01 00:00:00'


The program needs a parameter file. For example, if the parameter file is:

  make_fvcom_boundary_param2.py

then the program is run like this:

  python gen_nesting_file2.py make_fvcom_boundary_param2
  

Michael Dunphy
Maxim Krassovski
2018
"""

import sys
import OPPTools.nesting as nesting

import time
start_time = time.time()

# import parameters
prm = __import__(sys.argv[1])

x,y,z,tri,nsiglev,siglev,nsiglay,siglay,\
nemo_lon,nemo_lat,dx,dy,e3u_0,e3v_0,\
gdept_0,gdepw_0,gdepu,gdepv,tmask,umask,vmask,gdept_1d,nemo_h = nesting.read_metrics(
                                                            prm.fvcom_grd_file,
                                                            prm.fvcom_dep_file,
                                                            prm.fvcom_sigma_file,
                                                            prm.nemo_coord_file,
                                                            prm.nemo_mask_file,
                                                            prm.nemo_bathy_file,
                                                            prm.nemo_cut_i,
                                                            prm.nemo_cut_j)

inest,xb,yb = nesting.read_nesting(prm.nest_indices_file,
                                   prm.nest_refline_file)

if prm.search_dir_flag:
    prm.nemo_file_list = nesting.list_nemo_files(prm.input_dir, prm.nemo_file_pattern, 'T')
                                 
nesting.make_type3_nesting_file2(prm.output_file,
                                 x,y,z,tri,nsiglev,siglev,nsiglay,siglay,
                                 prm.fvcom_utmzone,
                                 inest, xb,yb,
                                 prm.rwidth, 
                                 prm.dl, prm.du,
                                 nemo_lon,nemo_lat, dx,dy, 
                                 e3u_0,e3v_0,
                                 prm.nemo_file_list,
                                 prm.fvcom_time_start,
                                 prm.fvcom_time_end,
                                 prm.opt,
                                 prm.nrampup,
                                 gdept_0=gdept_0,gdepw_0=gdepw_0,gdepu=gdepu,gdepv=gdepv,
                                 tmask=tmask,umask=umask,vmask=vmask,
                                 ua_name=prm.ua_name,
                                 va_name=prm.va_name,
                                 u_name=prm.u_name,
                                 v_name=prm.v_name,
                                 w_name=prm.w_name,
                                 t_name=prm.t_name,
                                 s_name=prm.s_name)

print("--- gen_nesting_file2 run time: %s seconds ---" % (time.time() - start_time))