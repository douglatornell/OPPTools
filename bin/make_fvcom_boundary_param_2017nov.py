# Control parameters for generation of FVCOM-NEMO nesting file.
# Supplied to make_fvcom_boundary.py

# Coupling files specific to the NEMO grid (full/cut) and FVCOM grid.
# Generated with Matlab routines.
#COUPLING_PATH = '/export/home/dunphym/OPP/vhfr_low_v2_RevB/input/coupling_nemocut/'
COUPLING_PATH = '/media/krassovskim/MyPassport/Max/Projects/opp/couple/'

# FVCOM grid files
#VHFR_PATH = '/export/home/dunphym/OPP/vhfr_low_v2_RevB/input/'
VHFR_PATH = '/media/krassovskim/MyPassport/Max/Projects/opp/couple/'

# Path to NEMO outputs where we find FVCOM_[U,V,T].nc 10-min files
#input_dir = '/export/home/dunphym/UBCFORCING/nowcast/'
input_dir = '/media/krassovskim/MyPassport/Data/opp/UBCFORCING/nowcast/'

# Time frame for the nesting file
# Since the outputs are output on 5min past the hour, we include
# one output before and one after the on-the-hour start date
fvcom_time_start = '2017-11-05 23:55:00'
fvcom_time_end   = '2017-11-11 00:05:00'

# Forcing ramp-up over nrampup NEMO outputs
nrampup = 3*24*6 # 3-day ramp-up at (24*6) outputs/day

# Option to generate barotropic or baroclinic forcing
opt = 'BRTP'
#opt = 'BRCL'

# Name for the forcing file to generate:
output_file      = VHFR_PATH+'test1_2017nov_'+opt.lower()+'.nc' # test_2017nov_brtp.nc


#======== Case-independent =============

# NEMO nodes corresponding to nesting zone (generated with nemo_nesting_zone.m)
nemo_nznodes_file = COUPLING_PATH+'nemo_nesting_zone_cut.txt'

# Nesting zone (generated by fvcom_nesting_zone.m)
nznodes_file  = COUPLING_PATH+'nesting-zone-utm10-nodes.txt' # fvcom grid nodes
nzcentr_file  = COUPLING_PATH+'nesting-zone-utm10-centr.txt' # fvcom element centroids

# Interpolant to interpolate U,V from NEMO grid to fvcom centroids in the nesting zone
import collections
Interp_uv = collections.namedtuple('interp_uv', ['ui','uj','uw','vi','vj','vw'])
interp_uv = Interp_uv(
        COUPLING_PATH+'interpolant_indices_u_i_cut.txt',
        COUPLING_PATH+'interpolant_indices_u_j_cut.txt',
        COUPLING_PATH+'interpolant_weights_u_cut.txt',
        COUPLING_PATH+'interpolant_indices_v_i_cut.txt',
        COUPLING_PATH+'interpolant_indices_v_j_cut.txt',
        COUPLING_PATH+'interpolant_weights_v_cut.txt')

# NEMO layer thickness for each grid point for u and v (generated with nemo_vertical_weight.m)
nemo_vertical_weight_file = COUPLING_PATH+'nemo_vertical_weight_cut.mat'

# NEMO grid orientation (generated with nemo_azimuth.m)
nemo_azimuth_file = COUPLING_PATH+'nemo_azimuth_cut.txt'

# Standard FVCOM grid files
fvcom_grd_file   = VHFR_PATH+'vhfr_low_v2_utm10_grd.dat'
fvcom_dep_file   = VHFR_PATH+'vhfr_low_v2_utm10_dep.dat'
fvcom_sigma_file = VHFR_PATH+'vhfr_low_v2_sigma.dat'
