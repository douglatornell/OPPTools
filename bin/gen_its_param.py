# Control parameters for generation of FVCOM-NEMO nesting file.
# Supplied to gen_its.py

# Coupling files specific to the FVCOM grid.
COUPLING_PATH = '/media/krassovskim/MyPassport/Max/Projects/opp/couple/'

# FVCOM grid files
FVCOM_GRID_PATH = '/media/krassovskim/MyPassport/Max/Projects/opp/couple/'

# NEMO grid files
NEMO_GRID_PATH = '/media/krassovskim/MyPassport/Data/opp/'


# Specify NEMO outputs where we find *T.nc files
# Option 1. Supply file list explicitly: nemo_file_list lists "T" NEMO output files.
search_dir_flag = False
nemo_file_list = ['/media/krassovskim/MyPassport/Data/opp/UBCFORCING/nowcast/21may18/FVCOM_T.nc',
                  '/media/krassovskim/MyPassport/Data/opp/UBCFORCING/forecast/21may18/FVCOM_T.nc']

# Option 2. Search the specified input_dir for files according to nemo_file_pattern.
# input_dir can be a list of directories.
#search_dir_flag = True
#input_dir = '/media/krassovskim/MyPassport/Data/opp/UBCFORCING/nowcast_baroclinic/11mar18'
##nemo_file_pattern = 'SalishSea_1h_*_grid_' # full nemo grid: search pattern would be 'SalishSea_1h_*_grid_T.nc'
#nemo_file_pattern = 'FVCOM_' # cut nemo grid: search pattern would be 'FVCOM_T.nc'


fvcom_time_start = '2018-05-22 00:00:00'

#t_name = None
#s_name = None
t_name = 'cons_temp'
s_name = 'ref_salinity'

# Name for the forcing file to generate:
output_file      = FVCOM_GRID_PATH+'test_2018may22'


#======== Case-independent =============

# Standard FVCOM grid files
fvcom_grd_file   = FVCOM_GRID_PATH+'vhfr_low_v2_utm10_grd.dat'
fvcom_dep_file   = FVCOM_GRID_PATH+'vhfr_low_v2_utm10_dep.dat'
fvcom_sigma_file = FVCOM_GRID_PATH+'vhfr_low_v2_sigma.dat'
file_exclude_poly = FVCOM_GRID_PATH+'exclude_polygon.txt' # needed for full SSC grid
fvcom_utmzone = 10

nemo_coord_file = NEMO_GRID_PATH+'coordinates_seagrid_SalishSea201702.nc'
nemo_mask_file  = NEMO_GRID_PATH+'mesh_mask201702.nc'
nemo_bathy_file = NEMO_GRID_PATH+'bathymetry_201702.nc'

# Indices limiting the output area. 
# For pyton array indexing: Zero-based, starting index inclusive, ending index exclusive.
# Use these variables in case NEMO output files in input_dir contain a trimmed area of 
# the full model grid defined in nemo_coord_file and nemo_mask_file. 
# Supply empty lists if the metrics files correspond to the output area.
nemo_cut_i = [225,369] # along x-axis
nemo_cut_j = [340,561] # along y-axis