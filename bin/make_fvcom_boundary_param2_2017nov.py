# Control parameters for generation of FVCOM-NEMO nesting file.
# Supplied to gen_nesting_file2.py

# Coupling files specific to the FVCOM grid.
#COUPLING_PATH = '/export/home/dunphym/OPP/vhfr_low_v2_RevB/input/coupling_nemocut/'
COUPLING_PATH = '/media/krassovskim/MyPassport/Max/Projects/opp/couple/'

# FVCOM grid files
#FVCOM_GRID_PATH = '/export/home/dunphym/OPP/vhfr_low_v2_RevB/input/'
FVCOM_GRID_PATH = '/media/krassovskim/MyPassport/Max/Projects/opp/couple/'

# NEMO grid files
NEMO_GRID_PATH = '/media/krassovskim/MyPassport/Data/opp/'

## Path to NEMO outputs where we find FVCOM_[U,V,T].nc 10-min files
##input_dir = '/export/home/dunphym/UBCFORCING/nowcast/'
#input_dir = '/media/krassovskim/MyPassport/Data/opp/UBCFORCING/nowcast/'
#
## Time frame for the nesting file
## Since the outputs are output on 5min past the hour, we include
## one output before and one after the on-the-hour start date
#fvcom_time_start = '2017-11-05 23:55:00'
#fvcom_time_end   = '2017-11-11 00:05:00'
#
## Forcing ramp-up over nrampup NEMO outputs
#nrampup = 3*24*6 # 3-day ramp-up at (24*6) outputs/day
#
## Option to generate barotropic or baroclinic forcing
#opt = 'BRTP'
#ua_name = 'ubarotropic'
#va_name = 'vbarotropic'
#u_name = None
#v_name = None
#w_name = None
#t_name = None
#s_name = None

#opt = 'BRTP'
opt = 'BRCL'
input_dir = '/media/krassovskim/MyPassport/Data/opp/UBCFORCING/nowcast_baroclinic/11mar18'
#nemo_file_pattern = 'SalishSea_1h_*_grid_' # full nemo grid: search pattern would be 'SalishSea_1h_*_grid_'+varname+'.nc'
nemo_file_pattern = 'FVCOM_*' # cut nemo grid: search pattern would be 'FVCOM_'+varname+'.nc'
fvcom_time_start = '2018-03-11 00:05:00'
fvcom_time_end   = '2018-03-11 23:55:00'
nrampup = 0 # no ramp-up
ua_name = None
va_name = None
u_name = 'uvelocity'
v_name = 'vvelocity'
w_name = 'wvelocity'
t_name = 'cons_temp'
s_name = 'ref_salinity'

# Name for the forcing file to generate:
#output_file      = FVCOM_GRID_PATH+'test2_2017nov_'+opt.lower()+'.nc' # test_2017nov_brtp.nc
output_file      = FVCOM_GRID_PATH+'test4_2018mar11_'+opt.lower()+'.nc' # test brcl


#======== Case-independent =============

# Standard FVCOM grid files
fvcom_grd_file   = FVCOM_GRID_PATH+'vhfr_low_v2_utm10_grd.dat'
fvcom_dep_file   = FVCOM_GRID_PATH+'vhfr_low_v2_utm10_dep.dat'
fvcom_sigma_file = FVCOM_GRID_PATH+'vhfr_low_v2_sigma.dat'
fvcom_utmzone = 10

# width of the zone where nesting weights transition from 0 to 1
rwidth = 8500. # metres

# parameters of the tanh function defining the transition profile
dl = 2
du = 2

nest_indices_file = COUPLING_PATH+'vhfr_low_v2_nesting_indices.txt'
nest_refline_file = COUPLING_PATH+'vhfr_low_v2_nesting_innerboundary.txt'

nemo_coord_file = NEMO_GRID_PATH+'coordinates_seagrid_SalishSea201702.nc'
nemo_mask_file  = NEMO_GRID_PATH+'mesh_mask201702.nc'
nemo_bathy_file = NEMO_GRID_PATH+'bathymetry_201702.nc'

# Indices limiting the output area. 
# For pyton array indexing: Zero-based, starting index inclusive, ending index exclusive.
# Use these variables in case NEMO output files in input_dir contain a trimmed area of 
# the full model grid defined in nemo_coord_file and nemo_mask_file. 
# Supply empty lists if the metrics files correspond to the output area.
nemo_cut_i = [225,369] # along x-axis
nemo_cut_j = [340,561] # along y-axis
