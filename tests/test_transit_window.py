""" Test transit_window()
"""
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
import OPPTools.utils as util

def test_transit_window():

    x = np.arange(0, 2*np.pi, 0.1)
    v = np.sin(x*2)
    
    t0 = dt.datetime(2019,3,1)
    t = np.array([t0 + dt.timedelta(hours=h) for h in range(len(v))])
    ts,te = util.transit_window(t,v,-.75,.75)
    
    plt.figure()
    plt.plot(t,v); plt.grid()
    plt.plot(np.c_[ts,ts].T,np.tile([-1, 1],(len(ts),1)).T,'--g')
    plt.plot(np.c_[te,te].T,np.tile([-1, 1],(len(te),1)).T,'--r')