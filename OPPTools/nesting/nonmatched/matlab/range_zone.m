function [kr,d] = range_zone(x,y,xp,yp,r)
% Linear indices for nodes within range r from the polyline xp,yp.
% xp,yp  can be NaN-separated

[~,d] = distance2curve([xp yp],[x y]);
in = d<=r;
kr = find(in);
d = d(in);
