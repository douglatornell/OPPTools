function fvcom_nesting_zone()

r = 50000; % 50 km

% fvcom grid with lon,lat coordinates for the nodes
fkit = '/media/user/My Passport/Data/fvcom/nemo/fvcom-NEMO-nesting/kit4_lonlat_grd.dat';
[lonn,latn,tri,z] = read_fvcom3_grd(fkit);
[xn,yn] = geo2utm(latn,lonn,9);
xc = mean(xn(tri),2);
yc = mean(yn(tri),2);
[latc,lonc] = utm2geo(xc,yc,9);

% x,y coordinates for the nodes (converted by Mike, not UTM9)
fxy = '/media/user/My Passport/Data/fvcom/nemo/fvcom-NEMO-nesting/kit4_dep.dat';
[xg,yg,zxy] = read_fvcom3_dep(fxy); % grid coordinates
% check:
disp(['Max diff between Z from grd and Z from dep: ' num2str(max(abs(z-zxy)))])
% centroids in grid coordinates
xe = mean(xg(tri),2);
ye = mean(yg(tri),2);


% open boundary polyline
ob = load('obc159'); % exported from Projects/Femgrids/nemo/kit4_openboundary.vg
xo = ob.r.x';
yo = ob.r.y';

% polygons to exclude from nesting zone
p = load('poly_exclude.mat');
p = p.r; % fields x,y must be present, they contain coords in lon,lat
npe = length(p);
pe = repmat(struct('x',[],'y',[]),1,npe);
for k = 1:npe
    % convert to UTM9 (excluding the trailing NaN)
    [pe(k).x,pe(k).y] = geo2utm(p(k).y(1:end-1), p(k).x(1:end-1), 9);
end    

find_zone(xn,yn,xo,yo,r,'nodes',lonn,latn,xg,yg,pe)
find_zone(xc,yc,xo,yo,r,'centr',lonc,latc,xe,ye,pe)


function find_zone(x,y,xo,yo,r,nodetype,lon,lat,xg,yg,pe)
% IDs (linear indices) for fvcom nodes within relaxation+transition zone

[k,d] = range_zone(x,y,xo,yo,r);

% exclude within specified polygons
for kp = 1:length(pe)
    in = inpolygon(x(k),y(k),pe(kp).x,pe(kp).y);
    k(in) = [];
    d(in) = [];
end

% output nesting zone nodes
fid = fopen(['nesting-zone-50km-' nodetype '.txt'],'w');
fprintf(fid,'%d %0.1f %0.6f %0.6f %0.2f %0.2f\n',[k d lon(k) lat(k)  xg(k) yg(k)]');
fclose(fid);
