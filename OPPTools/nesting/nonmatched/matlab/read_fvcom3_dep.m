function [x,y,z] = read_fvcom3_dep(fname)
% Read casename_dep.dat file from FVCOM input.

g = importdata(fname,' ',1); % 1 header line
g = g.data; % 3-column
x = g(:,1);
y = g(:,2);
z = g(:,3);