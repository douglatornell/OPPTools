function [au,av] = nemo_azimuth(fnemo)
% Local angle between nemo NEP036 grid lines and local meridian
% in degrees CW from north.
% Uses azimuth from mapping toolbox.

% fnemo = '/media/user/My Passport/Data/fvcom/nemo/Bathymetry_EastCoast_NEMO_R036_GEBCO_corrected_lat6_v12.nc';

lon = double(ncread(fnemo,'nav_lon'));
lat = double(ncread(fnemo,'nav_lat'));

av = azimuth(lat(:,1:end-1),lon(:,1:end-1),lat(:,2:end),lon(:,2:end));

% angle with the meridian, so it is comparable with av
au = azimuth(lat(1:end-1,:),lon(1:end-1,:),lat(2:end,:),lon(2:end,:)) - 90;


% figure;
% hp=pcolor(lon(:,1:end-1)',lat(:,1:end-1)',av');
% set(hp,'EdgeColor','none');
% grid;
% colorbar;
% set(gca,'DataAspectRatio',[1 cos(51.6*pi/180) 1],'Layer','top')
% title('NEP036 grid inclination (deg)')
% ylabel('Lat (deg)')
% xlabel('Lon (deg)')
% print('-dpdf','-r600','eps/nemo_azimuth.pdf')