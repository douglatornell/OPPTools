function m = nemo_mask(folder)
% Derive land masks from a bunch of NEMO output files.
% Mask criterion: Zero values for all times.
% Each mask has dimensions [x,y,nz], where nz is the number of layers for
% this variable in the vertical.

if ~exist('folder','var')
    folder = '/media/user/My Passport/Data/fvcom/nemo/NEP_FVCOM_backup';
end

ntype = 4; % 4 types of files
[m(1:ntype).type] = deal('T','U','V','W');
[m(1:ntype).var] = deal({'sossheig','votemper'},{'vozocrtx'},{'vomecrty'},{'vovecrtz'});
[m(1:ntype).mask] = deal({[],[]},{[]},{[]},{[]});
    
files = listfiles(folder,'nc');
nf = length(files);
tst = now;
hwb = waitbar(0,['Done 0 of ' num2str(nf) ';   ETA:']);
for kf = 1:nf
    typ = files(kf).name(end-9);
    it = [m.type]==typ;
    for kv = 1:length(m(it).var)
        v = ncread(files(kf).name,m(it).var{kv});
        % mask for this file: all zeros along the last (time) dimension
        mf = any(v~=0,ndims(v)); % ones for water, zeros for land
        if isempty(m(it).mask{kv})
            m(it).mask{kv} = mf; % initialize mask
        else
            m(it).mask{kv} = m(it).mask{kv} | mf; % update mask
        end
    end
    waitbar(kf/nf,hwb,['Done  ' num2str(kf) ' of ' num2str(nf) ';   ETA: ' datestr(now + (now-tst)*(nf-kf)/kf)])
end
delete(hwb)

% test for robustness of the method:
dm = m(1).mask{2}-m(4).mask{1}; % T and W masks should be the same
if any(dm(:))
    warning('The method is not robust: Masks for T and W are different.')
end
dm = m(1).mask{1}-m(1).mask{2}(:,:,1); % masks for zeta and T for the surface layer should be the same
if any(dm(:))
    warning('The method is not robust: Masks for zeta and T(surface) are different.')
end

casename = 'nemo_fvcom';
write_ncmask(m(1).mask{1},'node2d',casename)
write_ncmask(m(1).mask{2},'node3d',casename)
write_ncmask(m(2).mask{1},'u',casename)
write_ncmask(m(3).mask{1},'v',casename)
