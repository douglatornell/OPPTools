function  [x,y,utmzonedig,utmzonechar] = geo2utm(la,lo,utmzone)
% Convert lat,lon arrays to UTM coordinates (WGS84).
% utmzone   [optional]  either a 4-character string, e.g. '09 U' 
%           or a signed zone number (negative for southern hemisphere);
%           forces to use the supplied zone. By default it is determined
%           based on the central meridian and parallel for the dataset. 
%           UTM zone is uniform for the entire dataset.
% x,y           UTM coordinates, same size as la,lo
% utmzonechar   UTM zone used for conversion in the form of 4-char string
% utmzonedig    UTM zone numeric value, negative for southern hemisphere
%
% Maxim Krassovski 12-Sep-2011
%
% To implement the use of other reference ellipsoids see function calcoord 
% in utemization\utemization.m or utm\UTM.m
%
% Based on deg2utm by Rafael Palacios (with code vectorization);
% deg2utm uses parts of the code extracted from UTM.m function by Gabriel
% Ruiz Martinez.

% Argument check
error(nargchk(2, 3, nargin));  % 2 or 3 arguments required
n = length(la);
if n~=length(lo)
   error('Lat and Lon vectors should have the same length');
end

sa = 6378137.000000;
sb = 6356752.314245;

e2 = ( (sa^2 - sb^2) ^ 0.5 ) / sb;
e2cuadrada = e2 ^ 2;
c = sa^2 / sb;

lat = la*pi/180;
lon = lo*pi/180;

la = min(la(:))+range(la(:))/2; % centre parallel for the dataset
lo = min(lo(:))+range(lo(:))/2; % centre meridian for the dataset

if exist('utmzone','var') && ~isempty(utmzone) && all(~isnan(utmzone))
    if iscell(utmzone)
        utmzone = utmzone{1}; %%% use only the first supplied value
    end
    if ischar(utmzone) 
        if length(utmzone)==4
            utmzone(end) = []; % strip the letter
        end
        try
            Huso = str2double(utmzone);
        catch %#ok<CTCH>
            error('Unrecognized UTM zone string supplied.')
        end
    else
        Huso = abs(utmzone); % strip the sign
    end
    if Huso<1 || Huso>60
        error('Invalid UTM zone supplied.')
    end
else % no valid UTM zone supplied 
    Huso = fix(lo/6 + 31); % UTM zone number for the centre meridian
end

% UTM zone letter for the centre parallel
if (la<-72), Letra='C';
elseif (la<-64), Letra='D';
elseif (la<-56), Letra='E';
elseif (la<-48), Letra='F';
elseif (la<-40), Letra='G';
elseif (la<-32), Letra='H';
elseif (la<-24), Letra='J';
elseif (la<-16), Letra='K';
elseif (la<-8), Letra='L';
elseif (la<0), Letra='M';
elseif (la<8), Letra='N';
elseif (la<16), Letra='P';
elseif (la<24), Letra='Q';
elseif (la<32), Letra='R';
elseif (la<40), Letra='S';
elseif (la<48), Letra='T';
elseif (la<56), Letra='U';
elseif (la<64), Letra='V';
elseif (la<72), Letra='W';
else Letra='X';
end

utmzonechar = [num2str(Huso,'%02d') ' ' Letra];
if la<0
    utmzonedig = -Huso;
else
    utmzonedig = Huso;
end

% calculations
S = (Huso*6 - 183);
deltaS = lon - S*pi/180;
a = cos(lat) .* sin(deltaS);
epsilon = 0.5 * log( (1+a) ./ (1-a) );
nu = atan( tan(lat) ./ cos(deltaS) ) - lat;
v = 0.9996 * c ./ (1 + e2cuadrada*cos(lat).^2).^0.5;
ta = e2cuadrada/2 * epsilon.^2 .* cos(lat).^2;
a1 = sin(2*lat);
a2 = a1 .* cos(lat).^2;
j2 = lat + a1/2;
j4 = (3*j2 + a2)/4;
j6 = (5*j4 + (a2 .* cos(lat).^2) ) / 3;
alfa = 3/4*e2cuadrada;
beta = 5/3 * alfa^2;
gama = 35/27 * alfa^3;
Bm = 0.9996 * c * ( lat - alfa.*j2 + beta.*j4 - gama.*j6 );
x = epsilon .* v .* (1 + ta/3) + 500000;
y = nu .* v .* (1 + ta) + Bm;

if la<0
    y = 9999999 + y;
end
% y(y<0)=9999999+y(y<0);