"""
Interpolation of NEMO solution to FVCOM nesting zone.

The program needs a parameter file (example file is fvcom_NEMO_control.py).
If the parameter file is fvcom_NEMO_control.py, then the program is run like this:
python fvcom-NEMO-interp_UV.py fvcom_NEMO_control

Pramod Thupaki
Maxim Krassovski
2016
"""

import sys
import numpy as np 
from netCDF4 import Dataset as ncdata 
# import matplotlib.pyplot as plt 
import nemo_fvcom_toolkit as nft
import fvcomToolbox as ft 
from mpl_toolkits.basemap import Basemap as mapp 


def nemofolder2nestingzone(input_dir,output_prefix,nznodes_file,nzcentr_file,nemo_azimuth_file,
                           mask_n_file,mask_u_file,mask_v_file,opt = 'BRTP'):
    '''
    Interpolate NEMO variables from all files in the specified folder to FVCOM nodes.
    '''
    
    # obc_nodes_lon, obc_nodes_lat = get_fvcom_obc(grid_fvcom,fobc_file)
    nest_nodes_lon, nest_nodes_lat = get_fvcom_nesting_zone(nznodes_file)
    nest_centr_lon, nest_centr_lat = get_fvcom_nesting_zone(nzcentr_file)

    # land masks for the NEMO domain    
#    ncfile = ncdata(mask2d_file); mask_2d = ncfile.variables['mask'][:]; ncfile.close() # use upper layer from mask_3d
    ncfile = ncdata(mask_n_file); mask_n = ncfile.variables['mask'][:]; ncfile.close()
    ncfile = ncdata(mask_u_file); mask_u = ncfile.variables['mask'][:]; ncfile.close()
    ncfile = ncdata(mask_v_file); mask_v = ncfile.variables['mask'][:]; ncfile.close()
    
    # process each nemo file type
    process1type('T', input_dir, output_prefix, nest_nodes_lon, nest_nodes_lat, mask_n, opt=opt) # process SSH,T,S
    process1type('U', input_dir, output_prefix, nest_centr_lon, nest_centr_lat, [mask_u,mask_v], 
                 nemo_azimuth_file=nemo_azimuth_file) # process U and V files
    if opt == 'BRCL':
        process1type('W', input_dir, output_prefix, nest_nodes_lon, nest_nodes_lat, mask_n)
    

def process1type(vname, input_dir, output_prefix, obc_nodes_lon, obc_nodes_lat, mask, opt = 'BRTP', nemo_azimuth_file = ''):
    '''
    Interpolate NEMO variables from all files of one type in the specified folder to FVCOM nodes.
    The file type is specified by vname, which can be one of the following:
    T,U,W
    '''
    
    flist = nft.list_nemo_output(vname, input_dir)
    print 'Number of Files to be processed:', len(flist)
    
    if vname=='T':
        process_t(flist,output_prefix,obc_nodes_lon,obc_nodes_lat,mask,opt) # processes T,S,SSH
    elif vname=='U':
        nemovar = ['vozocrtx','vomecrty'] # var name in the NEMO nc file
        process_v(['U','V'],nemovar,flist,output_prefix,obc_nodes_lon,obc_nodes_lat,mask,nemo_azimuth_file)
    elif vname=='W':
        nemovar = 'vovecrtz'
        process_w(vname,nemovar,flist,output_prefix,obc_nodes_lon,obc_nodes_lat,mask)


def process_t(flist,output_prefix,obc_nodes_lon,obc_nodes_lat,mask,opt):
    """
    Interpolate NEMO variables T,S,SSH from T-files in the list using interpolant nlist,ndist.
    """
    
    # get NEMO coordinates from the 1st data file in the list
    nemo_nodes_file = flist[0]
    nemo_lon, nemo_lat = get_nemo_nodes(nemo_nodes_file)
    
    # interpolant for inverse distance interpolation    
    nlist, ndist = setup_idw(obc_nodes_lon, obc_nodes_lat, nemo_lon, nemo_lat)
    
    for fname in flist:
        print fname
        
        # open output files for the current input file
        time_id = '_'+fname[-33:-16] # 20130915_00000720   - time ID for each file
        fo_z   = open(output_prefix+time_id+'.zsl', 'w')
        if opt == 'BRCL':
            fo_tsl = open(output_prefix+time_id+'.tsl', 'w')
            fo_ssl = open(output_prefix+time_id+'.ssl', 'w')
    
        ncfile = ncdata(fname)
        ntimes = len( ncfile.dimensions['time_counter'] )
        fo_z.write('1 \n') # write 0 ndepth to single-layer varaible
        if opt == 'BRCL':
            zlev = ncfile.variables['deptht'][:]
            ndeptht= len( ncfile.dimensions['deptht'] )
            fo_tsl.write('{:d} \n'.format(ndeptht))
            fo_ssl.write('{:d} \n'.format(ndeptht))
        
        for t in range( ntimes ):
            time = ncfile.variables['time_counter'][t]
            print 'time = ', time
            
            nemo_ssh = ncfile.variables['sossheig'][t, :, :]
            nemo_ssh[mask[0,:,:]==0] = np.nan # apply land mask (upper layer = surface)
            z_intp = nft.idwIntp(nlist, ndist, nemo_ssh.flatten('F'), 2.0, excludeZeros = False)
            fo_z.write('{:7.2f} {:6.0f} '.format(0., time))
            write_line(fo_z, z_intp)
            
            if opt == 'BRCL':
                for d in range( ndeptht ):
                    nemo_wtmp = ncfile.variables['votemper'][t, d, :, :]
                    nemo_sals = ncfile.variables['vosaline'][t, d, :, :]
                    nemo_wtmp[mask[d,:,:]==0] = np.nan # apply land mask
                    nemo_sals[mask[d,:,:]==0] = np.nan # apply land mask
                    t_intp = nft.idwIntp(nlist, ndist, nemo_wtmp.flatten('F'), 2.0, excludeZeros = False)
                    s_intp = nft.idwIntp(nlist, ndist, nemo_sals.flatten('F'), 2.0, excludeZeros = False)
                    
                    fo_tsl.write('{:7.2f} {:6.0f} '.format(zlev[d], time))
                    fo_ssl.write('{:7.2f} {:6.0f} '.format(zlev[d], time))
                    
                    write_line(fo_tsl, t_intp)
                    write_line(fo_ssl, s_intp)
                
        ncfile.close()
        fo_z.close()
        if opt == 'BRCL':
            fo_tsl.close()
            fo_ssl.close()
        

def process_v(vname,nemovar,flist,output_prefix,obc_nodes_lon,obc_nodes_lat,mask,nemo_azimuth_file):
    """
    Interpolate a NEMO cetor variable (U,V) from files in the list using interpolant nlist,ndist.
    vname       file type: U, V as a list of strings, e.g. ['U','V']
    nemovar     corresponding varaible name in the NEMO nc files
    """
    
    # get NEMO coordinates from the 1st data file in the list
    nemo_nodes_fileu = flist[0]
    nemo_nodes_filev = get_nemo_pair(nemo_nodes_fileu,vname[1])
    
    nemo_lonu, nemo_latu = get_nemo_nodes(nemo_nodes_fileu)
    nemo_lonv, nemo_latv = get_nemo_nodes(nemo_nodes_filev)
    
    # interpolant for inverse distance interpolation
    nlistu, ndistu = setup_idw(obc_nodes_lon, obc_nodes_lat, nemo_lonu, nemo_latu)
    nlistv, ndistv = setup_idw(obc_nodes_lon, obc_nodes_lat, nemo_lonv, nemo_latv)
    depdimstr = 'depth'+vname[0].lower() # depth dimension is called differently for different variables
    nobc = np.size(obc_nodes_lon)
    
    theta   = np.genfromtxt(nemo_azimuth_file,delimiter=',').T # grid rotation
    for fname in flist:
        print fname
        
        # open output files for the current input file
        time_id = '_'+fname[-33:-16] # 20130915_00000720   - time ID for each file
        fo_usl = open(output_prefix+time_id+'.'+vname[0].lower()+'sl', 'w')
        fo_vsl = open(output_prefix+time_id+'.'+vname[1].lower()+'sl', 'w')
#        fo_ua = open(output_prefix+time_id+'.'+vname[0].lower()+'asl', 'w')
#        fo_va = open(output_prefix+time_id+'.'+vname[1].lower()+'asl', 'w')
        
        ncfileu = ncdata(fname)
        ncfilev = ncdata(get_nemo_pair(fname,vname[1]))
        ntimes = len( ncfileu.dimensions['time_counter'] )
        ndepth = len( ncfileu.dimensions[depdimstr] )
        zlev = ncfileu.variables[depdimstr][:]
        fo_usl.write('{:d} \n'.format(ndepth))
        fo_vsl.write('{:d} \n'.format(ndepth))
#        fo_ua.write('1 \n')
#        fo_va.write('1 \n')
        for t in range( ntimes ):
            time = ncfileu.variables['time_counter'][t]
            print 'time = ', time
#            uc = np.empty([ndepth, nobc]) # init array to collect values for all z
#            vc = np.empty([ndepth, nobc])
            for d in range( ndepth ):
                u = ncfileu.variables[nemovar[0]][t, d, :, :]
                v = ncfilev.variables[nemovar[1]][t, d, :, :]
                ur,vr = ft.rotate(u,v,-theta)
                ur[mask[0][d,:,:]==0] = np.nan # apply land mask for u
                vr[mask[1][d,:,:]==0] = np.nan # apply land mask for v
                u_intp = nft.idwIntp(nlistu, ndistu, ur.flatten('F'), 2.0, excludeZeros = False)
                v_intp = nft.idwIntp(nlistv, ndistv, vr.flatten('F'), 2.0, excludeZeros = False)
                fo_usl.write('{:7.2f} {:6.0f} '.format(zlev[d], time))  #######comment out temporary: for ua,va test
                fo_vsl.write('{:7.2f} {:6.0f} '.format(zlev[d], time))
                write_line(fo_usl, u_intp)  #######comment out temporary: for ua,va test
                write_line(fo_vsl, v_intp)
#                uc[d,:] = u_intp # collect for further depth-averaging
#                vc[d,:] = v_intp
#TODO: Do it properly if ua,va are needed. Average the fluxes. Besides, the code below gives all nans (nanmean is needed).
#            # average across depth, weighted by layer thickness
#            ua = np.mean(uc, axis=0)
#            va = np.mean(vc, axis=0)
#            fo_ua.write('{:7.2f} {:6.0f} '.format(0., time))
#            write_line(fo_ua, ua)
#            fo_va.write('{:7.2f} {:6.0f} '.format(0., time))
#            write_line(fo_va, va)
        ncfileu.close()
        ncfilev.close()
        fo_usl.close()
        fo_vsl.close()
#        fo_ua.close()
#        fo_va.close()
    
    
def get_nemo_pair(fname,vname):
    """
    For vector data, find a matching component file, e.g. for U-file, find V-file:
    Replace 10th character from the back with vname.
    Assuming naming conventions:
    NEP036-N07_IN_20130915_00000720_grid_U_FVCOM.nc
    NEP036-N07_IN_20130915_00000720_grid_V_FVCOM.nc
    """

    return fname[:-10] + vname + fname[-9:]
    
    
def process_w(vname,nemovar,flist,output_prefix,obc_nodes_lon,obc_nodes_lat,mask):
    """
    Interpolate the specified NEMO scalar variable from files in the list using interpolant nlist,ndist.
    vname       file type: W
    nemovar     corresponding varaible name in the NEMO nc files
    """
    
    # get NEMO coordinates from the 1st data file in the list
    nemo_nodes_file = flist[0]
    nemo_lon, nemo_lat = get_nemo_nodes(nemo_nodes_file)
    
    # interpolant for inverse distance interpolation    
    nlist, ndist = setup_idw(obc_nodes_lon, obc_nodes_lat, nemo_lon, nemo_lat)
    
    depdimstr = 'depth'+vname.lower() # depth dimension is called differently for different variables
    
    for fname in flist:
        print fname
        
        # open output files for the current input file
        time_id = '_'+fname[-33:-16] # 20130915_00000720   - time ID for each file
        fo_vsl = open(output_prefix+time_id+'.'+vname.lower()+'sl', 'w')
        
        ncfile = ncdata(fname)
        ntimes = len( ncfile.dimensions['time_counter'] )
        ndepth = len( ncfile.dimensions[depdimstr] )
        zlev = ncfile.variables[depdimstr][:]
        fo_vsl.write('{:d} \n'.format(ndepth))
        for t in range( ntimes ):
            time = ncfile.variables['time_counter'][t]
            print 'time = ', time
            for d in range( ndepth ):
                w = ncfile.variables[nemovar][t, d, :, :]
                w[mask[d,:,:]==0] = np.nan # apply land mask
                w_intp = nft.idwIntp(nlist, ndist, w.flatten('F'), 2.0, excludeZeros = False)                
                fo_vsl.write('{:7.2f} {:6.0f} '.format(zlev[d], time))
                write_line(fo_vsl, w_intp, precision = 'e') # use e notation, since w can be small
                
        ncfile.close()
        fo_vsl.close()


def get_fvcom_nesting_zone(nestingzone_file):
    lonlat   = np.genfromtxt(nestingzone_file, usecols = (2, 3))
    lon = lonlat[:,0]
    lat = lonlat[:,1]
    return lon, lat


def get_fvcom_obc(grid_fvcom,fobc_file):
    #~ ========================================================        read fvcom grid
    fvcom_nv, fvcom_lonlat = nft.read_grd( grid_fvcom )
    obc_nodes   = np.genfromtxt(fobc_file, skip_header = 1, usecols = (1), dtype = 'int')
    obc_nodes_lonlat = fvcom_lonlat[obc_nodes - 1, :]
    obc_nodes_lon = obc_nodes_lonlat[:,0]
    obc_nodes_lat = obc_nodes_lonlat[:,1]
    return obc_nodes_lon, obc_nodes_lat


def get_nemo_nodes(nemo_nodes_file):
    #~ ==========================================================================
    #~ read NEMO grid, setup idw neighbors and distance list
    #~ ==========================================================================
    print nemo_nodes_file 
    ncfile = ncdata(nemo_nodes_file,'r')
    nemo_lon = ncfile.variables['nav_lon'][:, :]
    nemo_lat = ncfile.variables['nav_lat'][:, :]
    ncfile.close()
    nemo_lon = nemo_lon.flatten('F')
    nemo_lat = nemo_lat.flatten('F')
    return nemo_lon, nemo_lat


def setup_idw(obc_nodes_lon, obc_nodes_lat, nemo_lon, nemo_lat):
    #~ ========================================================        setup IDW matrices
    # m = mapp(width=12000000,height=9000000,projection='lcc', resolution='c',lat_1=45.,lat_2=55,lat_0=50,lon_0=-107.)
    m = mapp(projection='poly', resolution=None,llcrnrlon = -130., llcrnrlat = 45., urcrnrlon = -100., urcrnrlat = 60., lat_0 = 50, lon_0 = -107.)
    [obc_x, obc_y]  = m( obc_nodes_lon, obc_nodes_lat )
    [nemo_x,nemo_y] = m( nemo_lon, nemo_lat )
    
    obcxy  = np.vstack((obc_x, obc_y)).transpose()
    nemoxy = np.vstack((nemo_x, nemo_y)).transpose()
    print np.shape(obcxy), np.shape(nemoxy )
    nlist, ndist = nft.idwSetup(nemoxy, obcxy, 5000.00)
    return nlist, ndist


def write_line(fo, values, precision = '0.3f'):
    formatstr = '{:'+str(precision)+'} '
    for v in values:
        fo.write(formatstr.format(v))
    fo.write('\n')


if __name__ == '__main__':

#    # input_dir = '/media/user/My Passport/Data/fvcom/nemo/NEP_FVCOM_backup'
#    kit_path    = '/media/user/My Passport/Data/fvcom/nemo/fvcom-NEMO-nesting/'
##    input_dir = kit_path+'interp_test'
##    input_dir = '/media/user/My Passport/Data/fvcom/nemo/NEP_FVCOM_backup' # 2013 test case
#    input_dir = '/media/user/My Passport/Data/fvcom/nemo/NEMO_NEP036_2015' # 2015 test case
#    # output_prefix = 'tsobc_input/NEP036_TS_intp'
##    output_prefix = kit_path+'nesting_zone_interp/NEP036_intp' # 2013 test case
#    output_prefix = kit_path+'nesting_zone_interp_2015/NEP036_intp' # 2015 test case
#    # fobc_file   = 'kit4_obc.dat'
#    # nestingzone_file   = 'nesting-zone-50km.txt'
#    nznodes_file = kit_path+'nesting-zone-50km-nodes.txt' # grid nodes
#    nzcentr_file = kit_path+'nesting-zone-50km-centr.txt' # element centroids
#    nemo_azimuth_file = kit_path+'nemo_fvcom_azimuth.txt' # nemo grid orientation (generated by nemo_fvcom_azimuth.m)
##    mask2d_file = kit_path+'nemo_fvcom_node2d_mask.nc' # use upper layer from node3d_mask
#    mask_n_file = kit_path+'nemo_fvcom_node3d_mask.nc'
#    mask_u_file = kit_path+'nemo_fvcom_u_mask.nc'
#    mask_v_file = kit_path+'nemo_fvcom_v_mask.nc'
#
#    nemofolder2nestingzone(input_dir,output_prefix,nznodes_file,nzcentr_file,nemo_azimuth_file,
#                           mask_n_file,mask_u_file,mask_v_file)
#    # sys.exit()

    # import parameters
    prm = __import__(sys.argv[1])
    
    nemofolder2nestingzone(prm.input_dir, prm.output_prefix, 
                           prm.nznodes_file, prm.nzcentr_file, prm.nemo_azimuth_file, 
                           prm.mask_n_file, prm.mask_u_file, prm.mask_v_file, prm.opt)