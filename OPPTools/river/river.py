"""
Routines to generate FVCOM river forcing files.
"""

import os
import glob
import numpy as np
import datetime
import pytz
import calendar
import yaml
import pandas as pd
import netCDF4 as nc
from OPPTools import nesting as nesting
import OPPTools.fvcomToolbox as fvt

def generate_river_forcing(param_yaml):
    """ Generates river forcing file according to parameters specified in param_yaml.
    
    Parameters
    ----------

    param_yaml : str
        Parameters file name; example file: bin/river_param_example.yaml
    """
    
    # get parameters from yaml file
    param = yaml.load(open(param_yaml, 'r'))
    rivers = param['rivers']
    
    timelim = [datetime.datetime.strptime(t1,'%Y-%m-%d %H:%M:%S') 
               for t1 in param['timelim']]
    
    # ensure timelim is tz-aware; assign UTC zone by default
    if timelim[0].tzinfo is None:
        timelim = [pytz.utc.localize(k) for k in timelim]
    
    # get river flow and temperature; resample to desired freq
    d = []
    for rvr in rivers:
        d.append(get_flow(rvr, timelim, dtmin=param['dtmin'], kwargs=rivers[rvr]['kwargs']))
    
    # unify sampling:
    # combine time vectors
    d = pd.concat(d, axis=1)
    # interpolate each sereis to combined time with extrapolation to outside nans
    d = d.interpolate(limit_direction='both')
    
    q = d['flow [m^3/s]'] # all flow columns
    t = d['temperature [deg C]'] # all temp columns
    
    # single river case: need to explicitely convert to dataframe
    if type(q) is pd.core.series.Series:
        q = q.to_frame()
    if type(t) is pd.core.series.Series:
        t = t.to_frame()
    
    # name columns with river names
    q.columns = rivers.keys()
    t.columns = rivers.keys()
    
    # copy temperature from the specified river, if any
    for rvr in rivers:
        if rivers[rvr]['temperature_source'] is not None:
            t[rvr] = t[rivers[rvr]['temperature_source']]
    
    # distribute over river inflow nodes
    nodes = yaml.load(open(param['nodes file'], 'r'))
    rivName = []
    rivLoc = []
    qdis = []
    tdis = []
    for k,rvr in enumerate(rivers):
        try:
            loc = nodes[rvr] # 1-based fvcom node indices
        except KeyError:
            raise KeyError("River nodes file doesn't have river: "+rvr)
        nriv = len(loc)
        qdis.append(discharge_split(q[rvr],nriv)) # (ntimes, nRiv)
        tdis.append(np.tile(t[rvr][:,None],nriv)) # replicate temperature
        
        # generate unique names from river name and node number
        rivName += [rvr+str(rivnode) for rivnode in loc]
        rivLoc += loc
    qdis = np.hstack(qdis)
    tdis = np.hstack(tdis)
    
    # time should cover the entire interval of the model run
    time_str = [t1.strftime('%Y-%m-%d %H:%M:%S') for t1 in d.index]
        
    # write forcing file
    fvt.generate_riv(param['casename'],time_str,rivLoc,qdis,
                     temp=tdis,
                     rivName=rivName,
                     namelist_file=param['namelist_file'])


def get_flow(riv_name,timelim,dtmin=None,kwargs=None):
    """ Get flow and temperature data for rivers in Vancouver Harbour region.
    
    Parameters
    ----------
    
    riv_name : str
        One of fraser,capilano,lynn,seymour
    timelim : list of datetime
        Time interval to cover
    dtmin : int, optional
        Target sampling interval (minutes)
    kwargs : dict
        Keyword arguments for the corresponding data aquisition function
    """
    switcher = {
        'fraser'    : get_fraser,
        'capilano'  : get_archived,
        'lynn'      : get_archived,
        'seymour'   : get_archived
    }
    # Get the function from switcher dictionary
    func = switcher.get(riv_name.split()[0].lower(), lambda: "Invalid river name.")
    return func(timelim,dtmin=dtmin,**kwargs)

def get_fraser(timelim,
               dtmin=None,
               nemo_river_files=None,
               nemo_coord_file=None,
               river_constemp_file=None):
    """ Helper to get Fraser flow data.
    
    Parameters
    ----------
    
    timelim : list of datetime
        Time interval to cover
    dtmin : int, optional
        Dummy parameter for consistency with other flow-getting functions.
        The returned series is daily.
    nemo_river_files : list or dict
        Either 
            - list of file names or
            - dict with search_path and search_pattern fields which will be 
              used in glob recursive search
    nemo_coord_file,
    river_constemp_file :
        Parameters for nemo_fraser
    
    Returns
    -------
    
    d : pandas.DataFrame
        Flow and temperature series
    """
    timelim_str = [k.strftime('%Y-%m-%d %H:%M:%S') for k in timelim]
    
    if type(nemo_river_files) is dict:
        # get available data files
        # needs Python 3.5 or newer: ** and recursive=True are for recursive search
        nemo_river_files = glob.glob(
                os.path.join(nemo_river_files['search_path'],'')+'**/'+\
                             nemo_river_files['search_pattern'], 
                recursive=True)
    
    time,q,temp = nemo_fraser(timelim_str,
                              nemo_river_files,
                              nemo_coord_file,
                              river_constemp_file)
    
    d = pd.DataFrame({'flow [m^3/s]':q,'temperature [deg C]':temp},index=time)
    d.index = d.index.tz_localize('UTC')
    
    return d

def get_archived(tlim,dtmin=None,arch_file=None):
    """ Helper to get flow data from a csv archive file.
    
    For file format see read_archive.
    
    Parameters
    ----------
    timelim : list of datetime
        Time interval to cover
    dtmin : int, optional
        Target sampling interval (minutes)
    arch_file : str
        Data file name
        
    Returns
    -------
    
    d : pandas.DataFrame
        Flow and temperature series. Temperature is all-nan; it has to be
        filled from other sources.
    """
    d = series_cover(read_archive(arch_file), tlim, dtmin=dtmin)
    d['temperature [deg C]'] = np.nan
    return d

def read_archive(arch_file):
    """ Read archived flow time series from a scv file.
    
    File format example:
        
    time [UTC],flow [m^3/s]
    2016-08-01 07:00:00+00:00,1.967508
    2016-08-01 07:05:00+00:00,2.031816
    
    Parameters
    ----------
    
    arch_file : str
        Data file name
        
    Returns
    -------
    
    d : pandas.Series object
        Flow time series
    """
    d = pd.read_csv(arch_file, index_col=0, parse_dates=[0])
    timezone = d.index.name.split('[')[-1].split(']')[0] # extract time zone
    d.index = d.index.tz_localize(timezone)
    return d


def nemo_fraser(timelim_str,nemo_river_files,nemo_coord_file,river_constemp_file):
    """ Fraser River discharge from a series of SalishSea NEMO river forcing files.
    
    Parameters
    ----------
    
        timelim_str : length 2 list of str
            FVCOM start and end time; format: '2017-05-31 00:00:00'
        nemo_river_files : list of str
            Series of NEMO river forcing files; should be sorted in time
        nemo_coord_file : str
            coordinates_seagrid_SalishSea201702.nc with full path
        river_constemp_file : str
            File with climatological temperatures for NEMO river forcing, 
            rivers_ConsTemp_month.nc
            
    Returns
    -------
    
        timeq : datetime
            Time for discharge values q
        q : array-like
            Fraser River discharge in (m^3 s-1)
        temp : array-like
            Temperature for discharge times
    """
    
    [j,i] = find_nemo_fraser(nemo_river_files[0],nemo_coord_file) # 500,394 # Fraser
    
    # coefficient to convert discharge to [m^3 s^-1]
    unitconv = discharge_nemo2fvcom(j,i,nemo_coord_file)
    
    # get time from file names, e.g. R201702DFraCElse_y2017m05d31.nc
    # use [-13:-3] part of the file name, e.g. 2017m05d31
    # and shift to the middle of the day
    timeq = np.array([datetime.datetime.strptime(f1[-13:-3],'%Ym%md%d')+datetime.timedelta(hours=12) 
        for f1 in nemo_river_files])
    
    time = np.array([datetime.datetime.strptime(t1,'%Y-%m-%d %H:%M:%S') for t1 in timelim_str])
    
    # sort by time
    isort = np.argsort(timeq)
    timeq = timeq[isort]
    nemo_river_files = [nemo_river_files[k] for k in isort]    
    
    # select files that cover fvcom run time
    istart = np.where(timeq<=time[0])[0]
    if istart.size == 0:
        istart = 0
    else:
        istart = istart.max()
    
    iend = np.where(timeq>=time[-1])[0]
    if iend.size == 0:
        iend = timeq.size
    else:
        iend = iend.min()+1
    
    timeq = timeq[istart:iend]
    
    # read discharge
    fi = range(istart,iend)
    q = np.nan*np.ones(len(fi)) # allocate
    for n,k in enumerate(fi): # read only required
        # Fraser, one value per file
        print('Reading: ',nemo_river_files[k])
        q[n] = nc.Dataset(nemo_river_files[k])['rorunoff'][0,j,i]
    
    q = q*unitconv # to [m^3 s^-1]
    
    # extrapolate to time limits if outside of coverage
    qs = series_cover(pd.Series(q,index=timeq),time)
    timeq = qs.index
    q = qs.values
    
    temp = nemo_river_temperature(river_constemp_file,nemo_coord_file,j,i,timeq)
    
    return timeq,q,temp


def find_nemo_fraser(nemo_river_file,nemo_coord_file):
    """ Hack: find the Fraser River node in SalishSea NEMO grid as the largest runoff value
    Inputs:
        nemo_river_file     str, a NEMO river forcing file
        nemo_coord_file     coordinates_seagrid_SalishSea201702.nc with full path
    Outputs:
        j,i   indices in NEMO grid for the Fraser runoff node
    """
    
    ncf = nc.Dataset(nemo_river_file)
    q1 = ncf['rorunoff'][0,:,:] # entire grid
    [j,i] = np.unravel_index(np.argmax(q1),q1.shape) # 500,394 # Fraser
    return j,i


def discharge_nemo2fvcom(j,i,nemo_coord_file):
    """ Coefficient to convert discharge to [m^3 s^-1] for (j,i) node of NEMO grid.
    Inputs:
        j,i                 indices in NEMO grid
        nemo_coord_file     coordinates_seagrid_SalishSea201702.nc with full path
    Outputs:
        unitconv    coefficient to convert discharge to [m^3 s^-1]: Qfvcom = Qnemo*unitconv
    """
    
    ncg = nc.Dataset(nemo_coord_file)
    e1t = ncg['e1t'][0,j,i]
    e2t = ncg['e2t'][0,j,i]
    unitconv = e1t*e2t / 1000. # cellarea / w.dens kg/m^3
    return unitconv


def discharge_split(q,nriv):
    """ Distribute discharge q evenly over nriv nodes.
    Inputs:
        q       (ntimes,) 1d array of discharge values
        nriv    scalar, number of river nodes for this discharge value
    Outputs:
        qdis    (ntimes, nRiv) q distributed over the nodes
    """
    return np.tile(q/nriv,(nriv,1)).T.copy() # (ntimes, nRiv)


def nemo_river_temperature(river_constemp_file,nemo_coord_file,j,i,time):
    """ Get river temperature for specified grid cell from SalishSea river climatology.
    
    Returns
    -------
    
    In-situ temperature at grid point j,i
    """
    
    # climatology is for 12 months; assume values are at mid-month
    tempc = nc.Dataset(river_constemp_file)['rotemper'][:,j,i]
    tempc = np.r_[tempc[-1],tempc,tempc[0]] # extend climatology so it covers entire year
    
    # generate climatology times; use leap year
    timec = timeline_monthly(datetime.datetime(3,12,16), datetime.datetime(5,1,16), 16)
    
    # convert datetime to numeric values
    timeg  = np.array([calendar.timegm(k.timetuple()) for k in time])
    timecg = np.array([calendar.timegm(k.timetuple()) for k in timec])
    
    # interpolate to specified time
    tcons = np.interp(timeg,timecg,tempc)

    # coordinates are completely masked in river_constemp_file; use coord file
    ncf = nc.Dataset(nemo_coord_file)
    lon = ncf['nav_lon'][j,i]
    lat = ncf['nav_lat'][j,i]
    
    # convert to in-situ temperature
    t,_ = nesting.CtSr2TS(tcons,np.zeros(tcons.shape), 0, 0, 0, lon, lat)
    
    return t


def timeline_monthly(start, end, day_of_month):
    """ Series of monthly timestamps as datetime objects.
    
    Parameters
    ----------
    
    start, end : datetime
        Series bounds.
    day_of_month : int
        Day to use for monthly series, e.g. 1 to place time stamps at 1st of each month.
        
    Returns
    -------
    
    list of datetime
    """
    # Code from https://stackoverflow.com/questions/34898525/generate-list-of-months-between-interval-in-python/34899127
    
    total_months = lambda dt: dt.month + 12 * dt.year
    mlist = []
    for tot_m in range(total_months(start)-1, total_months(end)):
        y, m = divmod(tot_m, 12)
        mlist.append(datetime.datetime(y, m+1, day_of_month))
    return mlist


def series_cover(d,tlim,dtmin=None):
    """ Cover specified time interval by the data in time series.
    
    Trims time series d by time limits tlim, resamples to sampling interval no 
    lower than dtmin, with nearest neighbour extrapolation to the tlim that are 
    outside of the series coverage.
    
    Parameters
    ----------
    
    d : pandas.Series or pandas.DataFrame object
        Data series; d.index and tlim have to be both tz-naive or both tz-aware
    tlim : list of datetime
        Time interval to cover
    dtmin : int, optional
        Target sampling interval (minutes)
        
    Returns
    -------
    
    d : pandas.Series object
        Trimmed series
    """
    
    tz = d.index[0].tz
    
    # downsample if necessary
    if dtmin is not None:
        # pandas.Series.resample resamples with window BEGINNING at time stamps;
        # use custom resampling with window centered at time stamps
        dtmin_half = pd.Timedelta(str(dtmin/2)+'min')
        t1 = d.index[0].floor(str(dtmin)+'min')
        t2 = d.index[-1].ceil(str(dtmin)+'min')
        sampling = pd.DateOffset(minutes=dtmin)
        bins = pd.to_datetime(
                pd.date_range(
                        start = t1-dtmin_half, 
                        end   = t2+dtmin_half, 
                        freq  = sampling))
        labs = pd.to_datetime(pd.date_range(start=t1,end=t2,freq=sampling))
        dcut = pd.cut(pd.to_datetime(d.index), bins, labels=labs) # break in bins
        d = d.groupby(dcut).mean() # average within bins
        # d ends up with pandas.core.indexes.category.CategoricalIndex;
        # convert it to pandas.core.indexes.datetimes.DatetimeIndex
        d.index = pd.to_datetime(d.index)
        d = d.tz_localize(tz=tz) # restore original time zone
        # exclude rows with all nans 
        d = d.dropna(how='all')
        
    # add nans at tlim
    if type(d) is pd.core.series.Series:
        dl = pd.Series([np.nan,np.nan], index=tlim)
    else: # DataFrame
        dl = pd.DataFrame([],index=tlim,columns=d.columns)
    d = pd.concat([d,dl]).sort_index()
    # exclude nans if they duplicate values at existing times
    d = d.groupby(d.index).mean()
    # fill nans linearly with extrapolation to outside values
    d = d.interpolate(limit_direction='both')
    d = d.truncate(before=tlim[0], after=tlim[1])
    
    return d