"""
Routines to create FVCOM atmospheric forcing from HRDPS-West output.
"""

import numpy as np
import scipy.interpolate
import OPPTools.fvcomToolbox as fvt
from pyproj import Proj
import os
import glob
import pygrib
import datetime
import time
import warnings
import netCDF4 as nc


def create_atm_hrdps(ftype,x,y,tri,utmzone,tlim,fname,hrdps_folder,
                     constant_values=np.array([10, 101300, 70, 200, 250])):
    """
    Atmospheric forcing file from raw CMC HRDPS files downloaded from daily archive.
    
    Inputs:
    ftype   'wnd','hfx', or 'hfx_const'
            'hfx_const' doesn't use HRDPS output, but fills-in constant and uniform values
    x,y,tri fvcom grid
    utmzone for fvcom grid coordinates, a numeric value
    tlim    time bounds for the forcing time series; list of str, format: "2018-02-12 00:00:00"
    fname   name for the forcing file to create
    hrdps_folder    folder with HRDPS grib2 files (can be in subfolders, any number of levels deep)
    constant_values for ftype=='hfx_const'; the order of variables as in fvcom_vars below
    """
    
    if (ftype=='hfx'):
        hrdps_vars = [
            'TMP_TGL_2',
            'PRMSL_MSL_0',
            'RH_TGL_2',
            'DSWRF_SFC_0',
            'DLWRF_SFC_0']
#            'TCDC_SFC_0'
#            'PRATE_SFC_0'

        # in the same order:
        fvcom_vars = [
            'air_temperature',
            'air_pressure',
            'relative_humidity',
            'short_wave',
            'long_wave']
#            'cloud_cover'
#            'precip'
    elif (ftype=='hfx_const'):
        fvcom_vars = [
            'air_temperature',
            'air_pressure',
            'relative_humidity',
            'short_wave',
            'long_wave']
    elif (ftype=='wnd'):
        hrdps_vars = [
            'UGRD_TGL_10',
            'VGRD_TGL_10']
    
        # in the same order:
        fvcom_vars = [
            'U10',
            'V10']
    elif (ftype=='precip'):
        hrdps_vars = [
            'LHTFL_SFC_0',
            'PRATE_SFC_0']
        
        # in the same order:
        fvcom_vars = [
            'evap',
            'precip']
        
    # fvcom grid
    Mobj,lat,lon,latc,lonc,latcn,loncn = grid2mobj_utm(x,y,tri,utmzone)
    
    # convert to mktime format, seconds since epoc (1970-1-1 on unix)
    tlim_stamp = [time.mktime(time.strptime(s, "%Y-%m-%d %H:%M:%S")) for s in tlim]
    htime,_,_ = hrdps_timeline(tlim_stamp)
    ntime = len(htime)
        
    routine_name = 'create_atm_hrdps.py'
    ncfile = setup_fvcom_forcing(Mobj,fname,fvcom_vars,htime,routine_name)
    
    if (ftype=='hfx_const'):
        
        pl = np.ones( (Mobj['nVerts'], ntime), np.float64 ) # [nNodes, nTime]
        
        # put constant values for the varaibles
        for k in range(len(fvcom_vars)):
            ncfile.variables[fvcom_vars[k]][:,:] = constant_values[k]*pl
        
    else:
        # ensure a list
        if type(hrdps_folder) is str:
            hrdps_folder = [hrdps_folder]

        hrdps_it, theta = hrdps_iterator(hrdps_vars,htime,hrdps_folder,lon,lat,lonc,latc,latcn,loncn)
#        print('HRDPS series length: ',len(hrdps_it))
        
        # get HRDPS values
        tst = time.time()
        for kt in range(ntime):
            [v,hrdps_it] = hrdps_iterate(kt,hrdps_it)
            
            # rotate from HRDPS coordinate system to fvcom coordinate system
            if (ftype=='wnd'): # u,v are 1st two variables
                [v[0],v[1]] = rotate_vec(v[0],v[1],theta)
    
            append_fvcom_forcing(ncfile,kt,fvcom_vars,v)
    
            pdone = (kt+1)/ntime
            eta = tst + (time.time()-tst)/pdone
            print(routine_name,': ',pdone*100,'% done. ETA: ',time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(eta)))
    
    ncfile.close()


def hrdps_iterator(var,t,hrdps_folder,lon,lat,lonc,latc,latcn,loncn):
    """
    % Iterator to read data from CMC HRDPS files downloaded from daily archive
    % and interpolate them to nodes lon,lat and elements lonc,latc.
    % 
    % Inputs:
    % var       HRDPS varaibles to include, e.g. UGRD_TGL_10, PRMSL_MSL_0, etc.
    % t         t = hrdps_timeline(tlim), timeline to cover; mktime format, seconds since epoc
    % hrdps_folder    folder with HRDPS grib2 files
    % lon,lat   fvcom nodes to interpolate to
    % lonc,latc fvcom element centroids to interpolate to
    % latcn,loncn   fvcom element centroid coordinates shifted somewhat in 
    %               the northing direction to calculate wind vector rotation between 
    %               HRDPS and FVCOM grids
    %
    % Returns:
    % hrdps_it  iterator object
    % theta     rotation between HRDPS and FVCOM grids at lonc,latc
    """
    
    # projection where HRDPS-West grid is rectangular
    psproj = Proj("+proj=stere +a=6371229 +lon_0=-113 +lat_0=90 +lat_ts=60")
    
    # transform all cordinates to the projection where HRDPS grid is rectangular
    xin,yin = psproj(lon,lat)
    xie,yie = psproj(lonc,latc)
    xien,yien = psproj(loncn,latcn)
    
    theta = np.arctan2(xien-xie,yien-yie)*180./np.pi # degrees, CW from fvcom grid northing
    # theta is then used in rotate_vec(u,v,theta), where u,v are components in
    # hrdps coordinates
    
    nvar = len(var)
    hrdps_it = [] # a list of iterators for every variable
    
    hrdps_tlim = [min(t),max(t)]
    for k in range(nvar):
        
        flist = find_hrdps_files(hrdps_folder,var[k])
#        print('1st found hrdps file: ',flist[0])
#        print('last found hrdps file: ',flist[-1])
        
        fid = pygrib.open(flist[0])
        grib = fid[1] # grib is one-based
        # to check out all available info in the file: grib.keys()
        
        # determine if it is an accumulated variable
        accumulated = grib.stepTypeInternal == 'accum' # can be 'accum' or 'instant'
        
        nx = grib.Nx
        ny = grib.Ny
        
        glat,glon = grib.latlons()
        x,y = psproj(glon,glat) # x,y should be now on a rectangular grid
        
        # check x,y are on a regular grid
        if np.max(np.abs(np.diff(x,axis=0)))>1e-5 or np.max(np.abs(np.diff(y,axis=1)))>1e-5:
            raise ValueError('HRDPS grid did not transform to a rectangular shape.') 
        
        # un-meshgrid for interp2d
        x,y = x[0,:],y[:,0]
    
        if not accumulated:
            # for non-accum variables
            fname,tnac,_,_ = hrdps_listfiles(flist,hrdps_tlim,accumflag=False)
            it,w = interpolant(tnac,t)
            iterator = class_iterator(it,w,fname,class_stack(nx,ny))
        else:
            # for accumulated variables
            tdiff,ipairs,fname,tfor = hrdps_listfiles_accumdiff(flist,hrdps_tlim)
            it,w = interpolant(tdiff,t)
            iterator = class_iterator(it,w,fname,class_stack(nx,ny),ipairs,tfor)
            
        print('The following files will be used:')
        print(fname)
        
        if var[k] in ['UGRD_TGL_10','VGRD_TGL_10']:
            # element-based
            xi = xie;
            yi = yie;
        else:
            # node-based
            xi = xin;
            yi = yin;
        
        unitscale,unitsift = units_cmc2fvcom(var[k])
        xic,yic,icovered = find_covered(x,y,xi,yi)
        
        hrdps_it.append(class_hrdps_iterator(accumulated,nx,ny,unitscale,unitsift,iterator,x,y,xic,yic,icovered))
    
    return hrdps_it, theta


class class_stack:
    """ Stack for data from previously read files
    """
    def __init__(self,nx,ny):
        imax = 4 # stack length (two is enough for non-accum vars, but use same stack for both types)
        self.k = np.full(imax,np.nan) # time step pointer for the HRDPS series
        self.v = np.full((ny,nx,imax),np.nan) # values
        self.ipush = 0 # stack pointer (0-based), increments after each push and returns to 0 if ipush reaches imax-1
        self.imax = imax


class class_iterator:
    """ Class to hold iterator data
    """
    def __init__(self,it,w,fname,stack,ipairs=None,tfor=None):
        self.it = it
        self.w = w
        self.fname = fname
        self.stack = stack
        self.ipairs = ipairs
        self.tfor = tfor
        
        
class class_hrdps_iterator:
    """ Class to hold hrdps iterator data
    """
    def __init__(self,accumulated,nx,ny,unitscale,unitsift,iterator,x,y,xi,yi,icovered):
        self.accumulated = accumulated
        self.nx = nx
        self.ny = ny
        self.unitscale = unitscale
        self.unitsift = unitsift
        self.iterator = iterator
        self.x = x   # HRDPS coords
        self.y = y
        self.xi = xi # fvcom coords
        self.yi = yi
        self.icovered = icovered


def find_hrdps_files(hrdps_folder,var):
    """ List all HRDPS files for the variable var in a list of folders 
    searching recursively each of them.
    
    Parameters
    ----------
    
    hrdps_folder : list of str
        Folders containing HRDPS grib files.
    var : str
        Name of the variable for which to search the files.
    
    Returns
    -------
    
    list
        List of file names.
    """
    # needs Python 3.5 or newer
    flist = []
    for folder in hrdps_folder:
        flist.extend(glob.glob(os.path.join(folder,'')+'**/*'+var+'*.grib2', recursive=True))
    
    # remove empty files (has happened for RH_TGL_2 files)
    flist = [f for f in flist if os.stat(f).st_size != 0]
    
    if (len(flist)==0):
        raise ValueError('No '+var+' grib2 files in specified HRDPS folder(s)')
    
    return flist


def hrdps_list_variables(archive_folder,suffix='.grib2'):
    """ List earliest file for each variable in HRDPS-west archive
    
    Examples: 
        hrdps_list_variables(archive_folder) # for all runs
        hrdps_list_variables(archive_folder,suffix='06_P000-00.grib2') # for 06 hrs runs
    """
    print('Listing files...')
    # list all grib2 files
    ff = glob.glob(os.path.join(archive_folder,'')+'**/*'+suffix, recursive=True)
    
#    print('Removing empty files...')
#    # remove empty files (has happened for RH_TGL_2 files)
#    ff = [f for f in ff if os.stat(f).st_size != 0]
    
    print('Sorting...')
    # also sorts by time
    ff.sort()
    
    # extract varaible name from each file name
    print('Getting names...')
    import re
    vv = [re.search('CMC_hrdps_west_(.*)_ps2.5km', k).group(1) for k in ff]
    
    # 1st occurrence for each unique variable name in the list;
    # set(vv) gives unique var names
    print('Extracting 1st...')
    return sorted(ff[vv.index(elem)].split('/')[-1] for elem in set(vv))
        


def hrdps_listfiles(fname,timeframe=None,accumflag=False):
    """
    % List raw HRDPS files in the specified folder covering the timeframe.
    % Works also if 00- and 06-hr (or 00- and 12-hr) forecasts are not the only duplicates.
    %
    % Inputs:
    % fname         list of available files with full paths;
    %               can be obtained with flist = find_hrdps_files(folder,var)
    % timeframe     (optional) [time_start time_end] time limits 
    %               (mktime format, seconds since epoc);
    %               defaults to the entire available HRDPS series
    % accumflag     (optional) if true, generate series for an accumulated
    %               quantity, i.e. expanded by one value at both ends and
    %               including 6-hr (12-hr) forecasts.
    %
    % Returns:
    % fname         file list
    % t             corresponding times
    % tb            base times (model run start times and also zero-hour accumulation times)
    % tfor          forecast horizon
    """
        
    ### hrdps_listfiles shouldn't be used with timeframe for accumulated quantities
#    if accumflag: # for accumulated quantities: expand by one hour (one HRDPS time step)
#        timeframe = timeframe + 3600*np.array([-1, 1])
        
    # for HRDPS only(!): assume all files are named as
    # CMC_hrdps_west_UGRD_TGL_10_ps2.5km_2014080518_P000-00.grib2
    # NB: RDPS10km files are named differently 
    # according to file naming convention, this sorts files by base time and then by forecast hour
    fname.sort()
    
    # timestamps (seconds since epoc, 1970-1-1) for base time (model run start time)
    tb = np.array([time.mktime(time.strptime(fk[-24:-14],'%Y%m%d%H')) for fk in fname])
    tfor = np.array([float(fk[-12:-9]) for fk in fname]) # forecast hours (0 to 12 in my repository)
    t = tb + tfor*60*60 # timestamp for each file
    
    # sort the reversed t sequence to pick up the last occurence, the most recent
    # requires numpy >= 1.9.0
    _,iu,iui,counts = np.unique(t[::-1], return_index=True, return_inverse=True, return_counts=True)
    counts = counts[iui]  # expand to size(t) with counts>1 for non-unique values
    counts[iu] = 1        # 1s are for values to keep; keep the last occurence
    counts = counts[::-1] # reverse to the original order
    
    # non-unique times to keep: of all duplicates, keep only 0 and 12 hr forecasts
    if accumflag:
#        counts[np.in1d(tfor,[0,12])] = 1 # keep 00- and 12-hr forecasts among non-unique times
        counts[np.in1d(tfor,[0,6])] = 1 # keep 00- and 06-hr forecasts among non-unique times
    
    lkeep = counts==1 # unique and those to keep
    t = t[lkeep]
    tb = tb[lkeep]
    tfor = tfor[lkeep]
    fname = [fn for fn,lk in zip(fname,lkeep) if lk] # fname = fname[lkeep]
    
    # apply time constraints
    if timeframe is not None:
        inr = np.logical_and(t>=timeframe[0], t<=timeframe[-1])
#        print('min t = ',np.min(t),'   max t = ',np.max(t))
#        print('timeframe[0] = ',timeframe[0],'   timeframe[-1] = ',timeframe[-1])
#        print('Number of values in the time frame: ',np.sum(inr))
        fname = [fn for fn,lk in zip(fname,inr) if lk] # fname = fname[inr]
        t = t[inr]
        tb = tb[inr]
        tfor = tfor[inr]
    
    return fname,t,tb,tfor


def hrdps_listfiles_accumdiff(fname,timeframe=None):
    """
    % List raw HRDPS files for an accumulated quantitiy in the specified folder covering the timeframe.
    %
    % Inputs:
    % fname         list of available files with full paths;
    %               can be obtained with flist = find_hrdps_files(folder,var)
    % timeframe     [time_start time_end] time limits (mktime format, seconds since epoc, 1970-1-1)
    %
    % Returns:
    % tdiff         [ndiff x 1] central time for the differences
    % ipairs        [2 x ndiff] indices into fname and tfor with columns corresponding to tdiff
    % fname         file list
    % tfor          forecats horizon for each file in fname
    """
    
    fname,t,tb,tfor = hrdps_listfiles(fname,timeframe=None,accumflag=True); # without time constraint
    
    # pairs for differentiation; zero-based
    n = len(t)
    ipairs = np.stack((np.arange(n-1), np.arange(1,n)),axis=1) # n-by-2 array
    
    # discard diffs for values with different base time
    inv = np.diff(np.take(tb,ipairs),axis=1) != 0
    ipairs = np.delete(ipairs,inv.nonzero(),axis=0)
    
    tdiff = np.mean(np.take(t,ipairs),axis=1)
    
    # apply time constraints
    if timeframe is not None:
        in1 = np.where(tdiff<=timeframe[0])[0] # np.where returns a tuple
        if len(in1)==0:
            warnings.warn('Beginning of the time interval is not covered by data.')
            in1 = np.array([0])
        else:
            in1 = np.max(in1)
        
        in2 = np.where(tdiff>=timeframe[-1])[0]
        if len(in2)==0:
            warnings.warn('End of the time interval is not covered by data.')
            in2 = len(tdiff)-1
        else:
            in2 = np.min(in2)
            
        ival = np.arange(in1,in2+1)
        
        tdiff = tdiff[ival]
        ipairs = ipairs[ival,:]
        
        # reduce file list to only the used files and renumber indices array
        ku,iui = np.unique(ipairs, return_inverse = True) # requires numpy >= 1.9.0
        fname = [fname[k] for k in ku] # fname = fname[ku]
        tfor = tfor[ku]
        ipairs = np.reshape(iui,(-1,2))
    
    return tdiff,ipairs,fname,tfor


def find_covered(x,y,xi,yi):
    """
    Find fvcom points covered by HRDPS grid
    """

    # determine which interpolation (fvcom) points are covered by the data (HRDPS) grid
    icovered = np.logical_and.reduce((xi>=np.min(x), xi<=np.max(x), yi>=np.min(y), yi<=np.max(y)))
    
    # coords of fvcom points in HRDPS domain
    xic = xi[icovered]
    yic = yi[icovered]
    
    return xic,yic,icovered


def hrdps_timeline(timeframe,accumflag=False):
    """
    % Generate timeline which covers the timeframe for four HRDPS runs daily:
    % 00, 06, 12, 18 hours with hourly output.
#    % Generate timeline which covers the timeframe for two HRDPS runs daily:
#    % 06, 18 hours with hourly output.
    %
    % Inputs:
    % timeframe     [time_start time_end] time limits (mktime format, seconds since epoc, 1970-1-1)
    % accumflag     (optional) if true, generate series for an accumulated
    %               quantity, i.e. expanded by one value at both ends and
    %               including 6-hr (12-hr) forecasts.
    %
    % Returns:
    % t             continous time vector (mktime format, seconds since epoc, 1970-1-1)
    % tb            base time (model start time) (mktime format, seconds since epoc, 1970-1-1)
    % mh            forecast hours (forecast time in hours since model stat time)
    %
    % See also: hrdps_fileseries, hrdps_filesmissing
    """
    
    hr2sec = 3600
    
    # HRDPS has two runs daily: 06, 18 hours with hourly output
    dt_hrdps = 3600       # HRDPS time step
#    hrdps_1st_run_hrs = 6 # HRDPS first run of the day start time
#    hrdps_horizon = 12    # maximum HRDPS forecast length (hrs) to use; hours between run
    hrdps_1st_run_hrs = 0 # HRDPS first run of the day start time
    hrdps_horizon = 6    # maximum HRDPS forecast length (hrs) to use; hours between runs
    
    if accumflag: # for accumulated quantities: expand by one hour (one HRDPS time step)
        timeframe = timeframe + dt_hrdps*np.array([-1, 1])
    
    
    t = timeline(timeframe, dt_hrdps) # hourly timeline, mktime format
    # this excludes 12th hour forecast files: 11hr is followed by 00hr
    mh = np.mod(t/hr2sec+hrdps_1st_run_hrs, hrdps_horizon) # forecast hours: from 00 to 11 hrs
    
    if accumflag: # for accumulated quantities
        # duplicate 00-hr forecasts to create 6-hr (12-hr) forecasts
        idup = mh==0
        # pre-pend (so 6-hr (12-hr) forecasts go before 00-hr forecasts) and sort
        t = np.r_[t[idup], t]
        mh = np.r_[np.full(sum(idup),hrdps_horizon), mh]
        isort = t.argsort()
        t = t[isort]
        mh = mh[isort]
    
    tb = t - mh*hr2sec # base time (hrdps model start time)
    
    return t,tb,mh


def timeline(timeframe,dt):
    """
    Generate equally spaced time vector bounded by timeframe (including the bounds)
    
    timeframe   [time_start, time_stop] time bounds in time.mktime 
                format (seconds since epoc, 1970-1-1)
    dt          time step in seconds
    """
    return np.arange(timeframe[0], timeframe[1]+dt, dt)
#    t = time.strptime(date0,'%Y-%m-%d %H:%M:%S')
#    t = datetime.datetime(*t[:6])
#    L = [t + datetime.timedelta(seconds=dt) for n in range(ntimes)]
#    
#    date_list = [date0 + datetime.timedelta(seconds=dt*x) for x in range(ntimes)]
#    
#    return L


def rotate_vec(u,v,theta):
    """
    % Rotate vectors with components u,v by theta degrees counter-clockwise.
    % theta can be a scalar or array of same size as u,v.
    %
    % E.g., to correct for magnetic declination = 19deg East:
    % [ur,vr] = rotate_vec(u,v,-19);
    %
    % Rotate coordinate frame for vector series along principal axis 330degT:
    % [ur,vr] = rotate_vec(u,v,330);
    """
    
    theta = theta*np.pi/180
    costheta = np.cos(theta)
    sintheta = np.sin(theta)
    ur = u*costheta - v*sintheta
    vr = u*sintheta + v*costheta
    
    return ur,vr


def grid2mobj_utm(x,y,tri,utmzone):
    """
    fvcom grid info in Mobj dictionary for input to setup_fvcom_forcing.
    x,y   node coordinates in UTM
    tri   triangulation array for the fvcom grid
    utmzone     numeric value
    """

    xc = fvt.convertNodal2ElemVals(tri, x)
    yc = fvt.convertNodal2ElemVals(tri, y)

    # Mobj - mesh object containing fields:
    Mobj = {"tri":tri,
            "nVerts":len(x),
            "nElems":tri.shape[0],
            "nativeCoords":"cartesian",
            "x":x, "y":y, "xc":xc, "yc":yc}
    
    # convert to geographic
    utmproj = Proj("+proj=utm +zone="+str(utmzone))
    lon,lat = utmproj(x,y, inverse=True)
    lonc,latc = utmproj(xc,yc, inverse=True)
    
    # extended 100 m in the northing direction to calculate grid rotation
    loncn,latcn = utmproj(xc,yc+100, inverse=True)
    
    return Mobj,lat,lon,latc,lonc,latcn,loncn


def units_cmc2fvcom(cmcvar):
    """
    Conversion of HRDPS units to FVCOM units for the variables that need conversion.
    
    % fvcom units
    % 'air_temperature'  'celsius'
    % 'air_pressure'     '0.01*Pa'
    % 'evap'             'm s-1'
    % 'precip'           'm s-1'
    
    % CMC units
    %    float Temperature_height_above_ground(time=28, height_above_ground=1, y=485, x=685);
    %      :units = "K";
    
         Upward surface latent heat flux
               Latent heat net flux @ Ground or water surface
            units 'W.m-2'
        FVCOM: m/s, ocean loose water is negative
            
    %    float Precipitation_rate_surface(time=28, y=485, x=685);
    %      :units = "kg.m-2.s-1";
        FVCOM: m/s, ocean loose water is negative
    
    % temp(fvcom) = 273.15 + temp(cmc)
    % pres(fvcom) = 1e-2 * pres(cmc) %???
    % precip(fvcom) = 1e3 * precip(cmc)
    """
    
    if cmcvar=='TMP_TGL_2':
        unitsift = -273.15
        unitscale = 1
    elif cmcvar=='LHTFL_SFC_0':
        unitsift = 0
        unitscale = -4.037864e-10 # evap = lhtfl/(lv*rho); negate: ocean loose water is negative
        # lv = 2.4773e6 J/kg (@10degC) latent heat of vaporisation
        # rho = 999.7 kg*m-3 (@10degC)
    elif cmcvar=='PRATE_SFC_0':
        unitsift = 0
        unitscale = 1e-3
    else:
        unitsift = 0
        unitscale = 1
    
    return unitscale, unitsift


def hrdps_iterate(kt,hrdps_it):
    """
    % Iterate to read data for one time step from CMC HRDPS grib2 files downloaded from daily archive.
    %
    % kt        time step in the iterator
    % hrdps_it  iterator object
    %
    % See also : hrdps_iterator
    
    % hrdps_it = repmat(struct('hrdps_long_name',[],'accumulated',[],...
    %     'x',[],'y',[],'nx',[],'ny',[],...
    %     'unitscale',[],'unitsift',[],'iterator',[],...
    %     'xm',[],'ym',[],'xic',[],'yic',[],'icovered',[]),nvar,1);
    
    % % for non-accumulated variables
    % iterator = struct('it',it,'w',w,'fname',fname,'stack',stack_init);
    
    % % for accumulated variables
    % iterator = struct('it',it,'w',w,'ipairs',ipairs,'fname',fname,'tfor',tfor,'stack',stack_init);
    """
    
    v = []
    for kv in range(len(hrdps_it)): # for each variable
        vk, hrdps_it[kv] = hrdps_iterate_1var_stack(kt, hrdps_it[kv])
        v.append(vk)
    
    return v, hrdps_it


def hrdps_iterate_1var_stack(kt,hrdps_it):
    """
    % Iterate for one varaible.
    % kt        time step in the iterator
    % hrdps_it  iterator object
    """
    
    def check_stack(kst):
        """
        % Extract previously recorded values from variables' stack or
        % read from file if not in stack.
        """
        
        ist = c.stack.k==kst
        if any(ist): # there are data in the stack for this time step
            vs = c.stack.v[:,:,ist].squeeze()
        else:
            fid = pygrib.open(c.fname[kst])
            try:
                # pygrib identifies only one variable in HRDPS files, grib is 1-based
                vs = fid[1].values # Ny,Nx float64
            except OSError:
                raise OSError('Cannot read: ',c.fname[kst])
            
            # push into stack
            c.stack.k[c.stack.ipush] = kst # index in the file list (0-based)
            c.stack.v[:,:,c.stack.ipush] = vs # corresponding values
            # increment and return to 0 if ipush reaches imax-1
            c.stack.ipush += (1-(c.stack.ipush==(c.stack.imax-1))*c.stack.imax)
            
        return vs
    
    # to get correct units for differentiated accumulated quantities, e.g.
    # Joules to Watts for radiation fluxes accumulated over an hour
    hour2sec = 1/60/60
    
    nx = hrdps_it.nx
    ny = hrdps_it.ny
    c = hrdps_it.iterator
    
    it1 = c.it[kt,0]
    if not hrdps_it.accumulated:
        # non-accumulated variable
        v1 = check_stack(it1)
    else:
        # accumulated variable
        k1 = c.ipairs[it1,:]
        
        if c.tfor[k1[0]]==0:
            # do not read file for 0-hr forecast for accum var, they're all zeros
            v11 = np.zeros((ny,nx),np.float64)
        else:
            v11 = check_stack(k1[0])
        
        if c.tfor[k1[1]]==0:
            # do not read file for 0-hr forecast for accum var, they're all zeros
            v12 = np.zeros((ny,nx),np.float64)
        else:
            v12 = check_stack(k1[1])
    
        # differentiate accumulated quantity
        v1 = (v12 - v11) / (c.tfor[k1[1]] - c.tfor[k1[0]]) * hour2sec
    
    w2 = c.w[kt,1]
    if w2!=0: # need to interpolate (only 2nd weight can be zero as defined in iterator())
        it2 = c.it[kt,1]
        if not hrdps_it.accumulated:
            # non-accumulated variable
            v2 = check_stack(it2)
        else:
            # accumulated variable
            k2 = c.ipairs[it2,:]
    
            if c.tfor[k2[1]]==0:
                # do not read file for 0-hr forecast for accum var, they're all zeros
                v21 = np.zeros((ny,nx),np.float64);
            else:
                v21 = check_stack(k2[0])
            
            if c.tfor[k2[1]]==0:
                # do not read file for 0-hr forecast for accum var, they're all zeros
                v22 = np.zeros((ny,nx),np.float64)
            else:
                v22 = check_stack(k2[1])
    
            # differentiate accumulated quantity
            v2 = (v22 - v21) / (c.tfor[k2[1]] - c.tfor[k2[0]]) * hour2sec
        
        v = v1*c.w[kt,0] + v2*w2 # linear interpolation
    else:
        v = v1
    
    # convert units
    v = v*hrdps_it.unitscale + hrdps_it.unitsift
    
    hrdps_it.iterator = c
    
    # interpolate to specified points, e.g. to fvcom nodes
    f = scipy.interpolate.interp2d(hrdps_it.x, hrdps_it.y, v, kind='linear', fill_value=np.nan)
    vi = np.full(hrdps_it.icovered.shape, np.nan) # initialize
    # bispeu doesn't produce nans outside of the domain; need to keep track of covered points
    vi[hrdps_it.icovered] = scipy.interpolate.dfitpack.bispeu(
            f.tck[0],f.tck[1],f.tck[2],f.tck[3],f.tck[4],hrdps_it.xi, hrdps_it.yi)[0]
    
    return vi, hrdps_it
    

def setup_fvcom_forcing(Mobj,fname,varname,htime,subname=None):
    """
    % Initialize FVCOM netCDF forcing file.
    %
    % write_FVCOM_forcing(Mobj, fvcom_forcing_file, data, infos, fver)
    %
    % DESCRIPTION:
    %   Initialize FVCOM netCDF forcing file for subsequent write of variables.
    %
    % INPUT:
    %   Mobj - mesh dict containing fields:
    %       tri - triangulation table for the unstructured grid
    %       nVerts - number of grid vertices (nodes)
    %       nElems - number of grid elements
    %       nativeCoords - model coordinate type ('cartesian' or 'spherical')
    %       x, y or lon, lat - node positions (depending on nativeCoords value)
    %       xc,yc - (optional) coordinates of element centriods
    %   fname - Output netCDF file name 
    %   varname - cell array of fvcom forcing varaible names; supported variables:
    %          'U10'
    %          'V10'
    %          'uwind_speed'
    %          'vwind_speed'
    %          'air_pressure'
    %          'cloud_cover'
    %          'specific_humidity'
    %          'evap'
    %          'precip'
    %          'air_temperature'
    %          'relative_humidity'
    %          'short_wave'
    %          'long_wave'
    %          'net_heat_flux'
    %
    % OUTPUT:
    %   FVCOM forcing netCDF file created
    %   ncfile      netcdf file object
    %
    %   Output file is for version 3.1.6 and up (all the forcing can go in a single file);
    %       Version 3.1.0 required separate files for specific forcing data
    %       (wind, heating and precipitation).
        
    % Options explained in write_FVCOM_forcing.m:
    % The fields in data may be called any of:
    %     - 'u10', 'v10' or 'uwnd', 'vwnd' - wind components
    %     - 'Et' or 'evap'      - evaporation
    %     - 'prate' or 'P_E'    - precipitation
    %     - 'nlwrs'             - net longwave radiation*,**
    %     - 'nswrs'             - net shortwave radiation*,**,***
    %     - 'shtfl'             - sensible heat net flux*,**
    %     - 'lhtfl'             - latent heat net flux*,**
    %     - 'slp' or 'pres'     - mean sea level pressure***
    %     - 'dswrf'             - downward shortwave flux
    %     - 'dlwrf'             - downward longwave flux***
    %     - 'rhum'              - relative humidity***
    %     - 'air'               - air temperature***
    %     - 'lon'               - longitude (vector)
    %     - 'lat'               - latitude (vector)
    %     - 'x'                 - eastings (vector)
    %     - 'y'                 - northings (vector)
    %     - 'nshf'              - pre-computed net surface heat flux**
    %     - 'lcc'               - low cloud cover
    %
    % Fields marked with an * are combined to form the "surface net heat flux"
    % (nshf) as follows:
    %
    %   nshf = nlwrs + nswrs - lhtfl - shtfl;
    %
    % ** Alternatively, a new field 'nshf' (net surface heat flux) can be
    % supplied, in which case shtfl and lhtfl are not necessary as their only
    % function is in the calculation of net surface heat flux. This approach
    % eliminates the need to interpolate so many variables onto the FVCOM grid,
    % thus decreasing the time needed to generate the FVCOM input files.
    %
    % *** These fields are required for HEATING_CALCULATED model input files.
    """
    # Based on Tools/Modelling/fvcom-toolbox-20160218/fvcom_prepro/write_FVCOM_forcing.m
    # by:
    #   Pierre Cazenave (Plymouth Marine Laboratory)
    #   Karen Amoudry (National Oceanography Centre, Liverpool)
    #   Rory O'Hara Murray (Marine Scotland Science)
    
    if subname is None:
        subname = 'setup_fvcom_forcing'
    
    ftbverbose = True
    
    nNodes = Mobj["nVerts"]
    nElems = Mobj["nElems"]

#    %--------------------------------------------------------------------------
#    % Create the netCDF header for the FVCOM forcing file
#    %--------------------------------------------------------------------------
    
    ncfile = nc.Dataset(fname, mode = 'w', format = 'NETCDF3_CLASSIC')
    
    # global attributes
    ncfile.title = 'FVCOM Forcing File'
    ncfile.source = 'FVCOM grid (unstructured) surface forcing'
    ncfile.history = 'File created with '+subname
        
    # time dimensions and variables
    fvt.setup_time(ncfile)
    fvt.write_time(ncfile,[datetime.datetime.fromtimestamp(k) for k in htime])
    
    # other dimensions
    ncfile.createDimension('nele',nElems)
    ncfile.createDimension('node',nNodes)
    
    nvar = len(varname)
    
    # define variables
    for vv in range(nvar):
        # On the elements
        if varname[vv]=='U10':
            data=ncfile.createVariable('U10','float32',('time','nele',))
            setattr(data,'long_name','Eastward Wind Speed')
            setattr(data,'units','m/s')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')
            
        elif varname[vv]=='V10':
            data=ncfile.createVariable('V10','float32',('time','nele',))
            setattr(data,'long_name','Northward Wind Speed')
            setattr(data,'units','m/s')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')
            
        elif varname[vv]=='uwind_speed':
            data=ncfile.createVariable('uwind_speed','float32',('time','nele',))
            setattr(data,'long_name','Eastward Wind Speed')
            setattr(data,'standard_name','Wind Speed')
            setattr(data,'units','m/s')
#                setattr(data,'grid','fvcom_grid')
            setattr(data,'type','data')
            
        elif varname[vv]=='vwind_speed':
            data=ncfile.createVariable('vwind_speed','float32',('time','nele',))
            setattr(data,'long_name','Northward Wind Speed')
            setattr(data,'standard_name','Wind Speed')
            setattr(data,'units','m/s')
#                setattr(data,'grid','fvcom_grid')
            setattr(data,'type','data')
                
        # On the nodes
        elif varname[vv]=='air_pressure': # Sea level pressure
            data=ncfile.createVariable('air_pressure','float32',('time','node',))
            setattr(data,'long_name','Surface air pressure')
            setattr(data,'units','Pa')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')

        elif varname[vv]=='cloud_cover':
            data=ncfile.createVariable('cloud_cover','float32',('time','node',))
            setattr(data,'long_name','Cloud cover')
#                setattr(data,'units','Fraction')
            setattr(data,'units','%')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')
            
        elif varname[vv]=='specific_humidity':
            data=ncfile.createVariable('specific_humidity','float32',('time','node',))
            setattr(data,'long_name','Specific humidity')
            setattr(data,'units','Kg kg^-1')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')
            
        elif varname[vv]=='evap':
            data=ncfile.createVariable('evap','float32',('time','node',))
            setattr(data,'long_name','Evaporation')
            setattr(data,'description','Evaporation, ocean lose water is negative')
            setattr(data,'units','m s-1')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')
            
        elif varname[vv]=='precip':
            data=ncfile.createVariable('precip','float32',('time','node',))
            setattr(data,'long_name','Precipitation')
            setattr(data,'description','Precipitation, ocean lose water is negative')
            setattr(data,'units','m s-1')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')
            
        elif varname[vv]=='air_temperature':
            data=ncfile.createVariable('air_temperature','float32',('time','node',))
            setattr(data,'long_name','Surface air temperature')
            setattr(data,'units','Celsius Degree')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')
            
        elif varname[vv]=='relative_humidity':
            data=ncfile.createVariable('relative_humidity','float32',('time','node',))
            setattr(data,'long_name','surface air relative humidity')
            setattr(data,'units','percentage')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')
            
        elif varname[vv]=='short_wave':
            # Downward shortwave radiation %%% can also be Net shortwave radiation for HEATING_ON?
            data=ncfile.createVariable('short_wave','float32',('time','node',))
            setattr(data,'long_name','Downward solar shortwave radiation flux')
            setattr(data,'units','Watts meter-2')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')
            
        elif varname[vv]=='long_wave':
            # Downward shortwave radiation %%% can also be Net longwave radiation for HEATING_ON?
            data=ncfile.createVariable('long_wave','float32',('time','node',))
            setattr(data,'long_name','Downward solar longwave radiation flux')
            setattr(data,'units','Watts meter-2')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')
            
        elif varname[vv]=='net_heat_flux':
            data=ncfile.createVariable('net_heat_flux','float32',('time','node',))
            setattr(data,'long_name','Surface Net Heat Flux')
            setattr(data,'units','W m-2')
#                setattr(data,'grid','fvcom_grid')
#                setattr(data,'coordinates',coordString)
            setattr(data,'type','data')

        elif varname[vv] in ['time', 'lon', 'lat', 'x', 'y','xc','yc','nv']:
            pass

        else:
            if ftbverbose:
                warnings.warn('Unknown or possibly unused input data type: '+varname[vv])
    
    return ncfile
    
    
def append_fvcom_forcing(ncfile,kt,fvcom_vars,data):
    """
    % Write time and data to an initialized FVCOM netCDF forcing file.
    % data  [nNodes, nVars] or cell length(vars)
    """
    
    for k in range(len(fvcom_vars)):
        ncfile[fvcom_vars[k]][kt,:] = data[k]


def interpolant(t,ti):
    """
    % Linear 1D interpolant to interpolate values at t to ti.
    % it    [nti x 2] indices into t (0-based)
    % w     [nti x 2] corresponding weights
    % In case of direct hit, it[k,1] is NaN and w[k,1] is 0
    """
    
    nt0b = len(t)-1
    nti = len(ti)
#    print('nt0b=',nt0b,'   nti=',nti)
    w  = np.zeros((nti,2))
    it = np.zeros((nti,2),dtype=int)
    for k in range(nti): # do in a loop to avoid memory overflow
        
        if ti[k]<t[0]: # outside on the left
            i1 = 0 # always keep the 1st index valid
            i2 = -9999 # np.nan cannot be int
            w[k,:] = [1,0] # nearest neighbour extrapolation
        elif ti[k]>t[-1]: # outside on the right
            i1 = nt0b
            i2 = -9999 # np.nan cannot be int
            w[k,:] = [1,0] # nearest neighbour extrapolation
        else: # within the range
            dt = t - ti[k]
            i1 = np.where(dt<=0)[0].max() # i1 = find(dt<=0,1,'last');
            i2 = np.where(dt>=0)[0].min() # i2 = find(dt>=0,1,'first');
            if i1==i2: # exact match (both dt==0)
                i2 = -9999
                w[k,:] = [1,0]
            else: # between two tdiff
                dt = abs(dt)
                w[k,:] = 1/dt[[i1,i2]] # inverse distance weighting
        
        it[k,:] = [i1,i2]
    
    w = w / np.sum(w,axis=1,keepdims=True) # scale to sum to 1
    
    return it,w


#if __name__ == '__main__':
#
#    hrdps_folder = '/media/krassovskim/MyPassport/Data/CMC/HRDPS/'
#    opp_folder = '/media/krassovskim/MyPassport/Max/Projects/opp/'
#    grd = opp_folder+'vhfr_low_v2_utm10_grd.dat'
#    tri,nodes = fvt.readMesh_V3(grd)
#    utmzone = 10
#
#    ftype = 'wnd'
##    ftype = 'hfx'
#    
#    fname = opp_folder+'vhfr_low_v2_2017nov_test5d_'+ftype+'.nc'
#    dts = ["2017-11-06 00:00:00","2017-11-11 00:00:00"]
#
#    tlim = [time.mktime(time.strptime(s, "%Y-%m-%d %H:%M:%S")) for s in dts]
#    
#    create_atm_hrdps(ftype,nodes[:,0],nodes[:,1],tri,utmzone,tlim,fname,hrdps_folder)