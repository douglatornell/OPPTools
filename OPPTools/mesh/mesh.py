""" Triangular mesh tools."""


import numpy as np
import numpy.ma as ma
from scipy import sparse
import matplotlib.tri as mtri
from OPPTools.general_utilities import geometry as mgeom


def fill_laplacian(tri,vnan):
    """ Fill missing values on a triangular mesh by valid neighbour averaging.
    
    Inputs:
        tri     (ntri,3)  triangulation array; zero-based or 1-based
        vnan    (nodes,m) values at mesh nodes, e.g. T,S at (nodes,layers)
                          missing values are nans, assumed to be same for all columns
    Output:
        v       (nodes,m) values with filled nans
    """
    # Inspired by Mesh2d_v24/smoothmesh.m by Darren Engwirda - 2007
    
    v = vnan.copy()
    n = v.shape[0]
    
    s = connectivity_sparse(tri,n)
    
    # nodes to fill
    ifix = np.isnan(v[:,0]) # nans are same for all columns (layers)
#    print('fill_laplacian: nodes to fill: ',np.sum(ifix))
    
    # suppress "RuntimeWarning: invalid value encountered in true_divide" produced by zeros in w
    with np.errstate(invalid='ignore'):
        while np.any(ifix):
            sf = s[:,~ifix][ifix,:] # valid neighbours for the nodes to fill
            w = np.sum(sf,axis=1) # weights: number of valid neighbours
            w[w==0] = np.nan
            v[ifix,:] = (sf*v[~ifix,:])/w # laplacian smoothing: average of neighbour values
            # nans remain for the nodes with no valid neighbours
            ifixnew = np.isnan(v[:,0])
            if np.all(ifix==ifixnew): # no new nodes filled, going in an endless loop
                break
            ifix = ifixnew.copy() # update valid node list
    
    return v


def smooth_laplacian(tri,vrough,maxit=20,ifix=None):
    """ Horizontally smooth a node-based quantity on a triangular mesh.
    Smoothing is done by iterative Laplacian smoothing (averaging of neighbours).
    
    Inputs:
        tri     (ntri,3)  triangulation array; zero-based or 1-based
        vrough  (nodes,m) values at mesh nodes, e.g. T,S at (nodes,layers)
        maxit   integer   number of iterations
        ifix    (nodes)   boolean, do not change values at these nodes (logical, size(v))
                          default: no fixed values
    Output:
        v       (nodes,m) smoothed field    
    """
    # Inspired by Mesh2d_v24/smoothmesh.m by Darren Engwirda - 2007
        
    v = vrough.copy()
    n = v.shape[0]
    
    if ifix is None:
        ifix = np.full((n,), False)
    
    if np.sum(ifix) != n: # if not all nodes are fixed
        
        s = connectivity_sparse(tri,n)
        
        w = np.sum(s,axis=1) # weights: number of valid neighbours
        if np.any(w==0):
            raise ValueError('Invalid mesh. Hanging nodes found.')
        
        vfix = v[ifix,:]
        
        for k in range(maxit):
            v = (s*v)/w # laplacian smoothing: average of neighbour values
            v[ifix,:] = vfix # don't change fixed nodes
        
        v = np.asarray(v) # convert from matrix to array to be consistent with the input
        
    return v


def connectivity_sparse(tri,n=None):
    """ Sparse connectivity matrix for a triangular mesh.
    
    Inputs:
        tri     (ntri,3)  triangulation array; zero-based or 1-based
        n       scalar    number of mesh nodes
    Output:
        s       (n,n)     sparse connectivity matrix for the nodes of the mesh
                          0 - no connection, 1 - connection (no 2's for internal edges)
    """
    
    # ensure tri is zero-based
    if tri.min()==1:
        tri = tri.copy() - 1 # assuming tri is for the entire mesh (!)
    
    # obtain the number of mesh nodes
    if n is None:
        n = tri.max() + 1 # assuming tri is for the entire mesh (!)
    
    # construct sparse connectivity matrix
    i = tri[:,[0,0,1,1,2,2]].ravel()
    j = tri[:,[1,2,0,2,0,1]].ravel()
    s = sparse.coo_matrix((np.ones(i.shape),(i,j)),shape=(n,n)).tocsr()
    
    # s has 1's for boundary neighbours and 2's for internal neighbours
    s[s==2] = 1 # replace 2's in s with 1's, so all neighbours have same weight
    
    return s


def tsearch(x,y,tri,xi,yi):
    """ Triangle indices for query points.
    
    Inputs:
        x,y     (n,)    triangulation nodes
        tri     (k,3)   triangulation array, zero-based
        xi,yi   (ni,)   query points
        
    Outputs:
        k       (ni,)   triangle indices
    """
    ts = mtri.Triangulation(x,y,tri).get_trifinder()
    return ts(xi,yi) # -1 for nodes outside the triangulation


def tri_transect(xt,yt,x,y,tri,xc=None,yc=None,nbe=None):
    """ Transect of a triangular mesh by a polyline.
    
    Discretize transect edges with points of intersection with grid eges
    and find interpolation indices and weights to interpolate fields at the mesh 
    to the transect nodes.
    
    Inputs:
        xt,yt   (nt,)  transect vertices,
        x,y     (n,)   grid nodes
        tri     (k,3)  triangulation array, zero-based
        xc,yc   (k,)   (optional) element centroids
        nbe     (k,3)  (optional) elements surrounding each element 
                       (indices into rows of tri); 
                       can be obtained from fvcom output variable 'nbe';
                       contains 1 zero value for each triangle with a boundary edge
    
    Outputs:
        xi,yi   (ni,2)  points of intersection
        nid     (ni,3)  grid node indices to use for further linear interpolation
                        (contains intersecting edges for pti in the 1st two columns)
        w       (ni,3)  weights for linear interpolation
                        (intersection location on grid edge in the 2nd column)
        xie,yie (nie,2) points of intersection for element-centered quantities,
                        same as xi,yi but with duplicates at discontinuity points 
                        (at grid edges and grid nodes)
        nide    (nie,4) grid element indices for interpolation of an element-centered quantity
        we      (nie,4) weights for linear interpoation for an element-centered quantity
        theta   (nie,)  transect orientation for subsequent calculation of normal
                        and tangential components of velocity as 
                        vn = v*cos(theta) - u*sin(theta)
    """
    
    ebnd,eint,tibnd,tiint = tri_edge(tri)
    e = np.r_[ebnd,eint] # all grid edges
    ti = np.r_[np.c_[tibnd,tibnd], tiint] # and their corresponding triangle indices (ne,2)
    
    xi,yi,loct,wt,loc,w = mgeom.edge_intersect(xt,yt, x,y, edges2=e, return_positions=True)
    
    # tinterpolant relies on tsearch, which doesn't guarantie correct result 
    # for points close to the edges, so derive nid,w for intersection points from 
    # loc,w
    ni = len(loc) # number of intersection points
    nid = np.c_[e[loc,:], np.zeros((ni,),dtype=int)] # add 3rd column: any valid node index, i.e. 0
    # w = [1-loc loc zeros(nj,1)]; % expand to 3-column array of weights: 3rd column - zero weights
    col3 = np.zeros((ni,))
    col3[np.isnan(w)] = np.nan
    w = np.c_[1-w, w, col3] # expand to 3-column array of weights: 3rd column - zero weights

    # transect orientation for each segment
    thetat = np.arctan2(np.diff(yt), np.diff(xt))
    theta = thetat[loct]
    theta = np.c_[theta,theta] # (ni,2)
    
    # a pair of angles for each transect node
    thetate = np.c_[  np.r_[thetat[0],thetat],  np.r_[thetat,thetat[-1]]  ] # (nt,2)
    
    # add transect points (if they don't match intersection points)
    iin = np.r_[loct[wt==0], loct[wt==1]+1] # indices of nodes in transect polyline which belong to intersection points
    iins = np.setdiff1d(np.arange(len(xt)),iin) # transect nodes to insert
    xtins = xt[iins]
    ytins = yt[iins]
    xi = np.r_[xi, xtins] # append to the end
    yi = np.r_[yi, ytins]
    
    nid_ins,w_ins,outside = tinterpolant(x,y,tri,xtins,ytins)
    w_ins[outside,:] = np.nan
    nid = np.r_[nid, nid_ins]
    w = np.r_[w, w_ins]
    
    #TODO: outside points' indices: change to 0 for neatness
    
    k = ti[loc,:] # indices of triangles for intersection points
    kt = tsearch(x,y,tri,xtins,ytins) # indices of triangles for iins transect nodes
    k = np.r_[k, np.c_[kt,kt]]
    theta = np.r_[theta, thetate[iins]]
    # NB: Not treating the case when a transect node touches on a mesh edge as a special case.
    # In this case, theta gets the value for transect edge in loct.
    
    isort = np.argsort(np.r_[loct+wt, iins]) # sort by position
    xi = xi[isort]
    yi = yi[isort]
    nid = nid[isort,:]
    w = w[isort,:]
    k = k[isort,:]
    theta = theta[isort,:]
    
#    ### test:
#    ### this misses points of intersection with the mesh boundary
#    # for node-based quantites
#    nidt,wt,outside = tinterpolant(x,y,tri,xi,yi)
    
    # for element-based quantites
    k = k.ravel()
    xie = np.repeat(xi,2)
    yie = np.repeat(yi,2)
    if xc is None or yc is None:
        xc = np.mean(x[tri],axis=1)
        yc = np.mean(y[tri],axis=1)
    if nbe is None:
        nbe = tri_nbe(tiint)
    nide,we = tinterpolantc(xc,yc,xie,yie,k,nbe)
    theta = theta.ravel()
    
    return xi,yi,nid,w, xie,yie,nide,we,theta


def tri_interp_eval(nid,w,f,zero_allnans=False):
    """Interpolate a node- or element-based quantity f with interpolant nid,w.
    
    Inputs:
        nid   [nt,ni] indices into grid nodes array
        w     [nt,ni] corresponding weights: all(sum(w,2))==1, where
                nt -- number of transect points
                ni -- number of grid nodes used for interpolation of each point
        f     [nnodes,...] scalar field(s) defined on the grid nodes, one column per field.
        zero_allnans    bool    determines how to treat values with all-nan weights
                                if False (default), returns zeros for all-nan wieghts
                                if True, returns nans for all-nan weights
    Outputs:
        fi    f interpolated to the transect points
        
    Example:
        Obtain interpolant for a transect of a triangular grid:
        xi,yi,nid,w, xie,yie,nide,we,theta = tri_transect(xt,yt,x,y,tri)
        Interpolate a salinity field to the transect nodes xt,yt:
        sali = grid_interp_eval(nid,w,sal)
    """
    
    # woks with nans in w
    fi = np.nansum(np.einsum('ij,ij...->ij...',w,f.take(nid,axis=0)),axis=1)
    
    # nansum gives zeros for all-nan slices, sub with nan if requested
    if not zero_allnans:
        fi[np.all(np.isnan(w),axis=1)] = np.nan # works for ND arrays
    
    return fi


def tinterpolant(x,y,tri,xi,yi,k=None):
    """Interpolant for linear interpolation of node-based values within a triangular grid.
    
    Does NOT perform nearest-neighbour extrapolation for points outside the
    triangulation.
     
    Inputs:
        x,y     (node,)     grid node coordinates
        tri     (nele,3)    grid triangulation array, zero-based
        xi,yi   (inode,)    points to interpolate to
        k       (node,)     k = tsearch(x,y,tri,xi,yi)
     
    Outputs:
        nid      (inode,3)   grid node indices to use for interpolation
        w        (inode,3)   weights for linear interpolation
     
    Example:
     Linear interpolation of scalar field(s) f:
     fi = (A1.*f(t1)+A2.*f(t2)+A3.*f(t3))./(A1+A2+A3);
     fi = w(:,1)*ones(1,nf).*f(nid(:,1),:) + ...
          w(:,2)*ones(1,nf).*f(nid(:,2),:) + ...
          w(:,3)*ones(1,nf).*f(nid(:,3),:);
    """
    # Based on Matlab routine tinterp by Darren Engwirda - 2005-2007
    
    if k is None:
        # get_trifinder requires triangulation to be valid:
        # it must not have duplicate points, triangles formed from colinear points, 
        # or overlapping triangles.
        k = tsearch(x,y,tri,xi,yi) # -1 for nodes outside the triangulation
    
    # corner nodes
    t0 = tri[k,0]
    t1 = tri[k,1]
    t2 = tri[k,2]
    
    # diffs
    dx0 = xi - x[t0]
    dx1 = xi - x[t1]
    dx2 = xi - x[t2]
    
    dy0 = yi - y[t0]
    dy1 = yi - y[t1]
    dy2 = yi - y[t2]
    
    # areas
    a2 = np.abs(dx0*dy1 - dy0*dx1)
    a1 = np.abs(dx0*dy2 - dy0*dx2)
    a0 = np.abs(dx2*dy1 - dy2*dx1)
    
    # weights for interpolation
    w = np.stack((a0,a1,a2), axis=1)/(a0 + a1 + a2)[:,None]
    w[k==-1,:] = 0 # for points outside of the grid
    
    # grid node indices for interpolation
    nid = np.stack((t0,t1,t2), axis=1)
    
    outside = k==-1
    
    return nid,w,outside


def tinterpolantc(xc,yc,xi,yi,k,nbe):
    """ Linear interpolant for an element-based quantity on a triangular grid.
    
    FVCOM-like interpolation of an element-based quantity. This is what FVCOM 
    uses to interpolate velocities. In each element, the velocity field is 
    determined as a plane fit in the central and the three bounding triangles in 
    the least squares sense.
    
    Use tri_interp_eval to 
    Used in tri_transect.
    
    Inpuits:
        xc,yc coordinates of the element centroids
        xi,yi (ni,) Points to interpolate to.
        k     (ni,) Indices of central triangles;
              no values for points outside of the grid bounds.
              N.B. Velocity field in FVCOM has discontinuities at element edges,
              therefore, k has
              1 value for points inside any triangle,
              2 values for points on a triangle edge,
              up to maxneigh values for points on a triangle vertex.
              If all possible values at discontinuities are required, several
              entries must be supplied for each xi,yi point, each with
              corresponding k.
              N.B. tsearch gives only one triangle for points on the edge or vertex
        nbe   (n,3) Indices (zero-based) into tri of surrounding triangles; 
              can be obtained from fvcom output as the variable 'nbe' (less 1 to make zero-based);
              The initial fvcom nbe contains 1 zero for each triangle with boundary edge; 
              after conversion to zero-based indices, the initial zeros become -1, 
              thus pointing to the LAST element in the list.
      
    Outputs:
        eid   (ni,4) Grid element indices to use for interpolation.
        w     (ni,4) Weights for linear interpolation.
    """
    
    # Converted from matlab code tinterpolantc.m (I.Fain, M.Krassovski 2016-03-21)
    
    ni = len(xi)
    eid_out = np.zeros((ni,4)).astype(int)
    w_out   = np.nan*np.ones((ni,4))
    ival = k != -1
    k = k[ival]
    xi = xi[ival]
    yi = yi[ival]
    
    be = nbe[k,:] # zero-based indices of bounding triangles
                  # contain -1 for triangles with boundary edge(s)
    be = be.filled(-1)
    
    #TODO: Halo elements for interpolation to the boundary?
    # Need to consider different slip conditions for solid and open boundary.
    # just ignore the missing bounding triange for boundary triangles
    # Does this work well?

    # add a dummy element with NaN coordinates; -1 in be will point to this dummy element
    xc = np.r_[xc, np.nan]
    yc = np.r_[yc, np.nan]
    
    # zero-centre at each central triangle's centroid 
    x = xc[be] - xc[k,None] # distances between bounding and central centroids
    y = yc[be] - yc[k,None]
    xi = xi - xc[k] # distances between bounding and central centroids
    yi = yi - yc[k]
    
    sx2 = np.nansum(x**2,axis=1)
    sy2 = np.nansum(y**2,axis=1)
    sxy = np.nansum(x*y,axis=1)
    
    a1 = sy2*x[:,0] - sxy*y[:,0]
    a2 = sy2*x[:,1] - sxy*y[:,1]
    a3 = sy2*x[:,2] - sxy*y[:,2]
    
    b1 = sx2*y[:,0] - sxy*x[:,0]
    b2 = sx2*y[:,1] - sxy*x[:,1]
    b3 = sx2*y[:,2] - sxy*x[:,2]
    
    d = sx2*sy2 - sxy*sxy
    
    w = np.zeros((len(d),4))
    w[:,0] = d - (xi*np.nansum(np.c_[a1,a2,a3],axis=1) + yi*np.nansum(np.c_[b1,b2,b3],axis=1))
    w[:,1] = (xi*a1 + yi*b1)
    w[:,2] = (xi*a2 + yi*b2)
    w[:,3] = (xi*a3 + yi*b3)
    
    with np.errstate(divide='ignore'):
        w = w/d[:,None]
    
    eid = np.c_[k, be] # some indices may point to the dummy element
    
    w_out[ival,:] = w
    eid_out[ival,:] = eid
    
    return eid_out, w_out


def tri_edge(t):
    """ Boundary and internal edges from a triangulation array t.
     
    Inputs:
         t     (ntri,3)     triangulation array; zero-based
         
    Outputs:
        ebnd   (nebnd,2)    bounadry edges; one row per edge with 2 node indices comprising the edge
        eint   (neint,2)    internal edges
        tibnd  (nebnd,)     indices of triangles in t corresponding to ebnd
                            (one triangle per edge).
        tiint  (neint,2)    indices of triangles corresponding to eint
                            (two triangles per edge).
    """
    # Based on matlab function getedges in meshpoly.m (mesh2d package by Darren Engwirda).

    # make each edge a row; then sort in both dimensions
    e = np.r_[t[:,[0,1]], t[:,[0,2]], t[:,[1,2]]] # edges (ntri*3,2)
    e = np.sort(e,axis=1) # sort each row
    esort = np.lexsort(e[:,::-1].T) # indices to sort the rows; sort keys go from 1st to last column
    e = e[esort,:] # sort rows
    
    idx = np.all(np.diff(e,axis=0)==0,axis=1) # Find shared edges -- same conseq indices in both colums
    idx = np.r_[idx,False] | np.r_[False,idx] # True for all shared edges
    ebnd = e[~idx,:] # Boundary edges
    eint = e[ idx,:] # Internal edges
    eint = eint[:-1:2,:] # unique internal edges
    
    # triange indices
    ti = np.tile(np.arange(t.shape[0]),3)
    ti = ti[esort]   # indices of triangles corresponding to e
    tibnd = ti[~idx] # indices of triangles corresponding to ebnd
    tiint = ti[ idx] # indices of triangles corresponding to eint
    tiint = np.reshape(tiint,(-1,2)) # -> two-column array (two triangles per edge)
    
    return ebnd,eint,tibnd,tiint


def tri_nbe(tiint):
    """ Surrounding triangles for each triangle in the element list.
    
    The array is similar to FVCOM's nbe array.
    
    Inputs:
        tiint (neint,2) internal edges; obtained with _,_,_,tiint=tri_edge(tri) or
              (ntri,3)  triangulation array; zero-based
        
    Output:
        nbe   (ntri,3)  masked array; indices of surrounding triangles (zero-based);
                        each triangle can have 3 or 2 or 1 surrounding triangles
                        depending on the number of boundary edges in each triangle;
    """
    
    if tiint.shape[1]==3:
        _,_,_,tiint = tri_edge(tiint)
    
    # construct element connectivity sparse matrix
    i = tiint.ravel()
    j = tiint[:,::-1].ravel()
    n = tiint.max() + 1
    s = sparse.coo_matrix((np.ones(i.shape),(i,j)),shape=(n,n)).tolil()
    nbe = s.rows # ndarray of lists of length 3 or 2 or 1 
                 # (depending on the number of boundary edges in each triangle)
    
    # combine in a 3-column masked array
    numcol = 3 # shouldn't be > lens.max()
    # solution is in part from here:
    # https://stackoverflow.com/questions/32037893/numpy-fix-array-with-rows-of-different-lengths-by-filling-the-empty-elements-wi
    lens = np.array([len(k) for k in nbe]) # length of each row of nbe
    mask = np.arange(numcol) < lens[:,None] # mask of valid places in each row
    out = ma.array(np.zeros(mask.shape, dtype=int), mask=~mask) # Setup output array
    out[mask] = np.concatenate(nbe) # put elements from nbe into valid positions
    
    return out