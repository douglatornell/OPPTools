""" Signal processing utilities
"""
import numpy as np
import scipy.signal
import scipy.linalg
import scipy.cluster
import scipy.stats
from collections import Counter
import datetime
import matplotlib

def zero_crossings(x,y):
    """ Zero-crossings of a linear segmented curve y = f(x).
    
    Parameters
    ----------

    x,y : array-like
        1D arrays, x-coordinate (can be array of datetime) and corresponding y values
            
    Returns
    -------
    
    x0 : array-like
        Coordinates of zero-crossings
    sign : array-like
        Corresponding direction of change:
        +1 for change of sign from negative to positive, 
        -1 for change of sign from positive to negative,
        0 if curve touches, but does not cross zero
    """
    difsign = np.diff(np.sign(y))
    difsign[np.isnan(difsign)] = 0 # exclude changes to and from nan
    i1 = np.where(difsign!=0)[0] # points before zero-crossings
    i2 = i1 + 1 # points after zero-crossings
    x0 = x[i1] + (x[i2]-x[i1]) * (-y[i1]) / (y[i2]-y[i1]) # linear interpolation
    sign = np.sign(y[i2])
    sign[np.sign(y[i1])==0] = 0 # needed if 1st point is zero or if curve touches zero
    x0,iu = np.unique(x0, return_index=True) # needed if curve touches zero
    sign = sign[iu] # also keep sign only for unique points
    return x0,sign


def regularize(t,v,si):
    """ Interpolate irregularly sampled series to regular intervals
    
    First time stamp in the output series coinsides with the first time stamp 
    in the original series.
    
    Parameters
    ----------
    
    t,v : array-like
        1D arrays, x-coordinate (can be array of datetime) and corresponding y values
    si : datetime.timedelta
        Target sampling interval, e.g. datetime.timedelta(minutes=2)

    Returns
    -------
    
    tr,vr : array-like
        Regularized series
    """
    tr = np.arange(t[0], t[-1], si).astype(datetime.datetime)
    vr = np.interp(matplotlib.dates.date2num(tr),matplotlib.dates.date2num(t),v)
    return tr,vr


def spikes(v,param=10,iterate=True,verbose=True):
    """ Detect spikes in a vector v by a threshold relative to standard deviation
    
    It is advised to pass a high-pass filetered series to this functon.
    
    Parameters
    ----------
    
    v : array_like
        Vector to despike (regularly sampled)
    param : float or int, optional
        Spike threshold parameter, multiples of standard deviation (default is 10)
    iterate : bool, optional
        Use iterative approach: run spike detection until no spikes detected in 
        the de-spiked series (default is True)
    verbose : bool, optional
        Print info on spikes detection. Default is True
        
    Returns
    -------
    
    inv : array_like
        Logical indices of spikes
    """
    
    sdev = np.nanstd(v)
    smean = np.nanmean(v)
    with np.errstate(invalid='ignore'):
        inv = np.abs(v-smean) > sdev*param # spike indices at this iteration
    if verbose:
        print('spikes: First iteration')
        print('   Max deviation = ',np.nanmax(np.abs(v-smean))/sdev,' of standard deviation')
        print('   Spikes detected: ',np.sum(inv))
    if iterate:
        vn = v.copy()
        vn[inv] = np.nan
        invk = inv.copy()
        while np.any(invk):
            sdev = np.nanstd(vn)
            smean = np.nanmean(vn)
            with np.errstate(invalid='ignore'):
                invk = np.abs(vn-smean) > sdev*param # spike indices at this iteration
            vn[invk] = np.nan
            inv = np.logical_or(inv,invk) # add newly detected spikes
            if verbose:
                print('spikes: Iterating...')
                print('   Max deviation = ',np.nanmax(np.abs(vn-smean))/sdev,' of standard deviation')
                print('   Spikes detected: ',np.sum(invk))
    return inv


def filterdata(x,fco, fs=1, rolloffband=None, attenuation=1e-4, showinfo=False):
    """Low-pass filtering 
    
    Low-pass Kaiser-windowed FIR filter is designed with optimal parameters
    estimated with kaiserord. Similar to Matlab FIR filtering approach.
    
    Parameters
    ----------
    
    x : array_like
        Series to filter. Works on columns of `x` in case it is a 2D array.
    fco : float
        Cutoff frequency (in same units as `fs`)
    fs : float, optional
        Sampling frequency, default is 1.
    rolloffband : float, optional
        FIR filter parameter: approximate 1/2 width of rolloff in time 
        units (inverse of `fs` units)
    attenuation : float, optional
        FIR filter parameter: maximum ripple in pass band and 
        minimum attenuation in stop band. Attenuation is relative energy 
        suppression level (not in decibels). Default is 1e-4, i.e. the energy 
        in stop band will be suppressed at least 1e-4 times of the original level.
    showinfo : bool, optional
        True to display window legth and beta, False for silent run. 
        Default is False.

    Returns
    -------
    
    y : array_like
        Filtered series.
    
    Examples
    --------
    Filter a series with 30-min sampling, cutoff 1/20 cpd, 
    rolloff from 1/18 to 1/22 cpd, energy attenuation in stop band 10^-4::
    
      y = filterdata(x, 1/20, fs=48, rolloffband=2, attenuation=1e-4)

    """
    if rolloffband is None:
        rolloffband = 1/fco/12 # half-rolloff, e.g. 2/24 for daily resampling with 30-hr cutoff
    
    width = 1/(1/fco-rolloffband) - 1/(1/fco+rolloffband)
#    width = 2 * (fco - 1/(1/fco+rolloffband)) # slightly narrower band
    winlen,beta = scipy.signal.kaiserord(-20*np.log10(attenuation),width/(fs/2))
    winlen += -(winlen % 2) + 1 # ensure odd length
    if showinfo:
        print('Filtering with window length: {}, beta: {}'.format(winlen,beta))
    b = scipy.signal.firwin(winlen, fco, window=('kaiser', beta), fs=fs)
    
    if x.ndim==1:
        x = x[:,None]
        x_is_vector = True
    else:
        x_is_vector = False
    
    ncol = x.shape[1]
    
    # pad each column with nans in front and at the end
    y = np.zeros(x.shape)
    for k in range(ncol): # convolution for each column
        y[:,k] = convolve_nan(x[:,k],b)
    
    if x_is_vector:
        y = y[:,0] # preserve shape of the input
        
    return y


def convolve_nan(x,b):
    """ Convolve a vector (with NaNs) with a window
    
    Parameters
    ----------
    
    x : array_like
        Vector of values
    b : array_like
        Window coefficients

    Returns
    -------
    
    y : array_like
        Convolved vector of same size as x
    """
    
    n = len(b) # window length
    npad = int(np.floor(n/2)) # window length should be odd to preserve original series length
    inv_orig = np.isnan(x)
    
    # NaN-padded convolution matrix
    pads = np.nan*np.ones(n-1)
    xcm = scipy.linalg.toeplitz(np.r_[x[0],pads], np.r_[x,pads]) # (npad+1, n+npad)
    
    # scale window to adjust for missing values and NaN-padding
    ival = ~np.isnan(xcm)
    wsc = np.einsum('i,ij->j', b, ival)
    
    xcm[~ival] = 0 # replace nans with zeroes (einsum cannot ignore nans)
    
    # apply window and scaling
    with np.errstate(divide='ignore'):
        y = np.einsum('i,ij,j->j', b, xcm, 1/wsc)
    
    # remove padding
    y = y[npad : -npad]
    y[inv_orig] = np.nan
    
    return y


def find_si(t):
    """ Find sampling interval as the most frequent value in diff(t)
    
    Parameters
    ----------
    
    t : array_like
        Vector of numbers or datetime objects

    Returns
    -------
    
    Sampling interval, a number or datetime.timedelta
    """
    if not issorted(t):
        raise ValueError('Input must be sorted in ascending order.')
    return scipy.stats.mode(np.diff(t))[0][0]


def find_si_robust(t):
    """ Robust determination of sampling interval
    
    Finds most frequent samping interval after custering of close values.
    
    Parameters
    ----------
    
    t : array_like
        Vector of datetime objects

    Returns
    -------
    
    Sampling interval, datetime.timedelta
    """
    
    if not issorted(t):
        raise ValueError('Input must be sorted in ascending order.')
    c = Counter(np.diff(t)) # count occurencies of each interval
    if len(c)==1:
        si_sec = next(iter(c.keys())).total_seconds() # Python 3
    else:
        counts = [k for k in c.values()]
        si = [k.total_seconds() for k in c.keys()] # sampling interval, decimal seconds
        # sort for subsequent clustering
        isort = np.argsort(si)
        si = np.array(si)[isort]
        counts = np.array(counts)[isort]
        # cluster very close si values (within 1% of most frequent si)
        cl = scipy.cluster.hierarchy.fclusterdata(si[:,None], 
                                                  si[np.argmax(counts)]*0.01, 
                                                  criterion='distance')
        counts_cl = accum_np(cl,counts) # sum counts within each cluster
        si_cl = accum_np(cl,si*counts)/counts_cl # weighted average of si within each cluster
        si_sec = si_cl[np.argmax(counts_cl)]
        
    # convert to timedelta
    # microseconds is the highest precision in timedelta, so round to 6 decimals
    si_sec = round(si_sec,6)
    sec = datetime.timedelta(seconds = int(si_sec // 1),
                             microseconds = int(round((si_sec % 1)*1e6)))
    return sec


def issorted(a):
    """ Check if vector is sorted in ascending order
    """
    return np.all(a[:-1] <= a[1:])


def fill_small_gaps(t,v,max_gap):
    """ Fill small gaps in a time series by linear interpolation
    
    Parameters
    ----------
    
    t : array_like
        Vector of datetime objects
    v : array_like
        Corresponding vector of values
    max_gap : int
        Longest gap to fill (in data points)

    Returns
    -------
    
    vi : array_like
        Series with filled small gaps, same shape as `v`
    """
    
    # fill all gaps
    ival = ~np.isnan(v)
    vi = np.interp(matplotlib.dates.date2num(t), matplotlib.dates.date2num(t[ival]), v[ival])
    
    # fill large gaps back with nans
    ist,ien = continuous_chunks(np.isnan(v))
    for k in range(len(ist)):
        if ien[k]-ist[k] > max_gap:
            vi[ist[k]:ien[k]] = np.nan # fill with nans
    
    return vi


def continuous_chunks(v):
    """ Find continuous intervals of 1's or True's in a vector of 1/0 or True/False
    
    Parameters
    ----------
    
    v : array_like
        Vector of 0/1 or True/False.
    
    Returns
    -------
    
    ist,ien : array_like, int
        Start and end indices of continuous intervals.
        
    Examples
    --------
    Find indices of gaps in a regularly spaced series with gaps filled with nans
    
    >>> ist,ien = continuous_chunks(np.isnan(x))
    
    Find indices of continuous intervals of good data
    
    >>> ist,ien = continuous_chunks(~np.isnan(x))
    
    """
    v = v.astype(int) # ensure 0/1 values 
    dn = np.diff(v)
    ist = np.where(dn==1)[0] + 1 # chunk start (zero-based)
    ien = np.where(dn==-1)[0] + 1 # chunk end (zero-based, exclusive)
    
    if v[0]==1:
        ist = np.r_[0,ist]
    if v[-1]==1:
        ien = np.r_[ien,len(v)]
    
    return ist,ien


def accum_np(accmap, a, func=np.sum):
    """ Accumulate values in `a` according to the map in `accmap`
    
    Careful: This quick hack only works with contiguous accmaps, 
    like 222111333, but not 1212323. Every change from one number to another 
    will be seen as a new value. This avoids the slow sorting.
    
    Code from:
    https://mldesign.net/blog/2013/02/18/speedy-numpy-replacement-for-matlab-accumarray/
    """
    indices = np.where(np.ediff1d(accmap, to_begin=[1], to_end=[1]))[0]
    vals = np.zeros(len(indices) - 1)
    for i in range(len(indices) - 1):
        vals[i] = func(a[indices[i]:indices[i+1]])
    return vals


def principal_axes(u,v):
    """ Principal axes of a vector series
    
    Parameters
    ----------
    
    u,v : array_like
        Zonal and meridional components; u,v can be multi-column; 
        NaNs in u,v are ignored.
    
    Returns
    -------
    
    theta : float
        Semi-major axis direction, degrees clockwise from North.
    lambda1,lambda2 : float
        Variances along semi-major and semi-minor axes.
    
    Reference
    ---------
    Emery and Thomson, 2nd ed., p327
    """
    
    # remove mean
    ud = u - np.nanmean(u,axis=0)
    vd = v - np.nanmean(v,axis=0)
    
    # variances
    mu2 = np.nanmean(ud**2,axis=0)
    mv2 = np.nanmean(vd**2,axis=0)
    muv = np.nanmean(vd*ud,axis=0)
    
    # calculate principal axis direction (degrees from true north)
    theta = 0.5 * np.arctan2(2*muv,(mv2-mu2)) * 180/np.pi
    
    # variance in the rotated frame
    lambda1 = 0.5 * (mv2 + mu2 + np.sqrt((mv2 - mu2)**2 + 4*(muv**2)))
    lambda2 = 0.5 * (mv2 + mu2 - np.sqrt((mv2 - mu2)**2 + 4*(muv**2)))
    
    return theta,lambda1,lambda2


def rotate_vec(u,v,theta):
    """ Rotate vector series along principal axes of variance
    
    The rotated series has positive `v` is towards `theta` direction.
    
    Parameters
    ----------
    
    u,v : array_like
        Zonal and meridional components.
    theta : float
        Semi-major axis direction, degrees clockwise from North.
    
    Returns
    -------
    
    ur,vr : array_like
        Vector components in the rotated coordinate frame.
    """
    
    theta_rad = theta*np.pi/180
    sin_t = np.sin(theta_rad)
    cos_t = np.cos(theta_rad)
    
    # rotate
    ur = u*cos_t - v*sin_t
    vr = u*sin_t + v*cos_t
    
    return ur,vr