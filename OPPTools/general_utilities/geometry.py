"""
Custom routines to handle geometric shapes.
"""

import numpy as np
import shapely.geometry as geom
from scipy.spatial import cKDTree

def poly_nan2list(p):
    """ Split nan-separated sequense to list of continuous chunks.
    Solution from
    https://stackoverflow.com/questions/14605734/numpy-split-1d-array-of-chunks-separated-by-nans-into-a-list-of-the-chunks
    
    Input:
        p   (np,) or (np,nd)   nan-separated polyline in nd-space
    Output:
        list of numpy arrays split on nans in the first column of p
    """
    
    if p.ndim==1:
        pi = p
    else:
        pi = p[:,0]
            
    return [p[s] for s in np.ma.clump_unmasked(np.ma.masked_invalid(pi))]


def inpoly(x,y,xypoly):
    """Popint in polygon test.
    
    Inputs:
        x,y     (nodes,)    point coordinates
        xypoly  (pnodes,2)  polygon vertices (can be nan-separated)
        
    Output:
        inp     (nodes)    list, with true for points in polygon
    """
    #NOTE This routine is slow. Use inpoly from bathy.py
    
    plist = poly_nan2list(xypoly)
    poly = geom.MultiPolygon([geom.Polygon(k) for k in plist])
    inp = [poly.contains(geom.Point(xk,yk)) for xk,yk in zip(x,y)]
    
    return inp


def points_decimate(p,r):
    """Decimate a point cloud p with radius of decimation r.
    
    Inputs:
        p   (npoints,ndims)     point coordinates
        r   float               radius of decimation
        
    Output:
        ikeep (npoints,) bool   True for points to keep
    """
    # Thanks:
    # https://stackoverflow.com/questions/47755704/filtering-coordinates-based-on-distance-from-a-point
    
    # mask for the points
    npoints = np.shape(p)[0]
    tocheck = np.ones(npoints, dtype=bool) # False for visited and removed points
    ikeep = np.zeros(npoints, dtype=bool) # True for visited points (points to keep after decimation)
    
    kd = cKDTree(p)
    
    inext = 0 # start with 1st point
    
    # do 1st iteration outside of the loop for the loop condition to check correctly,
    # since argmax returns 0 for all Trues
    i = kd.query_ball_point(p[inext,:], r) # find indices of points within radius
    tocheck[i] = False
    ikeep[inext] = True
    inext = np.argmax(tocheck) # finds 1st valid value
    # use argmax, since np.where is much slower
    
    while inext != 0: # argmax will return 0 for all Falses
        i = kd.query_ball_point(p[inext,:], r) # find indices of points within radius
        tocheck[i] = False
        ikeep[inext] = True
        inext = np.argmax(tocheck) # finds 1st valid value; next point to visit
    
    return ikeep # True for visited points


def edge_intersect(x1,y1, x2,y2, edges1=None,edges2=None,return_positions=False):
    """ Intersection points of two sets of segments.
    
    Inputs:
        x1,y1   (np1,)  Node coordinates for the 1st set of segments; 
                        can contain nans, e.g. for nan-separated polylines.
        x2,y2   (np2,)  Same for the 2nd set of segments.
        
        edges1  (ne1,2) (optional) Edges as indices in x1,y1 (zero-based) 
                        for the 1st set of segments.
        edges2  (ne2,2) Same for the 2nd set of segments.
                        By default, edges are cosequitive pairs of nodes, 
                        i.e. x,y is assumed to be a polyline.
        return_positions    bool    (optional) If True, return positions loc,w of 
                                    the intersection points on each set of segments.
                                    The intersection points are sorted by their 
                                    positions along 1st set of segments.
                                    If False, the intersection points are not sorted.
                                    Default is False.
     Outputs:
        xi,yi   (ni,)   Coordinates of intersection points.
        loc1    (ni,)   Segment index in the 1st set of segments for each intersection point.
        w1      (ni,)   Relative position of each intersection point on the segment,
                        1st segment node has position=0, 2nd node has position=1.
        loc2,w2 (ni,)   Same for 2nd set of segments.
    """
    
    def line(x,y,e):
        """ MultiLineString object and segment distances.
        """
        if e is None: # polyline was supplied
            n = len(x)
            e = np.c_[np.arange(n-1),np.arange(1,n)] # edges as consequitive pairs of nodes
        # remove edges with nan coordinates
        inv = np.where(np.isnan(x) | np.isnan(y))
        if np.any(inv):
            inve = np.where(np.any(e==inv,axis=1)) # edges to ignore
            e = np.delete(e, inve, axis=0) # remove rows for invalid edges
        xe = x[e]
        ye = y[e]
        ms = geom.MultiLineString([((xk[0],yk[0]),(xk[1],yk[1])) for xk,yk in zip(xe,ye)])
        d = np.sqrt(np.diff(xe,axis=1)**2 + np.diff(ye,axis=1)**2)[:,0] # segment lengths
        dc = np.cumsum(d) # cumulative distances along the set of segments
        return ms,d,dc,xe,ye
    
    def position(m,d,dc,xe,ye,pi,xi,yi):
        """ Position of intersection points on the set of segments.
        
        Returns indices of intersected edges and relative positions of 
        intersection points on each edge.
        """
        
        #N.B. shapely's project() won't work correctly if coords in pi and m
        # are in different precision. Convert both to either float32 or float64.
        pr = np.array([m.project(pik) for pik in pi])
        loc = np.searchsorted(dc,pr)
        
        # for intersection points matching with segment nodes
#        pclose = np.isclose(pr[:,None],dc) # (npi,ne)
        pclose = pr[:,None]==dc2 # (npi,ne) #TODO: Check that equality works.
        row,col = np.where(pclose) # works for line strings but not for multistrings with breaks
        w = np.ones(len(pr))
        loc[row] = col # for points matching with segment nodes
        # Deal with the case when two intersection points fall on two nodes 
        # at the edges of a break in multilinestring: Same pr, but different x,y.
        # indices of duplicates in col
        dupi = [np.where(colu==col)[0] for colu in np.unique(col) if np.sum(colu==col)>1]
        for dupik in dupi: # for each group of duplicates
            for dupikk in dupik: # for each duplicate in the group
                ipi = row[dupikk] # intersection point
                # compare x,y of the segments with xi,yi
                iseg,inode = np.where(np.logical_and(np.isclose(xe,xi[ipi]),np.isclose(ye,yi[ipi])))
                loc[ipi] = iseg
                w[ipi] = inode # 0 rel distance for 1st node, 1 for 2nd
        
        nonmatch = ~np.any(pclose,axis=1)
        w[nonmatch] = 1 - (dc[loc[nonmatch]] - pr[nonmatch])/d[loc[nonmatch]] # for non-matching points
        return loc,w
    
    # Shapely's project() won't work correctly if coords for two objects
    # are in different precision, e.g. float32 and float64. 
    # Convert the lower to the higher precision.
    if x1.dtype > x2.dtype:
        x2 = x2.astype(x1.dtype)
        y2 = y2.astype(y1.dtype)
    elif x1.dtype < x2.dtype:
        x1 = x1.astype(x2.dtype)
        y1 = y1.astype(y2.dtype)
        
    m1,d1,dc1,xe1,ye1 = line(x1,y1,edges1)
    m2,d2,dc2,xe2,ye2 = line(x2,y2,edges2)
    
    pi = m1.intersection(m2)
    
    # ensure MultiPoint object
    if type(pi) is geom.LineString:
        pi = geom.MultiPoint(pi.coords)
    elif type(pi) is geom.MultiLineString:
        allpoints = []
        for pik in pi: allpoints.extend(pik.coords) # collect all points from MultiLineString
        pi = geom.MultiPoint(allpoints)
    #TODO: deal with shapely.geometry.collection.GeometryCollection type
    
    pi = pi.intersection(pi) # unique pi
    
    pia = np.array(pi)
    xi = pia[:,0]
    yi = pia[:,1]
    
    if not return_positions:
        return xi,yi
    else:
        # indices of intersected edges and relative positions of intersection points
        loc1,w1 = position(m1,d1,dc1,xe1,ye1,pi,xi,yi)
        loc2,w2 = position(m2,d2,dc2,xe2,ye2,pi,xi,yi)
        
        # sort by position on the 1st set of segments
        isort = np.lexsort(np.c_[w1,loc1].T)
        xi = xi[isort]
        yi = yi[isort]
        loc1 = loc1[isort]
        w1 = w1[isort]
        loc2 = loc2[isort]
        w2 = w2[isort]
        
        return xi,yi,loc1,w1,loc2,w2