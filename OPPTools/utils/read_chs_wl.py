import numpy as np
import pandas as pd


def read_chs_wl(fname):
    """ Read CHS water level file.
    """
    d = pd.read_csv(fname,
                    delimiter='\s+',
                    skiprows=24,
                    header=None,
                    parse_dates={'time':[0,1]},
                    na_values=999.999)
    
    time = d['time'].dt.to_pydatetime()
    h = np.array(d[2])
    return time,h

#WaterLevel      07747 STANOVAN                                     2002/01/01||
#!Computed    49 17.4723N 123  0.2753W                        +08   0000:00   ||
#     25000 0017days 100.0% R                                       0001:00 03||
#                                                                             ||
#                                                                            0||
#                                                                             ||
#01 Date      YYYY/MM/DY     0 A     TY                                       ||
#02 Time            hhmm     0 A     TY                                       ||
#03 WaterLevel         m     0 A     TY                                       ||
#                                                                             ||
#                                                                             ||
#                                                                             ||
#Tides & Currents, IOS                                                        ||
#0774720020101.trm         07747_2001)2002.log       0774720010604.trm        ||
#CHANGED. Water Level -4.305 shift, 1.000 gain.                               ||
#Was called Chevron Oil Terminal when collected.                              ||
#                                                                             ||
#                                                                             ||
#                                                                             ||
#                                                                             ||
#                                                                             ||
#                                                                             ||
#Final/merged height                                                          ||
#                                                                             ||
#2002/01/01 00:00   0.555
#2002/01/01 00:01   0.545
