"""
extract_obc_from_nemo.py

Created on Tue Feb 12 12:01:51 2019
"""
import numpy as np
from pyproj import Proj
import datetime
import netCDF4 as nc
import matplotlib
import scipy
import time
import OPPTools.fvcomToolbox as fvt
import OPPTools.nesting as nesting

import getpass
import sys
import socket
if socket.gethostname() == 'salish.eos.ubc.ca':
    scDataset_path = '/home/mkrassov/scDataset'
else:
    # for local machines
    username = getpass.getuser()
    scDataset_path = '/media/'+username+'/MyPassport/Max/Tools/Modelling/scDataset'
sys.path.append(scDataset_path)
from scDataset import scDataset

def extract_obc_from_nemo(grd_file,obc_file,utmzone,nemo_folder,tlim,out_file):
    """
    """
    
    tri, node_xy = fvt.readMesh_V3(grd_file) # tri is 1-based
    x,y = node_xy[:,0],node_xy[:,1]
    obc = fvt.readObcFile_V3(obc_file)
    obc -= 1 # make zero-based
    x,y = x[obc],y[obc]
    
    
    # list nemo files
    nemo_file_list = nesting.list_nemo_files(nemo_folder, nemo_file_pattern, 'T')
#    nemo_time, nemo_file, nemo_kt = nesting.nemo_timeseries(nemo_file_list)
    
    # get time from file names, e.g. SalishSea_1h_20190210_20190210_grid_T.nc
    nemo_file_time = np.array([datetime.datetime.strptime(f1[-18:-10],'%Y%m%d') 
        for f1 in nemo_file_list])
    
    # sort by time
    isort = np.argsort(nemo_file_time)
    nemo_file_time = nemo_file_time[isort]
    nemo_file_list = [nemo_file_list[k] for k in isort]    
    
    # select files that cover fvcom run time
    istart = np.where(nemo_file_time<=tlim[0])[0]
    if istart.size == 0:
        istart = 0
    else:
        istart = istart.max()
    
    iend = np.where(nemo_file_time>=tlim[-1])[0]
    if iend.size == 0:
        iend = nemo_file_time.size
    else:
        iend = iend.min()+1
    
    nemo_file_time = nemo_file_time[istart:iend]
    nemo_file_list = nemo_file_list[istart:iend]
    
    
    # find nemo nodes corresponding to fvcom obc
    nccoord = nc.Dataset(nemo_file_list[0])
    nemo_lon = nccoord.variables['nav_lon'][:]
    nemo_lat = nccoord.variables['nav_lat'][:]
    
    utmproj = Proj("+proj=utm +zone="+str(utmzone))
    nemo_x,nemo_y = utmproj(nemo_lon,nemo_lat)
    
    locz = nesting.nemo_nesting_zone(nemo_x,nemo_y, x,y)
    j,i = np.unravel_index(locz, nemo_lon.shape)
    
    
    # extract
    ncf = scDataset(nemo_file_list)
    timevar = nccoord.variables['time_counter']
    if timevar.getncattr('units') != 'seconds since 1900-01-01 00:00:00':
        raise RuntimeError('Unknown time units in file: '+nemo_file_list[0])
    nemo_time = [datetime.datetime(1900,1,1,0,0,0) + 
                 datetime.timedelta(seconds = tk) for tk 
                 in ncf.variables['time_counter'][:]]
    ssh = []
    lon = []
    lat = []
    tst = time.time()
    for k,ij in enumerate(zip(i,j)):
        print(k+1,' of ',len(i))
#        print(ij)
        ssh.append(ncf.variables['sossheig'][:,ij[1],ij[0]])
        lon.append(ncf.variables['nav_lon'][ij[1],ij[0]])
        lat.append(ncf.variables['nav_lat'][ij[1],ij[0]])
        pdone = (k+1)/len(i)
        eta = tst + (time.time()-tst)/pdone
        print('extract_obc_from_nemo: ',pdone*100,'% done. ETA: ',time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(eta)))
    print(len(lon))
    scipy.io.savemat(out_file,
                     {'time':matplotlib.dates.date2num(nemo_time)+366,
                      'ssh':np.array(ssh),
                      'lon':lon,
                      'lat':lat})
        
    return nemo_time,ssh,lon,lat

if __name__ == '__main__':
    if socket.gethostname() == 'salish.eos.ubc.ca':
        grd_folder = '/SalishSeaCast/FVCOM-VHFR-config/grid/'
        nemo_folder = '/results2/SalishSea/nowcast-green.201806'
        out_folder = '/home/mkrassov/vh/'
    else: # local machines, for tests
        grd_folder = '/media/'+username+'/MyPassport/Max/Projects/opp/'
        nemo_folder = '/media/'+username+'/MyPassport/Data/opp/UBC-DATA/nowcast-green.201806'
        out_folder = '/media/'+username+'/MyPassport/Max/Projects/opp/mat/'
    # FVCOM grid
    grd_file = grd_folder+'vh_x2_grd.dat'
    obc_file = grd_folder+'vh_x2_obc.dat'
    utmzone = 10
    nemo_file_pattern = 'SalishSea_1h_*_grid_'
    tlim = [datetime.datetime(2017,11,1),datetime.datetime(2019,2,12)]
    out_file = out_folder+'extract_obc_from_nemo.mat'
    nemo_time,ssh,lon,lat = extract_obc_from_nemo(grd_file,obc_file,utmzone,
                                                  nemo_folder,tlim,out_file)