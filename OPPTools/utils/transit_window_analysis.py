"""
NOTE:
    This example uses FVCOM hindcasts. For fair comparison we need forecasts.
"""
import os
import numpy as np
import scipy.io as sio
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import netCDF4 as nc
from pyproj import Proj
import ttide as tt
import OPPTools.general_utilities as gutil
import OPPTools.fvcomToolbox as fvt
from OPPTools.mesh import mesh
from . import fvcom_postprocess as fpp
#from . import read_chs_curr
from OPPTools.utils.read_chs_curr import read_chs_curr

import socket
if socket.gethostname()=='usr-svc-sci-ctn038': # GPSC
    work = '/home/mak003/work/opp/vh/'
    run036folder = work+'run036'
    vh_x2_grd_file = work+'vh_x2_grd.dat'
    chs_const_file = work+'data/04100const.curr'
    ais_matfile = work+'data/ais_collect_station_hadcp_upto2019-03-20.mat'
    
elif socket.gethostname()=='WPBCIOS9039457': # local
    work = '/media/krassovskim/MyPassport/'
    run036folder = work+'Data/opp/vh/run036'
    vh_x2_grd_file = work+'Max/Projects/opp/vh_x2_grd.dat'
    chs_const_file = work+'Data/opp/observed/hadcp2nd/04100const.curr'
    ais_matfile = work+'Max/Projects/opp/mat/ais_collect_station_hadcp_upto2019-03-20.mat'
    
else:
    raise RuntimeError('Unknown host. Provide paths to data files manually.')

def script_check_scaling():
    """ Test CHS prediction and model performance with different velocity scaling
    """
    uthres = 2*0.51444 # m/s TW threshold
    
    tr,ur,lat = get_ais() # AIS
    
    tm,um,_,_ = get_hadcp_run036(mode='node-based') # FVCOM
    
    # keep concurrent parts only
    tmin = max(np.min(tr),np.min(tm))
    tmax = min(np.max(tr),np.max(tm))
    ivalm = np.logical_and(tm>=tmin,tm<=tmax)
    tm = tm[ivalm]
    um = um[ivalm]
    ivalo = np.logical_and(tr>=tmin,tr<=tmax)
    tr = tr[ivalo]
    ur = ur[ivalo]
    
    up = get_chs_prediction(tr,lat) # CHS
    
    # transit windows
    tos,toe = transit_window(tr,ur,-uthres,uthres)
    
    scales = np.arange(0.85, 1.16, 0.01)
    
    # check how velocity scaling changes the performance
    po = np.full((len(scales)),dt.timedelta(0))
    pu = np.full((len(scales)),dt.timedelta(0))
    mo = np.full((len(scales)),dt.timedelta(0))
    mu = np.full((len(scales)),dt.timedelta(0))
    for kp,scale in enumerate(scales):
        tps,tpe = transit_window(tr,up*scale,-uthres,uthres)
        top,tup, tosf,toef,tpsf,tpef,ttot = analyse_transit_window_chunks(tos,toe,tps,tpe,tr,ur)
        po[kp] = np.sum(top)
        pu[kp] = np.sum(tup)
        tms,tme = transit_window(tm,um*scale,-uthres,uthres)
        tom,tum, tosf,toef,tmsf,tmef,ttot = analyse_transit_window_chunks(tos,toe,tms,tme,tr,ur)
        mo[kp] = np.sum(tom)
        mu[kp] = np.sum(tum)
    
    plt.figure()
    plt.plot(scales,[k.total_seconds()/60/60/24 for k in po],'.-k')
    plt.plot(scales,[k.total_seconds()/60/60/24 for k in pu],'.--k')
    plt.plot(scales,[k.total_seconds()/60/60/24 for k in mo],'.-b')
    plt.plot(scales,[k.total_seconds()/60/60/24 for k in mu],'.--b')
    plt.legend(['prediction overestimation',
                'prediction underestimation',
                'model overestimation',
                'model underestimation'])
    plt.grid()
    plt.ylabel('Total over/underestimated transit time (days)')
    plt.xlabel('Velocity scaling')


def script_check_modes():
    """ Test model performance with series derived in different modes and 
    with different velocity scaling
    """
    uthres = 2*0.51444 # m/s TW threshold
    
    tr,ur,lat = get_ais() # AIS
    
    # keep concurrent parts only
    tm,_,_,_ = get_hadcp_run036(mode='one node') # good to get time limits
    tmin = max(np.min(tr),np.min(tm))
    tmax = min(np.max(tr),np.max(tm))
    ivalo = np.logical_and(tr>=tmin,tr<=tmax)
    tr = tr[ivalo]
    ur = ur[ivalo]
    
    up = get_chs_prediction(tr,lat) # CHS
    
    # transit windows
    tos,toe = transit_window(tr,ur,-uthres,uthres)
    
    # CHS prediction without scaling
    tps,tpe = transit_window(tr,up,-uthres,uthres)
    tps,tpe = transit_window(tr,up*0.91,-uthres,uthres) ##### scale so under~==over fpr prediction
    
    # analyse CHS prediction
    top,tup, tosf,toef,tpsf,tpef,ttot = analyse_transit_window_chunks(tos,toe,tps,tpe,tr,ur)
    # total predicted transit time (all windows)
    twptot = dt.timedelta(0)
    for t1,t2 in zip(tpsf,tpef): # for each predicted transit window
        twptot += t2-t1
    twptot = np.sum(twptot)
    overtotp = np.sum(top) 
    undertotp = np.sum(tup)
    
    # analyse the model
    model_modes=['one node','node-based','element-based']
    scales = np.arange(0.95, 1.16, 0.01)
    bo = np.zeros((len(scales),len(model_modes)))
    bu = np.zeros((len(scales),len(model_modes)))
    for km,mode in enumerate(model_modes):
        # model
        tm,um,_,_ = get_hadcp_run036(mode=mode)
        
        # keep concurrent parts only
        ivalm = np.logical_and(tm>=tmin,tm<=tmax)
        tm = tm[ivalm]
        um = um[ivalm]
        for ks,sc in enumerate(scales):
            tms,tme = transit_window(tm,um*sc,-uthres,uthres)
            tom,tum, tosf,toef,tmsf,tmef,ttot = analyse_transit_window_chunks(tos,toe,tms,tme,tr,ur)
            bo[ks,km] = (overtotp-np.sum(tom))/twptot*100
            bu[ks,km] = (undertotp-np.sum(tum))/twptot*100
    
    from cycler import cycler
    import matplotlib as mpl
    mpl.rcParams['axes.prop_cycle'] = cycler(color='rbgrbg')
    plt.figure()
    plt.plot(scales,bo,'.-')
    plt.plot(scales,bu,'.--')
    plt.legend(['single node overestimation',
                'node-based overestimation',
                'element-based overestimation',
                'single node underestimation',
                'node-based underestimation',
                'element-based underestimation'])
    plt.grid()
    plt.ylabel('Model better than CHS prediction (%)')
    plt.xlabel('Model velocity scaling')
    
    return scales,bo,bu


def script(model_mode='one node',model_scale=1,print_summary=True,do_plots=True):
    """ General script for the anlysis
    
    TW analysis for a) FVCOM model and b) CHS predictions relative to observations (AIS)
    
    model_mode : 'one node', 'node-based', or 'element-based'
        How to extract model velocity
    model_scale : float
        Hack to test performance of scaled model velocities
    """
    uthres = 2*0.51444 # m/s TW threshold
    tos,toe,tps,tpe,tms,tme, tr,ur,up, tm,um = get_transit_window(uthres=uthres,
                                                                  model_mode=model_mode,
                                                                  model_scale=model_scale)
    
    tom,tum, tosf,toef,tmsf,tmef,ttot = analyse_transit_window_chunks(tos,toe,tms,tme,tr,ur)
    top,tup, tosf,toef,tpsf,tpef,ttot = analyse_transit_window_chunks(tos,toe,tps,tpe,tr,ur)
    
    # total predicted transit time (all windows)
    twptot = dt.timedelta(0)
    for t1,t2 in zip(tpsf,tpef): # for each predicted transit window
        twptot += t2-t1
    twptot = np.sum(twptot)
    
    # stats
    overtotm = np.sum(tom)
    undertotm = np.sum(tum)
    overtotp = np.sum(top) 
    undertotp = np.sum(tup)
    betterover = (overtotp-overtotm)/twptot*100
    betterunder = (undertotp-undertotm)/twptot*100
        
    # print stats
    if print_summary:
        print('')
        print('Transit Window Analysis')
        print('=======================')
        print('Total length of time series analysed (continuous intervals): ',ttot)
        print('Total CHS-predicted transit time: ',twptot)
        print('')
        print('Model')
        print('-----')
        print('Total overestimation: ',overtotm)
        print('Total underestimation: ',undertotm)
        print('')
        print('CHS prediction')
        print('--------------')
        print('Total overestimation: ',overtotp)
        print('Total underestimation: ',undertotp)
        print('')
        print('Model is {:.2f}% better than CHS prediction for overestimation'.
              format(betterover))
        print('Model is {:.2f}% better than CHS prediction for underestimation'.
              format(betterunder))
        print('')
    
    if do_plots:
        # plot underestimation and overestimation for each observed TW
        secom = np.array([k.total_seconds() for k in tom])
        secum = np.array([k.total_seconds() for k in tum])
        secop = np.array([k.total_seconds() for k in top])
        secup = np.array([k.total_seconds() for k in tup])
        plt.figure()
        plt.plot(secom/60/60,'ob',markerfacecolor='none')
        plt.plot(-secum/60/60,'xb') # plot underestimations negative
        plt.plot(secop/60/60,'or',markerfacecolor='none')
        plt.plot(-secup/60/60,'xr') # plot underestimations negative
        plt.legend(['Model overestimation','Model underestimation',
                    'CHS prediction overestimation','CHS prediction underestimation'])
        plt.grid()
        plt.xlabel('Observed Transit Window number')
        plt.ylabel('Over-/Underestimation (hours)')
        plt.title('Over-/Underestimation of Transit Window')
        
        # plot time series and transit windows
        plt.figure()
        t,u,_ = get_ais_raw()
        hr = plot_transit_window(t, u, tosf,toef,uthres,linestyle='--')
        ho = plot_transit_window(tr,ur,tosf,toef,uthres,linestyle='--')
        hm = plot_transit_window(tm,um,tmsf,tmef,uthres,linestyle=':')
        hp = plot_transit_window(tr,up,tpsf,tpef,uthres,linestyle='-.')
        plt.legend([hr,ho,hm,hp],['AIS raw','AIS processed','Model','CHS prediction'])
        plt.ylabel('U-component (m/s)')
        plt.title('Analysed series and Transit Windows (threshold={} m/s)'.format(uthres))
        
        # histograms of Under-/Over-estimation
        def uo_hist(ax,tum,tom,title):
            su = np.array([k.total_seconds() for k in tum])/60/60
            so = np.array([k.total_seconds() for k in tom])/60/60
            ax.hist(-su,100,log=True);
            ax.hist(so,100,log=True,color='r');
            ax.grid()
            ax.set_ylabel('Number of windows')
            ax.set_title(title)
        fig,ax = plt.subplots(2,1,sharex=True,sharey=True,figsize=(13.07,6.69))
        uo_hist(ax[0],tum,tom,'Model')
        uo_hist(ax[1],tup,top,'Prediction')
        ax[-1].set_xlabel('Under-/Over-estimation (hours)')
    
    return betterover,betterunder,tom,tum,top,tup


def analyse_transit_window_chunks(tos,toe,tms,tme,t,u):
    """ Aanalyse transit windows on continuous chunks of t,u time series
    
    t,u should be regularly sampled with gaps filled with nans.
    
    """
    
    ist,ien = gutil.continuous_chunks(~np.isnan(u))
    
    ttot = dt.timedelta(0) # combined duration of continuous chunks
    to = []
    tu = []
#    tft = dt.timedelta(0)
#    nf = 0
    tosf = []
    toef = []
    tmsf = []
    tmef = []
    for t1,t2 in zip(t[ist],t[ien-1]): # for each continuous chunk
        ttot += t2-t1
        to_,tu_,tos_,toe_,tms_,tme_ = \
            analyse_transit_window(tos[np.logical_and(tos>=t1, tos<=t2)],
                                   toe[np.logical_and(toe>=t1, toe<=t2)],
                                   tms[np.logical_and(tms>=t1, tms<=t2)],
                                   tme[np.logical_and(tme>=t1, tme<=t2)])
        to = np.r_[to,to_]
        tu = np.r_[tu,tu_]
#        tft += tft_
#        nf += nf_
        tosf = np.r_[tosf,tos_]
        toef = np.r_[toef,toe_]
        tmsf = np.r_[tmsf,tms_]
        tmef = np.r_[tmef,tme_]
    
    return to,tu, tosf,toef,tmsf,tmef, ttot    


def analyse_transit_window(tos,toe,tms,tme):
    """ Transit window (TW) analysis
    
    For each observed TW find:
        - TW overestimation
        - TW underestimation
    Also find completely false model windows
    
    Parameters
    ----------
    
    tos,toe : array_like, datetime objects
        Observational TW start and end times
    tms,tme : array_like, datetime objects
        Model TW start and end times
        
    Returns
    -------
    
    time_over : array_like, timedelta objects
        TW overestimation for each observed TW
    time_under : array_like, timedelta objects
        TW underestimation for each observed TW
        
    tos,toe,tms,tme : array_like, datetime objects
        TW start and end times after check
    
    Notes
    -----
    
    All input end times should correspond to start times, i.e. they should have 
    been obtained from continuous series.
    
    See also
    --------
    get_transit_window()
    """
    
    # Check consistency of transit windows
    tos,toe = check_transit_window(tos,toe)
    tms,tme = check_transit_window(tms,tme)
    
    time_under = tw_underestimate(tos,toe,tms,tme) # observed TW not covered by model
    time_over  = tw_underestimate(tms,tme,tos,toe) # model TW not covered by observed
    
    return time_over,time_under, tos,toe,tms,tme
    

def tw_underestimate(tos,toe,tms,tme):
    """ Underestimation of model TWs relative to the observed TWs
    
    Parameters
    ----------
    
    tos,toe : array_like, datetime objects
        Observational TW start and end times
    tms,tme : array_like, datetime objects
        Model TW start and end times
    
    Returns
    -------
    
    time_under : array_like, timedelta objects
        TW underestimation for each observed TW
    """
    time_under = toe - tos # preallocate with full window lengths
    for k in range(len(tos)):
        # overlap with each model TW
        tx = np.minimum(toe[k],tme)-np.maximum(tos[k],tms)
        # model intervals that overlap this obs interval
        iover = tx>dt.timedelta(0)
        if np.any(iover):
            time_under[k] -= np.sum(tx[iover]) # reduce by overlap part
    
    return time_under


def check_transit_window(ts,te):
    """ Check consistency of transit window bounds
    
    Checks:
        TW start, `ts`, goes first, TW end, `te`, is last
        Exactly one `te` is between successive `ts`
        Exactly one `ts` is between successive `te`
        
    Parameters
    ----------

    ts,te : array_like, numbers or datetime objects
        Vectors with starts and ends of transit window intervals
            
    Returns
    -------
    
    ts,te : array_like, numbers or datetime objects
        Vectors with verified consistency
    """
    
    if ts[0]>te[0]:
        te = te[1:]
        
    if te[-1]<ts[-1]:
        ts = ts[:-1]
    
    if len(ts) != len(te):
        raise RuntimeError('Vectors must be of same length.')
    else:
        if type(ts[0])==dt.datetime:
            zero_interval = dt.timedelta(days=0)
        else: # assume it's numbers
            zero_interval = 0
        
        ok = np.logical_and((te - ts)>=zero_interval, np.r_[True,(ts[1:] - te[:-1])>=zero_interval])
        
        if np.all(ok):
            return ts,te
        else:
            inv = np.where(~ok)[0][0]
            raise RuntimeError('Inconsistent bounds (reversed or overlapping). First of them: start={} end={}'.format(ts[inv],te[inv]))


def plot_transit_window(t,u,ts,te,ytw,linestyle='--',scolor='g',ecolor='r'):
    """ Time series plot with TWs marked
    
    Parameters
    ----------

    t,u : array_like
        Time series
    ts,te : array_like of datetime
        Transit windows start and end times
    ytw : float
        Extent of TW markers along Y-axis in both pos and neg directions
    """
    h, = plt.plot(t,u)
    plt.plot([t[0],t[-1]],[-ytw, -ytw],'--k')
    plt.plot([t[0],t[-1]],[ytw, ytw],'--k')
    plt.plot(np.c_[ts,ts].T,np.tile([-ytw, ytw],(len(ts),1)).T,linestyle=linestyle,color=scolor);
    plt.plot(np.c_[te,te].T,np.tile([-ytw, ytw],(len(te),1)).T,linestyle=linestyle,color=ecolor);
    plt.grid(True)
    
    return h


def get_transit_window(uthres=2*0.51444,model_mode='single node',model_scale=1):
    """ Calculate transit windows for Second Narrows velocity time series
    
    for U-component of observed (HADCP), CHS prediction, and model
    
    Parameters
    ----------

    uthres : float
        Velocity threshold for transit window detection (m/s). Default is 2 knots.
    model_mode : 'single node', 'node-based', or 'element-based'
        How to extract model velocity
    model_scale : float
        Coefficient to scale model velocity
        
    Returns
    -------
    
    tos,toe,tps,tpe,tms,tme : array_like of datetime objects
        TW start and end times for obs, CHS prediction, and model
    tr,ur,up : array_like
        Time series for obs and CHS prediction (with common time vector)
    tm,um : array_like
        Time series for the model
        
    Example
    -------
        import transit_window_analysis as twa
        import matplotlib.pyplot as plt
        import numpy as np
        tos,toe,tps,tpe,tms,tme, tr,ur,up, t,u = twa.get_transit_window()
        plt.plot(tr,ur,tr,up,t,u); plt.grid()
        plt.legend(['AIS data','CHS const','Model'])
        plt.plot(np.c_[toe,toe].T,np.tile([-2, 2],(len(toe),1)).T,'--r');
        plt.plot(np.c_[tos,tos].T,np.tile([-2, 2],(len(tos),1)).T,'--g');
        plt.plot(np.c_[tme,tme].T,np.tile([-1.5, 1.5],(len(tme),1)).T,'--m');
        plt.plot(np.c_[tms,tms].T,np.tile([-1.5, 1.5],(len(tms),1)).T,'--c');
        plt.plot(np.c_[tpe,tpe].T,np.tile([-2.5, 2.5],(len(tpe),1)).T,'-.m');
        plt.plot(np.c_[tps,tps].T,np.tile([-2.5, 2.5],(len(tps),1)).T,'-.c');
    """
    
    # AIS
    tr,ur,lat = get_ais()
    
    # model
    tm,um,_,_ = get_hadcp_run036(mode=model_mode)
    
    # keep concurrent parts only
    tmin = max(np.min(tr),np.min(tm))
    tmax = min(np.max(tr),np.max(tm))
    
    ival = np.logical_and(tr>=tmin,tr<=tmax)
    tr = tr[ival]
    ur = ur[ival]
    
    ival = np.logical_and(tm>=tmin,tm<=tmax)
    tm = tm[ival]
    um = um[ival]
    
    # CHS
    up = get_chs_prediction(tr,lat)
    
    # transit windows
    tos,toe = transit_window(tr,ur,-uthres,uthres)
    tps,tpe = transit_window(tr,up,-uthres,uthres)
    tms,tme = transit_window(tm,um*model_scale,-uthres,uthres)
    
    return tos,toe,tps,tpe,tms,tme, tr,ur,up, tm,um


def get_ais():
    """ AIS HADCP series
    
    Issues with obs:
        - interference (instrument cross-talk)
        - spikes; largely dealt with
        - HF noise; removed by smoothing
    """
    print('*** Getting AIS series ***')
    
    t,u,lat = get_ais_raw()
    
    #sampling interval rounded to whole seconds
    si = dt.timedelta(seconds=np.round(gutil.find_si_robust(t).total_seconds()))
    
    tr,ur = gutil.regularize(t,u,si)
    
    # despike
    fs=1/(si.total_seconds()/60/60/24) # cpd
    fco = 12 # cpd, cutoff at 2 hrs
    ul = gutil.filterdata(ur,fco, fs=fs, rolloffband=1/fco/5, attenuation=1e-3, showinfo=True)
    inv = gutil.spikes(ur-ul) # detect for high-passed series
    ur[inv] = np.nan
    
    # Smooth with ~40 min cutoff
    ur = gutil.filterdata(ur,1/(40/24/60), fs=fs, rolloffband=10/24/60, attenuation=1e-3, showinfo=True)
    
    # fill small gaps (< 1 hr)
    ur = gutil.fill_small_gaps(tr,ur,round(dt.timedelta(hours=1)/si))
    
    print('*** Finished getting AIS series ***')
    
    return tr,ur,lat


def get_ais_raw():
    """ Get HADCP series as supplied by AIS with bad data removed and time converted to UTC
    """
    s = sio.loadmat(ais_matfile)
    t = gutil.mtime2datetime(s['s']['time'][0,0][:,0])
    u = s['s']['u'][0,0][:,0]
    u = u*0.51444 # knots to m/s
    lat = s['s']['lat'][0,0][0,0]
    
    # bad data in AIS Second Narrows HADCP (times are PST)
    tbad = [dt.datetime(2018,2,8,16,3),dt.datetime(2018,2,14,22,0),
            dt.datetime(2018,3,1,5,56),dt.datetime(2018,3,1,9,11),
            dt.datetime(2018,3,11,9,0),dt.datetime(2018,3,12,10,0),
            dt.datetime(2018,7,31,17,0),dt.datetime(2018,7,31,23,30),
            dt.datetime(2018,12,20,12,33),dt.datetime(2018,12,20,15,55),
            dt.datetime(2018,12,20,22,19),dt.datetime(2018,12,20,22,45),
            dt.datetime(2018,12,21,0,8),dt.datetime(2019,1,8,14,0)]
    for k in range(0,int(len(tbad)/2),2):
        u[np.logical_and(t>=tbad[k],t<=tbad[k+1])] = np.nan
        
    # another chunk that is most likely bad: significantly underestimated velocities
    u[np.logical_and(t>=dt.datetime(2018,7,30,0,0),t<=dt.datetime(2018,9,17,0,0))] = np.nan
    
    with np.errstate(invalid='ignore'):
        u[np.abs(u)>3.5] = np.nan # remove larger than normal velocities
    
    #TODO: use timezone-aware times for proper conversion
    # daylight savings to normal
    inon = np.logical_and(t>dt.datetime(2018,3,11,17,0),t<=dt.datetime(2018,3,11,18,0)) # non-existent time
    print('Values at non-existent times removed: ',np.sum(inon))
    t = t[~inon]
    u = u[~inon]
    idst = t>dt.datetime(2018,3,11,18,0) # IDK why it's normal before that
    t[idst] -= dt.timedelta(hours=1)
    # convert to UTC
    t = t + dt.timedelta(hours=8)
    
    return t,u,lat


def get_chs_prediction(tr,lat):
    print('*** Getting CHS predicted series ***')
    # CHS constituents
    cname,freq,amaj,amin,inclination,phase,hdr = read_chs_curr(chs_const_file)
    freq[0] = 0
    
    # CHS constituents are relative to local time
    with np.errstate(divide='ignore'):
        period_hr = 1/freq
    period_hr[0] = 0
    with np.errstate(divide='ignore'):
        phase_greenwich = phase + 8/period_hr*360
#    phase_greenwich = phase # after converting tr to UTC, we don't need to shift phases any more
    phase_greenwich[0] = 0
    
    zz = np.zeros(len(freq))
    tidecon = np.c_[amaj,zz, amin,zz, inclination,zz, phase_greenwich,zz]
    
    cname = tt.t_utils.fourpad(cname)
    uvp = tt.t_predic(tr, cname, np.array(freq), tidecon, lat=lat, synth=0)
    up = np.real(uvp)
    
    # looks like Z0 is not treated right in t_predic, 
    # we have phase=180 for Z0, i.e. it should be negative, whereas mean(up)>0
    # so subtract double Z0 amplitude (inclination is small)
    up -= 2*amaj[0]
    print('*** Finished getting CHS predicted series ***')
    
    return up


def get_hadcp_run036_u_elem():
    """ Get u at HADCP transect from the full grid output, i.e. from u at elements.
    """
    print('*** Getting model series ***')
    subs = ['b','b2','b3','b4','b5'] # subfolders for sub-runs
    casename = 'vh_x2'
    
    f = fpp.nclist(os.path.join(run036folder,subs[0]),casename=casename)
    
    x,y,h,tri,_,_,_,siglay,siglev = fpp.ncgrid(f[0])
    
    # generate transect points
    utm10 = Proj("+proj=utm +zone=10")
    # coords from 
    # Data/opp/observed/hadcp2nd/DFO 20181129/PR-1011 ASL DFO 2nd Narrows Data.pdf
    xt0,yt0 = utm10(-(123+1.472/60),49+17.644/60)
    # ranges from 
    # Data/opp/observed/hadcp2nd/DFO 20181129/Raw Data/October 2018/_RDI_000.000
    ranges = np.arange(4,132+2,2) # entire range
    xt = np.full(ranges.shape,xt0)
    yt = yt0 + ranges
    
    tr = fpp.vertical_transect(xt,yt,x,y,tri,h,siglay,siglev)
    
    t,dd,zsig,u,longname,units = fpp.vertical_transect_snap_all(f,'u',tr)
    for sub in subs[1:]:
        f = fpp.nclist(os.path.join(run036folder,sub),casename=casename)
        t2,_,zsig2,u2,_,_ = fpp.vertical_transect_snap_all(f,'u',tr)
        t = np.r_[t,t2[1:]]
        zsig = np.concatenate((zsig,zsig2[:,:,1:]),axis=2)
        u = np.concatenate((u,u2[:,:,1:]),axis=2)    
    
    print('*** Finished getting model series ***')
    return t,dd,zsig,u,longname,units


def get_hadcp_run036(mode='one node'):
    print('*** Getting model series ***')
    subs = ['b','b2','b3','b4','b5'] # subfolders for sub-runs
    fname = 'vh_x2_station_timeseries.nc'
    
    if mode != 'element-based':
        if mode == 'one node':
            stn = 'HADCP 17952' # this is a sweet spot (with surface layer only)
    #        stn = 'HADCP 17989'
            layer = 1 # 2nd layer
#            layer = 0 # 1st layer
            ui = None
            ranges = None
        else: # Average all hadcp nodes from station output
            stn = ['HADCP 17952','HADCP 17956','HADCP 17954','HADCP 17945',
                   'HADCP 17981','HADCP 17989','HADCP 17984']
            layer = None # all layers
        
        t,u = fpp.ncstation(os.path.join(run036folder,subs[0],fname),'u',stn,layer=layer)
        for sub in subs[1:]:
            t2,u2 = fpp.ncstation(os.path.join(run036folder,sub,fname),'u',stn,layer=layer)
            t = np.r_[t,t2[1:]]
            u = np.r_[u,u2[1:]]
        u = np.squeeze(u)
    
    if mode != 'one node':
        # instrument depth from 
        # Data/opp/observed/hadcp2nd/DFO 20181129/PR-1011 ASL DFO 2nd Narrows Data.pdf
        zadcp = 7.34 # m, relative to MSL
        # ranges from 
        # Data/opp/observed/hadcp2nd/DFO 20181129/Raw Data/October 2018/_RDI_000.000
        ranges = np.arange(4,132+2,2) # entire range
        
        if mode == 'node-based':
            # generate transect coords
            utm10 = Proj("+proj=utm +zone=10")
            # coords from 
            # Data/opp/observed/hadcp2nd/DFO 20181129/PR-1011 ASL DFO 2nd Narrows Data.pdf
            xt0,yt0 = utm10(-(123+1.472/60),49+17.644/60)
            
            xt = np.full(ranges.shape,xt0)
            yt = yt0 + ranges
            
            # make up a small triangular grid (only HADCP output nodes)
        
            ncf = nc.Dataset(os.path.join(run036folder,subs[0],fname))
            
            x,y,h,_,_,_,_,siglay,siglev = fpp.ncgrid(ncf)
            
            # pick requested stations
            stations = fpp.ncstation_names(ncf)
            # indices of requested stations in the stations list
            istn = [stations.index(k) for k in stn]
            x = x[istn]
            y = y[istn]
            h = h[istn]
            siglay = siglay[:,istn]
            siglev = siglev[:,istn]
            
            # indices of grid nodes which are output in station file for HADCP vicinity
            nstn = [int(s.replace('HADCP ','')) for s in stn] # 1-based
            gtri,_ = fvt.readMesh_V3(vh_x2_grd_file) # triangles for the entire grid (1-based)
            # triangles for "station" nodes (entirely comprised of "station" nodes)
            stri = gtri[np.all(np.isin(gtri,nstn),axis=1)]
            # re-number: indices into nstn of elements in stri (0-based)
            tri = np.array([np.where(nstn==k)[0][0] for k in stri.flatten()]).reshape(stri.shape)
            
    #        # plot the small grid and station nodes for HADCP
    #        plt.figure()
    #        plt.triplot(x,y,tri)
    #        for k in range(len(x)): plt.text(x[k],y[k],stn[k])
    #        plt.plot(xt,yt,'.-r')
            
            tr = fpp.vertical_transect(xt,yt,x,y,tri,h,siglay,siglev)
            
            zeta = fpp.ncstation_var(os.path.join(run036folder,subs[0],fname),'zeta',stn)
            for sub in subs[1:]:
                zeta2 = fpp.ncstation_var(os.path.join(run036folder,sub,fname),'zeta',stn)
                zeta = np.r_[zeta,zeta2[1:]]
            
            # make u in (nodes,nlayers,ntime) shape; zeta in (nodes,ntime) shape
            dd,zsig,utr = vertical_transect_eval(tr,u.T,zeta.T)
            
        elif mode == 'element-based':
            # transect u from elements (pre-processed with get_hadcp_run036_u_elem)
            f = np.load('/media/krassovskim/MyPassport/Data/opp/vh/run036/get_hadcp_run036_u_elem.npz')
            t = f['t']
            dd = f['dd'][:,0]
            zsig = f['zsig']
            utr = f['u']
            
    #        # plot model velocity transects at peak positive/negative and close to TW threshold
    #        kt=6138; transect_plot(dd,zsig[:,:,kt],u[:,:,kt],t[kt],-zadcp,ranges-ranges[0])
    #        plt.savefig('eps/twa_u_transect_peak_neg_run036.png')
    #        kt=6144; transect_plot(dd,zsig[:,:,kt],u[:,:,kt],t[kt],-zadcp,ranges-ranges[0])
    #        plt.savefig('eps/twa_u_transect_peak_pos_run036.png')
    #        kt=6147; transect_plot(dd,zsig[:,:,kt],u[:,:,kt],t[kt],-zadcp,ranges-ranges[0])
    #        plt.savefig('eps/twa_u_transect_2kts_pos_run036.png')
    #        kt=6148; transect_plot(dd,zsig[:,:,kt],u[:,:,kt],t[kt],-zadcp,ranges-ranges[0])
    #        plt.savefig('eps/twa_u_transect_2kts_neg_run036.png')
                
        zsig *= -1 # invert sign to make positive; 
        # after that, zsig goes from small to high (important for np.interp)
        
        # keep only nodes at adcp bins
        iadcp = np.isin(dd,ranges-ranges[0])
        dd = dd[iadcp]
        zsig = zsig[iadcp,:]
        utr = utr[iadcp,:]
        
        # Interpolate layers to HADCP instrument depth
        nbin = utr.shape[0]
        ntime = utr.shape[2]
        ui = np.zeros((nbin,ntime))
        for kd in range(nbin): # for adcp bins
            for kt in range(ntime): # for each time
                ui[kd,kt] = np.interp(zadcp,zsig[kd,:,kt],utr[kd,:,kt])
        
        # mean velocity: this is what presumably goes in AIS feed
        u = np.mean(ui,axis=0)
    
    print('*** Finished getting model series ***')
    return t,u,ui,ranges


def compare_x2_r12_profile():
    """ Compare velocity profile at HADCP bins for x2 and r12 models
    based on nowcasts:
        salish.eos.ubc.ca/opp/fvcom/nowcast-x2/14apr19/vh_x2_0001.nc
        salish.eos.ubc.ca/opp/fvcom/nowcast-r12/14apr19/vh_r12_0001.nc
    """
    fx2 = '/media/krassovskim/MyPassport/Data/opp/vh/salish/nowcast-x2/14apr19/vh_x2_0001.nc'
    fr12 = '/media/krassovskim/MyPassport/Data/opp/vh/salish/nowcast-r12/14apr19/vh_r12_0001.nc'
    
    utm10 = Proj("+proj=utm +zone=10")
    xt0,yt0 = utm10(-(123+1.472/60),49+17.644/60)
    ranges = np.arange(4,132+2,2)
    xt = np.full(ranges.shape,xt0)
    yt = yt0 + ranges
    
    zadcp = 7.34 # m, relative to MSL
    
    def get_profile(f,xt,yt,ranges):
        x,y,h,tri,_,_,_,siglay,siglev = fpp.ncgrid(f)
        tr = fpp.vertical_transect(xt,yt,x,y,tri,h,siglay,siglev)
        t,dd,zsig,u,_,_ = fpp.vertical_transect_snap_all(f,'u',tr)
        dd = dd[:,0]
        zsig *= -1
        iadcp = np.isin(dd,ranges-ranges[0])
        dd = dd[iadcp]
        zsig = zsig[iadcp,:]
        u = u[iadcp,:]
        
        # Interpolate layers to HADCP instrument depth
        nbin = u.shape[0]
        ntime = u.shape[2]
        ui = np.zeros((nbin,ntime))
        for kd in range(nbin): # for adcp bins
            for kt in range(ntime): # for each time
                ui[kd,kt] = np.interp(zadcp,zsig[kd,:,kt],u[kd,:,kt])
        return t,ui,u,dd,zsig
    
    t1,u1,uo1,dd1,zsig1 = get_profile(fr12,xt,yt,ranges)
    t2,u2,uo2,dd2,zsig2 = get_profile(fx2,xt,yt,ranges)
    
    
    ul2 = u2.copy()
    ul2[np.abs(ul2)<.2] = np.nan
    ul2mean = np.nanmean(ul2,axis=0,keepdims=True)
    urel2 = np.abs(ul2/ul2mean)
    up2 = np.nanmean(urel2,axis=1)
    
    ul1 = u1.copy()
    ul1[np.abs(ul1)<.2] = np.nan
    urel12 = np.abs(ul1/ul2mean) # show relative to x2!!!
    up12 = np.nanmean(urel12,axis=1)
    
    urel1 = np.abs(ul1/np.nanmean(ul1,axis=0,keepdims=True)) # show relative to itself
    up1 = np.nanmean(urel1,axis=1)
    
    # bin-averaged r12 vs x2
    plt.figure()
    plt.plot(t1,np.mean(u1,axis=0),t2,np.mean(u2,axis=0),'.-')
    plt.grid()
    plt.legend(["r12","x2"])
    
    
    fig, ax = plt.subplots(1,1,figsize=(7.55, 8.91))
    ax.plot(ranges,up1,'.-')
    ax.plot(ranges,up12,'.-')
    ax.plot(ranges,up2,'.-')
#    ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
    ax.grid()
    ax.legend(["x1 rel to its' own mean","r12 rel to x2 mean","x2 rel to its' own mean"])
    ax.set_ylabel('Relative velocity')
    ax.set_xlabel('HADCP bin range (m)')
    ax.set_title('HADCP velocty profile relative to the profile mean '+
              '(for velocities>0.2m/s)');
#    fig.savefig('eps/twa_compare_x2_r12_profile.png')
    
    return t1,u1,t2,u2,uo1,dd1,zsig1
    

def plot_hadcp_rel_vel_profile():
    """ Plot mean relative velocity profile for HADCP observed and model
    """
    # observed: raw HADCP May-July
    sa = sio.loadmat('/media/krassovskim/MyPassport/Data/opp/observed/hadcp2nd/DFO 20181129/Raw Data/October18_0.mat')
    # gutil.struct_disp(s)
    ua = sa['adcp'][0,0]['east_vel']
    ta = sa['adcp'][0,0]['mtime'][0]
    # last value is bad, remove it
    ta = ta[:-1]
    ua = ua[:,:-1]
    ta = gutil.mtime2datetime(ta)
    # keep May-July (Aug-Sep is low)
    ival = ta<dt.datetime(2018,8,1)
    ta = ta[ival]
    ua = ua[:,ival]
    rangesa = sa['adcp'][0,0]['config'][0,0]['ranges'][:,0]
    ula = ua.copy()
    ula[np.abs(ula)<.2] = np.nan
    urela = np.abs(ula/np.nanmean(ula,axis=0,keepdims=True))
    upa = np.nanmean(urela,axis=1)
    
    # model
    def get_model_profile(mode,ta):
        t,_,ui,ranges = get_hadcp_run036(mode=mode)
        ul = ui.copy()
        # keep same time interval as for raw HADCP: May-July
        ival = np.logical_and(t>=ta[0], t<=ta[-1])
        ta = t[ival]
        ul = ul[:,ival]
        ul[np.abs(ul)<.2] = np.nan
        urel = np.abs(ul/np.nanmean(ul,axis=0))
        up = np.nanmean(urel,axis=1)
        return up,ranges
    
    upn,ranges = get_model_profile('node-based',ta)
    upe,_      = get_model_profile('element-based',ta)
    
    fig, ax = plt.subplots(1,1,figsize=(7.55, 8.91))
    ax.plot(rangesa,upa,'.-')
    ax.plot(ranges,upe,'.-')
    ax.plot(ranges,upn,'.-')
    ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
    ax.grid()
    ax.legend(['HADCP','Model element-based','Model node-based'])
    ax.set_ylabel('Relative velocity')
    ax.set_xlabel('HADCP bin range (m)')
    ax.set_title('HADCP velocty profile relative to the profile mean '+
              '(for velocities>0.2m/s) \n '+
              'HADCP raw and FVCOM run036 \n '+'both series from '+
              ta[0].strftime("%Y-%m-%d")+' to '+ta[-1].strftime("%Y-%m-%d"));
#        plt.savefig('eps/twa_HADCP_mean_velocity_profile_run036.png')
    fig.savefig('eps/twa_HADCP_mean_velocity_profile_run036may-july.png')


def vertical_transect_eval(tr,v,zeta):
    """ Evaluate transect `tr` of a node-based fvcom field `v`
    
    Parameters
    ----------

    tr : named tuple
        transect data structure output from fvcom_postprocess.vertical_transect()
    v : array_like
        fvcom field, (nodes,nlayers,ntime)
    zeta : array_like
        fvcom zeta, (nodes,ntime)
    
    Returns
    -------
    
    dd : array_like
        distance along transect, (nd,)
    zsig : array_like
        vertical coordinates, depths at sigma-layers, (nd,nsig,ntime)
    vi : array_like
        field values at the transect, (nd,nsig,ntime)
    
    """
    vi = mesh.tri_interp_eval(tr.nidn, tr.wn, v) # interpolate to transect
    
    nd = len(tr.d)
    ns = tr.siglay.shape[1]
    # siglay-based variable
    # duplicate 1st and last layers to extend data to surface and bottom
    vi = np.concatenate((vi[:,[0],:],vi,vi[:,[-1],:]),axis=1)
    sig = np.c_[np.zeros(nd),tr.siglay,-np.ones(nd)] # mid-layer depths
    ns += 2
    
    # get z of sigma-layers/levels and distance for each transect node
    zeta = mesh.tri_interp_eval(tr.nidn, tr.wn, zeta, zero_allnans=True)
    zsig = zeta[:,None] + (tr.h[:,None]+zeta)[:,None]*sig[:,:,None] # layer bound depths
#    dd = np.tile(tr.d[:,None],(1,ns))
    dd = tr.d
    
    return dd,zsig,vi


def transect_plot(dd,zsig,u,t,zadcp,ranges):
    vname,units = 'Eastward Water Velocity','m/s'
    
    contour_major = np.arange(-3.2,3.2,.2)
    contour_minor = np.setdiff1d(np.arange(-3.2,3.2,.1),contour_major)
    
    fig,ax,hp,cbar = fpp.vertical_transect_plot(dd,zsig,u,'u',vname,units,
                                                t.strftime("%Y-%m-%d %H:%M"),
                                                return_handles=True)
#    hp.vmin=-3; hp.vmax=3
    hp.set_clim(-3,3)
    plt.contour(dd,zsig,u,contour_minor,colors='k',linestyles='--',linewidths=.5)
    h = plt.contour(dd,zsig,u,contour_major,colors='k',linestyles='-',linewidths=.5)
    plt.clabel(h, h.levels, inline=True, fontsize=8, fmt='%0.1f')
    plt.plot(ranges,np.full(ranges.shape,zadcp),'.m',markersize=3)
    plt.ylim(top=3)


def transit_window(time,v,vmin,vmax):
    """ Determine ship transit window based on current speed and min,max current values.
    
    Parameters
    ----------

    time : list of DateTime
        time vecotor
    v : array-like
        corresponding velocity values
    vmin : float
        minimum allowed v for transit
    vmax : float
        maximum allowed v for transit
            
    Returns
    -------
    
    transit_start : list of DateTime
        start times of transit windows
    transit_end : list of DateTime
        end times of transit windows
    """
    
    t0,sign = gutil.zero_crossings(time,v-vmin)
    tstart_min = t0[sign==1]
    tend_min   = t0[sign==-1]
    
    t0,sign = gutil.zero_crossings(time,v-vmax)
    tstart_max = t0[sign==-1]
    tend_max   = t0[sign==1]
    
    transit_start = np.sort(np.r_[tstart_min,tstart_max])
    transit_end = np.sort(np.r_[tend_min,tend_max])
    
    return transit_start, transit_end
