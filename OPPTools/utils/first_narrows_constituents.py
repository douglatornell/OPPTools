""" Constituents for First Narrows under the bridge from 1-year vh_x2 run036
"""
import os
import numpy as np
import netCDF4 as nc
from pyproj import Proj
import ttide
#from . import fvcom_postprocess as fpp
from OPPTools.utils import fvcom_postprocess as fpp
from OPPTools.utils.read_chs_curr import read_chs_curr
from OPPTools.general_utilities import signal

import socket
if socket.gethostname()=='usr-svc-sci-ctn038': # GPSC
    work = '/home/mak003/work/opp/vh/'
    run036folder = work+'run036'
    vh_x2_grd_file = work+'vh_x2_grd.dat'
    chs_const_file = work+'data/04100const.curr'
    ais_matfile = work+'data/ais_collect_station_hadcp_upto2019-03-20.mat'
    
elif socket.gethostname()=='WPBCIOS9039457': # local
    work = '/media/krassovskim/MyPassport/'
    run036folder = work+'Data/opp/vh/run036'
    vh_x2_grd_file = work+'Max/Projects/opp/vh_x2_grd.dat'
    chs_const_file = work+'Data/opp/observed/hadcp2nd/04100const.curr'
    ais_matfile = work+'Max/Projects/opp/mat/ais_collect_station_hadcp_upto2019-03-20.mat'
    
else:
    errstr = 'Unknown host: {} \nProvide paths to data files manually.'.format(
            socket.gethostname())
    raise RuntimeError(errstr)


def get_run036_uv(elem=45227-1,layer=0):
    """ Get u,v at First Narrows under the bridge from the full grid output.
    
    elem,layer : int
        zero-based element and layer indices
    """
    print('*** Getting model series ***')
    subs = ['b','b2','b3','b4','b5'] # subfolders for sub-runs
    casename = 'vh_x2'
    
    ncf = fpp.ncopen(os.path.join(run036folder,subs[0]),casename=casename)
    time = fpp.nctime(ncf)
    u = ncf['u'][:,layer,elem] # ('time', 'siglay', 'nele')
    v = ncf['v'][:,layer,elem] # ('time', 'siglay', 'nele')
    ncf.close()
    
    for sub in subs[1:]:
        ncf = fpp.ncopen(os.path.join(run036folder,sub),casename=casename)
        time = np.concatenate((time,fpp.nctime(ncf)))
        u = np.concatenate((u,ncf['u'][:,layer,elem]))
        v = np.concatenate((v,ncf['v'][:,layer,elem]))
        ncf.close()
    
    [time,iu] = np.unique(time,return_index=True)
    u = u[iu]
    v = v[iu]
    
    print('*** Finished getting model series ***')
    
    serfile = os.path.join(run036folder,'get_run036_uv_elem{}_layer{}_1-based.npz'.format(elem+1,layer+1))
    np.savez(serfile,time=time,u=u,v=v)
#    return time,u,v


def run036_tide_alongshore(elem):
    elem1 = elem + 1 # 1-based
    serfile = os.path.join(run036folder,'get_run036_uv_elem{}_layer1_1-based.npz'.format(elem1))
    d = np.load(serfile,allow_pickle=True)
    time = d['time']
    u = d['u']
    v = d['v']
    theta,lambda1,lambda2 = signal.principal_axes(u,v)
    ur,vr = signal.rotate_vec(u,v,theta)
    istart = 12 # skip 1st 12 hours (spinup)
    savename = 't_tide_run036_v{}degTrue_elem{}_layer1.txt'.format(int(np.round(theta)),elem1)
    outfile = os.path.join(run036folder,savename)
    out = ttide.t_tide(vr[istart:],stime=time[istart],lat=49.3,ray=.7,synth=1e2)#,
#                       outfile=os.path.join(run036folder,savename))
    np.savez(outfile[:-3]+'npz',out)


def run036_tide_uv(elem):
    elem1 = elem + 1 # 1-based
    serfile = os.path.join(run036folder,'get_run036_uv_elem{}_layer1_1-based.npz'.format(elem1))
    d = np.load(serfile,allow_pickle=True)
    time = d['time']
    u = d['u']
    v = d['v']
    istart = 12 # skip 1st 12 hours (spinup)
    savename = 't_tide_run036_uv_elem{}_layer1.txt'.format(elem1)
    outfile = os.path.join(run036folder,savename)
    uv = u + 1j*v
    out = ttide.t_tide(uv[istart:],stime=time[istart],lat=49.3,ray=.7,synth=1e2)#,
#                       outfile=os.path.join(run036folder,savename))
    np.savez(outfile[:-3]+'npz',out)


if __name__=="__main__":
    # Get element index in matlab:
    # First Narrows under the bridge element for vh_x2
    #[x,y,tri] = read_fvcom3_grd('vh_x2_grd.dat');
    #[x,y,z] = read_fvcom3_dep('vh_x2_dep.dat');
    #g.x=x';g.y=y';g.z=z';g.tri=tri;g.type=3;
    #VisualGrid
    #tri1 = load('first_narrows_triangle_vh_x2.txt')
    #itri = find(all(ismember(tri,tri1),2))
    
    # extract and save the series
#    get_run036_uv(elem=45227-1) # First Narrows
#    get_run036_uv(elem=47751-1) # Second Narrows
    
    
    # tidal analysis
    # First Narrows
#    run036_tide_alongshore(45227-1) # to 0-based element index
    run036_tide_uv(45227-1) # to 0-based element index
    
    # Second Narrows
#    run036_tide_alongshore(47751-1) # to 0-based element index
    run036_tide_uv(47751-1) # to 0-based element index
