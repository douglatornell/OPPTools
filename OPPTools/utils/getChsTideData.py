"""
getChsTideData.py

Description: Get Water Level data via the CHS webservice between given dates.

Inputs: (name, type, description and allowed values)

    dtype     [string]:    'obs' for observational data, 'pred' for predictions
    stnId     [string]:    5 digit tide station id
    sd        [string]:    startDate in YYYY-MM-DD HH:MM:SS format
    ed        [string]:    endDate in YYYY-MM-DD HH:MM:SS format
    order     [string]:    order to arrange output data
                           'asc' for ascending, 'desc' for descending (default 'asc')
    numData   [int]:       Number of data to return in each call (default 1000)

Output:

    pandas DataFrame with columns ['TYPE','DATETIME','MDATE','STATION_ID','LAT','LON','VALUE']
    * note that MDATE is a serialized date in the matlab format

Example Call:

    getTide('obs', '07120', '2017-09-08 12:00:00', '2017-09-09 12:00:00', 'asc', 1000)

Sept 27, 2017 MAJ

"""

from zeep import helpers, Client
import json
import datetime as dt
import matplotlib.dates as mdates
import pandas as pd

def getChsTideData(dtype, stnId, sd, ed, order = 'asc', numData = 1000, verbose=True):

    # get observational or prediction data
    if dtype == 'pred':
        endPoint = 'https://ws-shc.qc.dfo-mpo.gc.ca/predictions?WSDL'
        if verbose:
            print('Retrieving predictions from ' + sd + ' to ' + ed +
                  ' for Station ' + stnId)
    else:
        endPoint = 'https://ws-shc.qc.dfo-mpo.gc.ca/observations?WSDL'
        if verbose:
            print('Retrieving water level data from ' + sd + ' to ' +
                   ed + ' for Station ' + stnId)

    origStartDtObj = dt.datetime.strptime(sd, '%Y-%m-%d %H:%M:%S')
    endDtObj = dt.datetime.strptime(ed, '%Y-%m-%d %H:%M:%S')
    client = Client(endPoint)
    data = []
    dates = []
    nreq = 0
    startDtObj = origStartDtObj
    df = pd.DataFrame(columns=['TYPE', 'DATETIME', 'MDATE', 'STATION_ID', 'LAT', 'LON', 'VALUE'])
    while startDtObj < endDtObj:
        nreq += 1
        newStartDate = startDtObj.strftime('%Y-%m-%d %H:%M:%S')
        if dtype == 'pred':
            obj = client.service.search("wl15", -90.0, 90.0, -180.0, 180.0, 0.0,
                                        0.0, newStartDate, ed, 1, numData, True,
                                        "station_id=" + stnId, order)
        else:
            obj = client.service.search("wl", -90.0, 90.0, -180.0, 180.0, 0.0,
                                        0.0, newStartDate, ed, 1, numData, True,
                                        "station_id=" + stnId, order)
        inputDict = helpers.serialize_object(obj)
        outputDict = json.loads(json.dumps(inputDict))

        # No data available
        if nreq == 1 and outputDict['size'] == 0:
            break

        # Data is available, accumulate it
        if outputDict['size'] > 0:
            #print ("Collected "+ str(outputDict['size']) + " data points")
            for i, var in enumerate(outputDict['data']):
                data.append(float(outputDict['data'][i]['value']))
                dates.append(outputDict['data'][i]['boundaryDate']['min'])
            startDtObj = dt.datetime.strptime(dates[-1], '%Y-%m-%d %H:%M:%S')

        # If we received less than a full block of data, we're done
        if outputDict['size'] < numData:
            break

    dts = [dt.datetime.strptime(d, '%Y-%m-%d %H:%M:%S') for d in dates]
    mdts = [mdates.date2num(d) for d in dts]

    # Assemble DataFrame
    df['DATETIME'] = dts
    df['MDATE'] = mdts
    df['STATION_ID'] = stnId
    df['LAT'] = outputDict['boundarySpatial']['latitude']['min']
    df['LON'] = outputDict['boundarySpatial']['longitude']['min']
    df['VALUE'] = data
    df['TYPE'] = dtype
    
    # Remove duplicates due to overlapping start/end times when >1 request used
    df = df.drop_duplicates()

    if verbose:
        print('Retrieval complete, received {} data points with {} requests.'.format(df.shape[0],nreq))

    return df
